The humanet database contains the following three tables: "t_encounters", "t_nodeList" and "t_states".

Table "t_encounters" contains all the events detected by nodes throughout the experiment. It contains 7 fields, which are explained in the sequel:
	- dev1 -> Bluetooth LAP address of the device that generated this entry 
	- dev2 -> Code identifying which type of entry this is:
			a) "eeeee0" -> Transmission power reduction (-5dB)
			b) "eeeee1" -> Transmission power increase (+5dB)
			c) "beac11" -> Device starts functioning in beacon mode
			d) "beac00" -> Device starts functioning in normal mode
			e) "ffffff" -> Device rebooted
			f) In all other cases, dev2 equals to the Bluetooth LAP address of the device detected.
	- init -> Time of day when the event described by this entry started
	- date_init -> Date when the event described by this entry started
	- end -> Time of day when the event described by this entry finished
	- date_end -> Date when the event described by this entry finished
	- state -> Additional info regarding this entry (table "t_states" describes the meaning of used numerical codes):
			a) If dev2="eeeee0" -> New transmission power in dBm
			b) If dev2="eeeee1" -> New transmission power in dBm
			c) If dev2="beac11" -> New transmission power in dBm
			d) If dev2="beac00" -> New transmission power in dBm
			e) If dev2="ffffff" -> Error code 255 as reboot indicator
			f) In all other cases, "state" reflects device's position:
				- "state"=0 -> Device is horizontally
				- "state"=1 -> Device is vertically and static
				- "state"=2 -> Device is vertically and moving

Besides, and to identify more easily each device, table "t_nodeList" provides the list of the devices (and their Bluetooth address) that took part in the experiment:
	- id -> unique numerical identificator of a device (in range [1,56] for people and [57,86] for beacons)
	- nap -> Bluetooth NAP address of the device
	- uap -> Bluetooth UAP address of the device
	- lap -> Bluetooth LAP address of the device



mysql> show tables;
+-------------------+
| Tables_in_humanet |
+-------------------+
| t_encounters     |
| t_nodeList        |
| t_states          |
+-------------------+

mysql> describe t_encounters;
+-----------+------------+------+-----+---------+-------+
| Field     | Type       | Null | Key | Default | Extra |
+-----------+------------+------+-----+---------+-------+
| dev1      | varchar(6) | YES  |     | NULL    |       |
| dev2      | varchar(6) | YES  |     | NULL    |       |
| init      | time       | YES  |     | NULL    |       |
| date_init | date       | YES  |     | NULL    |       |
| end       | time       | YES  |     | NULL    |       |
| date_end  | date       | YES  |     | NULL    |       |
| state     | int(3)     | YES  |     | NULL    |       |
+-----------+------------+------+-----+---------+-------+

mysql> describe t_nodeList;
+-------+------------+------+-----+---------+-------+
| Field | Type       | Null | Key | Default | Extra |
+-------+------------+------+-----+---------+-------+
| id    | int(3)     | YES  |     | NULL    |       |
| nap   | int(4)     | YES  |     | NULL    |       |
| uap   | int(2)     | YES  |     | NULL    |       |
| lap   | varchar(6) | NO   | PRI | NULL    |       |
+-------+------------+------+-----+---------+-------+

mysql> describe t_states;
+-------+-------------+------+-----+---------+-------+
| Field | Type        | Null | Key | Default | Extra |
+-------+-------------+------+-----+---------+-------+
| id    | int(3)      | NO   | PRI | 0       |       |
| state | varchar(20) | YES  |     | NULL    |       |
+-------+-------------+------+-----+---------+-------+

