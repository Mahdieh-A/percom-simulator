package it.uniroma1.di.kosta;

public class SORTRecom implements Recommendation {

	Node recommender;
	private int rsh; // service trust history size with that node
	private int rrh; // recommendation history size
	private double rcb;
	private double rib;
	private double rr;

	public SORTRecom(int rsh, int rrh, double rcb, double rib, double rr) {
		super();
		// this.recommender = source;
		this.rsh = rsh;
		this.rrh = rrh;
		this.rcb = rcb;
		this.rib = rib;
		this.rr = rr;
	}

	public Node getRecommender() {
		return recommender;
	}

	public int getRsh() {
		return rsh;
	}

	public int getRrh() {
		return rrh;
	}

	public double getRcb() {
		return rcb;
	}

	public double getRib() {
		return rib;
	}

	public double getRr() {
		return rr;
	}

	// A recommendation is valid only if the recommender has interaction history
	// with that node
	boolean isValid() {
		return (this.rrh != 0 || this.rsh != 0);
	}

	@Override
	public float getMetric() {
		double serviceTrust = ((double) rsh / Constants.maxSizeForSORT) * (this.rcb - this.rib / 2)
				+ (1 - ((double) rsh / Constants.maxSizeForSORT)) * this.rr;
		// System.out.println("Recom:" + serviceTrust);
		//assert serviceTrust >= 0;
		return (float) serviceTrust;
	}

}
