package it.uniroma1.di.kosta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Vector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.uniroma1.di.kosta.Constants.Commands;
import it.uniroma1.di.kosta.Constants.Devices;
import it.uniroma1.di.kosta.Constants.Framework;
import it.uniroma1.di.kosta.Constants.KEY;
import it.uniroma1.di.kosta.Task.TaskState;

/**
 * Represents a node/client with needs for {@link Task} offloading.
 * 
 * @author sokol
 *
 */
public class Node implements Comparable<Node> {

	public static int countNotStartedLocal = 0;
	public int requestsNum;
	

	//private Logger log;

	private static Random randomCpu = new Random();
	private static Random randomDevice = new Random();
	private float usedJoules;
	private float usedUseFulJoules;
	private int id;
	private Constants.Devices deviceType; //type of device
	private int cpuPower; // in MHz
	private Battery batteryLevel; // The battery level of each node initialized to
	private float batteryLevelMin;
	private float joulesPerPercent; // The joules that every percent of battery

	private Map<Node, NodeStatistics> knownNodes;
	private Map<Double, Integer> statNrNodesInRangeOnTask;

	// Set of future remained budget tasks
	private Set<Task> futureRemainBudgetTasks;
	// Set of tasks that are to be run in local
	private Set<Task> futureLocalTasks;
	// Set containing the tasks that were finished locally
	private Set<Task> finishedLocalTasks;
	// A local execution was started but an offloaded response came before
	// finishing the local execution.
	private Set<Task> interruptedLocalTasks;
	private Map<KEY, Number> constants;

	private double lastBusyTime;

	public Node(int id, int cpuPower, Constants.Devices typeOfDevice, float joulesPPercent, Map<KEY, Number> constants) {
		this.id = id;
		this.requestsNum = 0;
		this.usedJoules = 0;
		this.usedUseFulJoules = 0;
		//log = LogManager.getLogger("Node-" + this.id);
		this.cpuPower = cpuPower;
		lastBusyTime = constants.get(KEY.TASKS_START_GENERATING).floatValue();
		this.joulesPerPercent = joulesPPercent;
		this.constants = constants;
		this.deviceType = typeOfDevice;
		this.batteryLevelMin = 0;
		knownNodes = new HashMap<>();
		futureLocalTasks = new HashSet<>();
		this.futureRemainBudgetTasks = new HashSet<>();
		finishedLocalTasks = new HashSet<>();
		interruptedLocalTasks = new HashSet<>();
		statNrNodesInRangeOnTask = new HashMap<>();
	}

	public Node(int id, Map<KEY, Number> constants) {
		this(id, 
				Utils.intervalRandomInt(randomCpu, Constants.CPU_POWER_MIN, Constants.CPU_POWER_MAX),
				Constants.Devices.values()[Utils.intervalRandomInt(randomDevice, 0, Constants.Devices.values().length-1)],
				Constants.JOULES_PER_PERCENT, constants);
	}



	/**
	 * Returns if this node is busy or not!
	 * 
	 * @return
	 */
	public boolean isBusy(long duration, double maxAllowedTimeStamp) {

		// System.out.println("Busy Result:" + this + "_" +
		// ((maxAllowedTimeStamp - this.lastBusyTime) > duration) + ":"
		// + this.lastBusyTime + ":" + duration + ":" + maxAllowedTimeStamp);

		Long maxdts = (long) maxAllowedTimeStamp;
		Long lbt = (long) this.lastBusyTime;
		Long dur = (long) duration;

		// System.out.println("Busy Result:" + this + "_" +
		// ((maxAllowedTimeStamp - this.lastBusyTime) > duration) + ":"
		// + lbt + ":" + dur + ":" + maxdts);
		Long diff = maxdts - lbt;
		// System.out.println(diff + ":" + dur);
		return (diff < dur);
	}

	/**
	 * Consumes how battery level changes when sending data
	 * 
	 * @param dataSize
	 *            in bytes
	 */
	private void sendData(long dataSize, double current) {
		float consumedJoules = dataSize * Task.JOULES_PER_BYTE_SEND;
		float consumedPercent = (consumedJoules / this.joulesPerPercent);
		this.usedJoules +=consumedJoules;
		// System.out.print("::::::::::::" + this.batteryLevel + ":" +
		// consumedPercent);
		this.batteryLevel.consumeBattery(current, consumedPercent);
		assert(batteryLevel.getBattery(current) >= 0);
		// System.out.println("::::::::::::" + this.batteryLevel);
	}

	public void setMinBatteryLevel(float minBattery) {
		this.batteryLevelMin = minBattery;
	}

	/**
	 * Consumes how battery level changes when receiving data
	 * 
	 * @param dataSize
	 *            in bytes
	 */
	private void receiveData(long dataSize, double current) {
		float consumedJoules = dataSize * Task.JOULES_PER_BYTE_RECEIVE;
		float consumedPercent = (int) (consumedJoules / this.joulesPerPercent);
		this.usedJoules +=consumedJoules;
		// System.out.print("::::::::::::" + this.batteryLevel + ":" +
		// consumedPercent);
		this.batteryLevel.consumeBattery(current, consumedPercent);
		assert(batteryLevel.getBattery(current) >= 0);
		// System.out.println("::::::::::::" + this.batteryLevel);
	}




	/**
	 * This node is meeting with another node. Record the time of this event on
	 * the intercontact map so that when a LEAVE happens the contact duration
	 * can be calculated.<br>
	 * This node will check for two things now:<br>
	 * 1. do I have a task to offload to this other node?<br>
	 * 2. do I have an offloaded task that I was running for this other node?
	 * 
	 * @param otherNode
	 */

	public void meet(double meetTimeStamp, Node otherNode){
		System.out.println(meetTimeStamp+" Meet:" + this.toString() + otherNode.toString());
		NodeStatistics ns = knownNodes.get(otherNode);
		if (ns == null) {
			ns = new NodeStatistics();
			knownNodes.put(otherNode, ns);
		} else {
			// log.info("Found node " + otherNode + " on knownNodes map");
		}
		ns.addMeet(meetTimeStamp);
	}

	public void meetOperations(double meetTimeStamp, Node otherNode, Trace trace) {

		NodeStatistics ns = knownNodes.get(otherNode);

		// log.info(meetTimeStamp + " MEET with " + otherNode);

		// log.info("Known nodes (" + knownNodes.keySet().size() + "):");
		// StringBuilder out = new StringBuilder();
		// for (Node n : knownNodes.keySet()) {
		// out.append(knownNodes.get(n) + " ");
		// }
		// log.info(out.toString());



		// log.info("Number of contacts between " + this + "->" + otherNode + "=" + knownNodes.get(otherNode).meetTimestamps.size());

		// Check if we have some remaining budget task to give it to him, of
		// course if it isn't uploaded to this before
		List<Task> futureRemainBudgetTasksList = new ArrayList<Task>(futureRemainBudgetTasks);
		for (Task tsk : futureRemainBudgetTasksList) {

			if (tsk.isValid(meetTimeStamp) && !ns.futureOffloadTasks.contains(tsk) && !ns.sendingOffloadTasks.contains(tsk)
					&& !ns.offloadedTasks.contains(tsk)) {
				Response propose = null;
				if (constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.FLOPCOIN_THETA || constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.SORT) {
					propose = otherNode.giveOffResponse(tsk, this, meetTimeStamp);
					this.sendData((long) Task.REQ_SIZE, meetTimeStamp);
					otherNode.receiveData((long) Task.REQ_SIZE, meetTimeStamp);
					otherNode.sendData((long) Task.RESP_SIZE, meetTimeStamp);
					this.receiveData((long) Task.RESP_SIZE, meetTimeStamp);
					double wieghtPropose = (tsk.getApplication() == Framework.TASK_OFF) ? (tsk.getEnergy()/Task.MAX_APP_ENERGY) : 1;
					//System.out.println("MeetBudget: " + tsk.getTaskId());
					if (propose.answer.equals(Constants.Commands.NO)){
						if(propose.mass>=0.25)
							ns.trust.interaction(Constants.Interactions.NEGATIVE_NOT_HONEST_IV, wieghtPropose, (float) propose.mass);

					}else if(propose.answer.equals(Constants.Commands.BAD_REPUTATION)){

						ns.trust.interaction(Constants.Interactions.NEGATIVE_NOT_HONEST_IV, wieghtPropose, (float) propose.mass);

					}}

				if(!(constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.FLOPCOIN_THETA || constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.SORT) 
						|| propose.answer.equals(Constants.Commands.YES)) {
					// System.out.println("Id: " + tsk.getTaskId() + "Bid: " + proposedBid.getRequiredFlopCoins());
					Simulation.neighborNum++;
					futureRemainBudgetTasks.remove(tsk);
					ns.futureOffloadTasks.add(tsk);
					if (constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.FLOPCOIN_THETA || constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.SORT) {

					float newThetaSum = (1 - ns.trust.getMetric()) * tsk.getUnsuccessProbability();
					tsk.setUnsuccessProbability(newThetaSum);
					// System.out.println("Add Remain:" + this + otherNode +
					// proposedBid.getRequiredFlopCoins() + ":"
					// + this.flopCoins + ":" + this.burnedFlopCoins);
					if ((1 - tsk.getUnsuccessProbability()) > Constants.THETA_THRESHOLD) {
						// System.out.println("Remove: " + tsk.getTaskId() + " Prob:
						// " + (1 - tsk.getUnsuccessProbability())
						// + "Budget: " + tsk.getTaskRemainingBudget());
					} else {
						// System.out.println("Add Again: " + tsk.getTaskId() + "
						// Prob: " + (1 - tsk.getUnsuccessProbability())
						// + "Budget: " + tsk.getTaskRemainingBudget());
						futureRemainBudgetTasks.add(tsk);
					}}else{
						futureRemainBudgetTasks.add(tsk);
					}
				}}

		}


		// Check if we have some task to offload to the other node.
		Iterator<Task> iterator = ns.futureOffloadTasks.iterator();
		while (iterator.hasNext() && otherNode.isOnForOffloading(meetTimeStamp)) {
			Task task = iterator.next();
			// Start sending the task. Set the startTime of this task to be this meet timestamp.
			//System.out.println(task.getTaskId() + ": Meet Sent To:" + this + otherNode.toString());
			ns.sendingOffloadTasks.add(task);
			double transmissionTimeStamp = task.calculateRequestTxTime(Constants.UL_RATE) + meetTimeStamp;
			this.sendData(task.getRequestSize(), meetTimeStamp);
			otherNode.receiveData(task.getRequestSize(), meetTimeStamp);
			Task localCopyTask = task.copy(transmissionTimeStamp, otherNode);
			localCopyTask.setState(TaskState.TASK_DELIVERED);
			trace.addEvent(localCopyTask);
			iterator.remove();
		}



		// Check if we have executed some offloaded task for the other node.
		// In that case we have to give the result back if the task has
		// finished.

		iterator = ns.receivedAndFinishedTasks.iterator();
		while (iterator.hasNext() && otherNode.isOn(meetTimeStamp)) {
			Task task = iterator.next();
			if(task.getApplication() == Constants.Framework.TASK_OFF){
				if(!task.isValid(task.getTriggerTime())){
					iterator.remove();
				}else{
					double DeliveredTimeStamp = task.calculateResultTxTime(Constants.UL_RATE) + meetTimeStamp;
					//System.out.println(task.getTaskId() + ": Meet Deliver To:" + otherNode + this);
					this.sendData(task.getAnswerSize(), meetTimeStamp);
					otherNode.receiveData(task.getAnswerSize(), meetTimeStamp);
					Task copyTask = task.copy(task.getAnswerSize(), otherNode);
					copyTask.setState(TaskState.RESPONSE_DELIVERED);
					copyTask.setNode1(this);
					ns.receivedAndDeliveredTasks.add(copyTask);
					//System.out.println("meet" + copyTask.getNode1() + ":" + copyTask.getNode2());
					trace.addEvent(copyTask);
					iterator.remove();
				}
			}}

		Vector<Task> recivedTempTasks = new Vector<Task>();
		iterator = ns.receivedTasks.iterator();
		while (iterator.hasNext() && this.isOnForOffloading(meetTimeStamp)) {
			Task task = iterator.next();
			if(!task.isValid(task.getTriggerTime())){
				iterator.remove();
			}else{
				//System.out.println(task.getTaskId() + ": Meet Execute for:" + otherNode);
				Task copyTask = task.copy(meetTimeStamp, otherNode);
				copyTask.setNode1(this);
				recivedTempTasks.add(copyTask);
			}}
		for(Task tmp : recivedTempTasks){
			if(tmp.getApplication()==Constants.Framework.TASK_OFF){
				this.receiveOffloadedTask(otherNode, tmp.copy(), trace, meetTimeStamp);
			}}

		recivedTempTasks = new Vector<Task>();
		iterator = ns.receivedAndFinishedTasks.iterator();
		while (iterator.hasNext() && this.isOnForOffloading(meetTimeStamp)) {
			Task task = iterator.next();
			if(!task.isValid(task.getTriggerTime())){
				iterator.remove();
			}else{
				//System.out.println(task.getTaskId() + ": Meet Execute for:" + otherNode);
				Task copyTask = task.copy(meetTimeStamp, otherNode);
				copyTask.setNode1(this);
				recivedTempTasks.add(copyTask);
			}}
		for(Task tmp : recivedTempTasks){

			if(tmp.getApplication()==Constants.Framework.MES_FORW){
				this.receiveTaskForwarding(otherNode, tmp, trace, meetTimeStamp);
			}
		}
	}

	public void leave(double leaveTimeStamp, Node otherNode) {
		// log.info(leaveTimeStamp + " LEAVE with " + otherNode);

		// Can't be null, there should have been a MEET before
		System.out.println(leaveTimeStamp+" Leave:" + this.toString() + otherNode.toString());

		NodeStatistics ns = knownNodes.get(otherNode);
		assert ns != null;

		ns.addLeave(leaveTimeStamp);

		Iterator<Task> iterator = ns.sendingOffloadTasks.iterator();
		while (iterator.hasNext())
		{
			Task tsk = iterator.next();
			System.out.println(tsk.getTaskId() + ": Stopped offloading:" + tsk.getNode2());
			ns.futureOffloadTasks.add(tsk);
			iterator.remove();
		}

		iterator = ns.receivedAndDeliveredTasks.iterator();
		while (iterator.hasNext())
		{
			Task tsk = iterator.next();
			if(tsk.getApplication() == Constants.Framework.MES_FORW){
				System.out.println(tsk.getTaskId() + ": Stopped delivering:" + tsk.getNode2());
				ns.receivedAndFinishedTasks.add(tsk);
				iterator.remove();
			}}

		//handleOffloadingTasks(otherNode);
		//handleOffloadedResults(otherNode);
	}

	/**
	 * If the battery level is lower than this value, the smart phone becomes
	 * off
	 * 
	 * @return
	 */
	public boolean isOnForOffloading(double current) {
		// return true;
		// System.out.println(this.batteryLevelMin);
		//System.out.println("On:" + ":" + (this.batteryLevel+ ":" + this.batteryLevelMin));
		return (this.batteryLevel.getBattery(current) > this.batteryLevelMin);
	}

	/* If the battery level is lower than this value, the smart phone becomes
	 * off
	 * 
	 * @return
	 */
	public boolean isOn(double current) {
		// return true;
		// System.out.println(this.batteryLevelMin);
		//System.out.println("On:" + ":" + (this.batteryLevel+ ":" + this.batteryLevelMin));
		return (this.batteryLevel.getBattery(current) > 5);
	}


	/**
	 * This node has a new task that can be offloaded. The node will try to find
	 * the most appropriate nodes to whom to offload this task and send it to
	 * them. At the same time, this node will start a timer for running the task
	 * locally if the task was not offloaded to other node. This node will also
	 * start another timer for running the task locally if the task was
	 * offloaded but no response was received during a reasonable time.
	 * 
	 * @param task
	 */
	public void newTask(Trace trace, Task task) {

		System.out.println(task.getState() + ":" + task.getTaskId());
		switch (task.getState()) {

		case TO_RUN:

			// Add some statistics.
			// In particular, get the number of nodes that are currently in
			// contact with this node
			// in this moment. This is important if we want to send the task
			// only to the nodes
			// we are in contact with on the moment of task generation.
			int count = 0;
			for (Node n : knownNodes.keySet()) {
				if (knownNodes.get(n).isInContact())
					count++;
			}
			statNrNodesInRangeOnTask.put(task.getTriggerTime(), count);

			// Calculate the deltaLocalTime to wait before triggering the local execution for this task.
			// Tr(maxAllowedDelay - duration) assures that the task will finish before the delay if executed locally.
			// Add this event to the trace and let see. If in the meantime the task is offloaded to some other node then
			// we can discard the local execution.
			double deltaLocalTime = (task.getMaxAllowedDelay() - task.getDuration());
			Task localCopyTask = task.copy(task.getCreationTime() + deltaLocalTime);
			if(task.getApplication()==Constants.Framework.TASK_OFF){
				localCopyTask.setState(TaskState.TO_RUN_LOCAL);
				trace.addEvent(localCopyTask);
			}else if(task.getApplication()==Constants.Framework.MES_FORW){
				this.futureLocalTasks.add(task);
			}
			System.out.println(this + ":" + task.getTaskId() + ":Task creation:" + task.getCreationTime() + " Max allowed delay:" + (task.getCreationTime() + deltaLocalTime) + " duration:" + task.getDuration());

			// Find the nodes to whom send the task for offload
			LinkedList<Node> nodes = (LinkedList<Node>) chooseNodesForOffload(task);

			if (task.getTaskRemainingBudget() > 0) {
				//System.out.println("Id:" + task.getTaskId() + " RemainBudget:" + task.getUnsuccessProbability());
				this.futureRemainBudgetTasks.add(task);
			}

			// Check if we are already in contact with any of these nodes.
			// If yes, then ask them to execute the task for us and add the task to the offloaded set.
			// Otherwise, put the task to the futureOffloadTask set of the node.

			for (Node otherNode : nodes) {
				NodeStatistics ns = knownNodes.get(otherNode);
				Task offloadCopyTask = task.copy();
				if (ns.isInContact() && otherNode.isOnForOffloading(task.getTriggerTime())) {
					// Start sending the task. Set the startTime of the copy task to be the same as that of the task.
					//System.out.println(task.getTaskId() + ": Sent To:" + otherNode.toString());
					double transmissionTimeStamp = task.calculateRequestTxTime(Constants.UL_RATE) + task.getTriggerTime();
					this.sendData(task.getRequestSize(), task.getTriggerTime());
					otherNode.receiveData(task.getRequestSize(), task.getTriggerTime());
					localCopyTask = task.copy(transmissionTimeStamp, otherNode);
					localCopyTask.setState(TaskState.TASK_DELIVERED);
					ns.sendingOffloadTasks.add(localCopyTask.copy());
					trace.addEvent(localCopyTask);

				} else {
					// Add the task to the other node's list of future offloaded tasks.
					ns.futureOffloadTasks.add(offloadCopyTask);
					//System.out.println("NOT Sent");

				}
			}
			break;

		case TASK_PUNISH:
			NodeStatistics ns = knownNodes.get(task.getNode2());
			//System.out.println(task.getTaskId() + ": Punish:" + task.getNode2());
			if(ns.offloadedTasks.contains(task) && !ns.finishedOffloadedTasks.contains(task)){
				ns.trust.interaction(Constants.Interactions.NEGATIVE_GET_BACK_RESULT_I, 1, 1);
			}
			break;

		case TASK_DELIVERED:

			ns = knownNodes.get(task.getNode2());
			if(!task.isValid(task.getTriggerTime())){
				ns.sendingOffloadTasks.remove(task);
			}else if(ns.sendingOffloadTasks.contains(task))
			{
				//System.out.println(task.getTaskId() + ": Task arrived To:" + task.getNode2());
				//assert ns.meetTimestamps.get(ns.meetTimestamps.size()-1) <= task.getCreationTime();
				ns.offloadedTasks.add(task);
				ns.sendingOffloadTasks.remove(task);
				this.requestsNum++;
				//double max = task.getCreationTime() + task.getMaxAllowedDelay();
				if(task.getApplication()==Constants.Framework.MES_FORW){
					task.getNode2().receiveTaskForwarding(this, task, trace, task.getTriggerTime());
				}else if(task.getApplication()==Constants.Framework.TASK_OFF){
					task.getNode2().receiveOffloadedTask(this, task.copy(), trace, task.getTriggerTime());
				}}

			break;

		case EXCUSE_DELIVERED:
			ns = this.knownNodes.get(task.getNode2());
			//System.out.println(task.getNode2() + ": excuse arrived To0:" + ns.isInContact());
			if(ns.isInContact() && task.getApplication()==Constants.Framework.MES_FORW){
				//System.out.println(task.getTaskId() + ": excuse arrived To0:" + this);
				if(ns.offloadedTasks.contains(task) && (ns.receivedTasks.contains(task) 
						|| ns.receivedAndFinishedTasks.contains(task) || ns.receivedAndDeliveredTasks.contains(task))){
					//System.out.println(task.getTaskId() + ": excuse arrived To1:" + this);
					ns.offloadedTasks.remove(task);
					ns.finishedOffloadedTasks.add(task);
					ns.trust.interaction(Constants.Interactions.POSITIVE_GET_BACK_RESULT_III, 1, 1);
				}else if(ns.offloadedTasks.contains(task)){
					//System.out.println(task.getTaskId() + ": excuse arrived To2:" + this);
					//ns.trust.interaction(Constants, weight, msatis);
					ns.offloadedTasks.remove(task);
					ns.finishedOffloadedTasks.add(task);
				}
			}
			break;
		case RESPONSE_DELIVERED:
			if(task.getApplication()==Constants.Framework.TASK_OFF){
				ns = knownNodes.get(task.getNode2());
				//System.out.println(this.toString() + task.getNode2() + ns + task);
				//System.out.println(ns.receivedAndDeliveredTasks.contains(task));
				if(ns.receivedAndDeliveredTasks.contains(task))
				{
					ns.receivedAndDeliveredTasks.remove(task);
					ns.paidReceivedAndDeliveredTasks.add(task);
					//System.out.println(task.getTaskId() + ": Response Arrived To:" + task.getNode2());
					task.getNode2().receiveOffloadedResult(this, task);
				}}else if(task.getApplication()==Constants.Framework.MES_FORW){

					//System.out.println(task.getTaskId() + ": Response Arrived To:" + task.getNode2());
					deltaLocalTime = (task.getMaxAllowedDelay() - task.getDuration());					
					Task localPunishTask = task.copy(task.getCreationTime() + deltaLocalTime, task.getNode1());
					localPunishTask.setNode1(task.getNode2());
					localPunishTask.setState(TaskState.TASK_PUNISH);
					trace.addEvent(localPunishTask);

				}

			break;

		case TASK_FINISHED:

			ns = knownNodes.get(task.getNode2());
			double DeliveredTimeStamp = task.calculateResultTxTime(Constants.UL_RATE) + task.getTriggerTime();

			if(task.getApplication()==Constants.Framework.TASK_OFF){

				if(!task.isValid(task.getTriggerTime())){
					ns.receivedAndFinishedTasks.remove(task);
				}else if(task.getNode2().isOn(task.getTriggerTime()) && ns.isInContact()){

					//System.out.println(task.getTaskId() + ": Sending Result To:" + task.getNode2());
					this.sendData(task.getAnswerSize(), task.getTriggerTime());
					task.getNode2().receiveData(task.getAnswerSize(), task.getTriggerTime());
					ns.receivedAndFinishedTasks.remove(task);
					localCopyTask = task.copy(DeliveredTimeStamp, task.getNode2());
					localCopyTask.setState(TaskState.RESPONSE_DELIVERED);
					ns.receivedAndDeliveredTasks.add(localCopyTask.copy());
					trace.addEvent(localCopyTask);
				}}else if(task.getApplication()==Constants.Framework.MES_FORW){

					LinkedList<Node> peers = (LinkedList<Node>) chooseNodesForOffload(task);

					if (task.isValid(task.getTriggerTime()) && task.getTaskRemainingBudget() > 0) {
						this.futureRemainBudgetTasks.add(task);
					}

					//System.out.println(task.getTaskId() + ": Sending message to:" + task.getNode2());

					for(Node peer : peers){
						NodeStatistics peerns = knownNodes.get(peer);
						if(peerns.isInContact()){
							if (peerns.isInContact() && peer.isOnForOffloading(task.getTriggerTime())) {
								//System.out.println(task.getTaskId() + ": Sending message to:" + peer);
								this.sendData(task.getRequestSize(), task.getTriggerTime());
								peer.receiveData(task.getRequestSize(), task.getTriggerTime());
								localCopyTask = task.copy(task.getTriggerTime() + task.calculateRequestTxTime(Constants.UL_RATE), peer);
								localCopyTask.setState(TaskState.TASK_DELIVERED);
								localCopyTask.setNode1(this);
								peerns.sendingOffloadTasks.add(localCopyTask.copy());
								trace.addEvent(localCopyTask);
							}} else {
								// Add the task to the other node's list of future offloaded tasks.
								peerns.futureOffloadTasks.add(task.copy());
								//System.out.println("NOT Sent");

							}}
				}
			break;

		case TO_RUN_LOCAL:
			// The task should be executed locally by this node, which is the node who generated it.
			if (!interruptedLocalTasks.contains(task)) {
				//System.out.println("Executed Local" + this + task.getTaskId());
				futureLocalTasks.add(task);
				// The task should be finished in local by this time.
				task.setState(TaskState.FINISHED_LOCAL);
				task.setTriggerTime(task.getTriggerTime() + task.getDuration());
				trace.addEvent(task);
			} else {
				// log.info("Not running " + task + " locally since it's already
				// executed");
				countNotStartedLocal++;

			}
			break;

		case FINISHED_LOCAL:
			// This is an event telling that at this timestamp the task should
			// have finished the local execution.
			// The task could have finished the execution before this timestamp
			// because of offloading.

			// Check if this task is still in the futureLocalTasts set.
			// If yes, then move it to the finishedLocalTasks and remove the
			// task from futureOffloadTasks set
			// of known nodes where this task has been added.
			if (this.futureRemainBudgetTasks.contains(task)) {
				futureRemainBudgetTasks.remove(task);
			}
			// System.out.println("Finished local: " + this + task.getTaskId());

			if (futureLocalTasks.contains(task) && (!Constants.consumeEnergyForLocal || this.canExecuteLocal(task, task.getTriggerTime()))) {
				if (Constants.consumeEnergyForLocal) {
					//this.usedUseFulJoules += task.getEnergy();
					this.execute(task, task.getTriggerTime()); 
				}
				//System.out.println("Finished Local" + this + task.getTaskId());

				futureLocalTasks.remove(task);
				finishedLocalTasks.add(task);
				// punish every peer who did'nt execute this remotely
				if (constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.FLOPCOIN_THETA || constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.SORT) {
					for (Node n : knownNodes.keySet()) {
						if (knownNodes.get(n).offloadedTasks.contains(task)) {
							// Punish them
							//knownNodes.get(n).trust.interaction(Constants.Interactions.NEGATIVE_GET_BACK_RESULT_I,(task.getEnergy()/Task.MAX_APP_ENERGY), (float)1);
							//System.out.println("Punish: " + n.getId());
							// System.out.println("Negative:" +
							// n.getMinBatteryLevel());

						}
					}
				}
				removeTaskFromFutureOffload(task);

				// log.info("Task: " + task + " finished local execution");
			} else {
				//log.info("Task " + task + " didn't finish local execution, it was solved remotely");
			}
			break;

		default:
			//log.error("Task state not handled: " + task.getState());
			break;
		}

	}

	private void receiveRequestForwardig(Node otherNode, Task task){
		NodeStatistics ns = this.knownNodes.get(otherNode);
		ns.receivedTasks.add(task);

		if( ns.offloadedTasks.contains(task) && !ns.finishedOffloadedTasks.contains(task)){
			ns.offloadedTasks.remove(task);
			ns.finishedOffloadedTasks.add(task);
			if (constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.FLOPCOIN_THETA || constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.SORT) {
				ns.trust.interaction(Constants.Interactions.POSITIVE_GET_BACK_RESULT_III, 1, 1);

			}}
	}
	private void receiveTaskForwarding(Node otherNode, Task task, Trace trace, double current) {
		NodeStatistics ns = this.knownNodes.get(otherNode);

		if(this.receivedTask(task)){
			if (constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.FLOPCOIN_THETA || constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.SORT) {

				//System.out.println(task.getTaskId() + " sending excuse" + this);
				//Mahdieh: we consider this answer can not be interrupted
				double taskStartExecution = Math.max(this.lastBusyTime, task.getTriggerTime());
				Task localCopyTask = task.copy(taskStartExecution + task.calculateResultTxTime(Constants.UL_RATE), this);
				localCopyTask.setState(TaskState.EXCUSE_DELIVERED);
				localCopyTask.setNode1(otherNode);
				this.sendData(task.getAnswerSize(), current);
				otherNode.receiveData(task.getRequestSize(), current);
				trace.addEvent(localCopyTask);
			}

		}else if(!this.receivedTask(task)){
			ns.receivedTasks.remove(task);
			ns.receivedAndFinishedTasks.add(task);
			double max = task.getCreationTime() + task.getMaxAllowedDelay();

			if (this.isOn(current) && (!Constants.BUSY || 
					(!this.isBusy(task.estimateTaskExecutionTime(this), max)) && this.canExecute(task, current))) {

				//System.out.println(task.getTaskId() + " finished Execution" + this);

				// Move the task to the finished set
				ns.receivedAndFinishedTasks.remove(task);
				this.execute(task, current);
				ns.receivedAndDeliveredTasks.add(task);
				task.setNode1(otherNode);
				if (constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.FLOPCOIN_THETA || constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.SORT) {
					ns.selfRespect.interaction(Constants.Interactions.POSITIVE_GET_BACK_RESULT_III, 1, 1);
				}
				double taskStartExecution = Math.max(this.lastBusyTime, task.getTriggerTime());
				this.lastBusyTime = Math.max(this.lastBusyTime, task.getTriggerTime()) + task.estimateTaskExecutionTime(this);
				
				Task localCopyTask = task.copy(taskStartExecution + task.estimateTaskExecutionTime(this), otherNode);
				localCopyTask.setState(TaskState.TASK_FINISHED);
				localCopyTask.setNode1(this);
				trace.addEvent(localCopyTask);

			}
		} 

	}

	public boolean receivedTask(Task task) {

		if(this.futureLocalTasks.contains(task))
			return true;
		for(NodeStatistics ns: this.knownNodes.values()){
			if( ns.receivedAndDeliveredTasks.contains(task)){
				return true;
			}
		}
		return false;
	}

	/**
	 * Look if this task is in the futureOffloadTasks of some node. If yes,
	 * remove it from there since there is no need to offload it anymore.
	 * 
	 * @param task
	 */
	private void removeTaskFromFutureOffload(Task task) {
		for (Node otherNode : knownNodes.keySet()) {
			NodeStatistics ns = knownNodes.get(otherNode);
			if (ns.futureOffloadTasks.contains(task)) {
				//System.out.println("Task " + task + " already finished, no need to offload to node " + otherNode);
				ns.futureOffloadTasks.remove(task);
			}
		}
	}

	private List<Node> chooseNodesForOffload(Task task) {
		if (constants.get(Constants.KEY.SIMULATION_TYPE).intValue() == Constants.RANDOM)
			return chooseNodesRandomly(task);
		else if (constants.get(Constants.KEY.SIMULATION_TYPE).intValue() == Constants.GREEDY)
			return chooseNodesGreedy(task);
		else if (constants.get(Constants.KEY.SIMULATION_TYPE).intValue() == Constants.FLOPCOIN_THETA)
			return chooseNodesIntelligently(task);
		else if (constants.get(Constants.KEY.SIMULATION_TYPE).intValue() == Constants.SORT)
			return chooseNodesIntelligently(task);
		return null;
	}





	/**
	 * Chooses the nodes which have the higher value of theta
	 * 
	 * @return
	 */

	private List<Node> chooseNodesIntelligently(Task task) {

		List<Node> nodes = new LinkedList<>();
		// Recieve every node in response and store htat in a map consisting
		// <Node, Response> pair
		Map<Node, Response> auction = new HashMap<Node, Response>();
		for (Node n : knownNodes.keySet()) {
			NodeStatistics ns = knownNodes.get(n);
			if (ns.isInContact()){
				Response resTemp= n.giveOffResponse(task, this, task.getTriggerTime());
				this.sendData((long) Task.REQ_SIZE, task.getTriggerTime());
				n.receiveData((long) Task.REQ_SIZE, task.getTriggerTime());
				n.sendData((long) Task.RESP_SIZE, task.getTriggerTime());
				this.receiveData((long) Task.RESP_SIZE, task.getTriggerTime());
				
				double wieghtPropose = (task.getApplication() == Framework.TASK_OFF) ? (task.getEnergy()/Task.MAX_APP_ENERGY) : 1;
				if(resTemp.answer.equals(Constants.Commands.YES)) {
					auction.put(n, resTemp);
				}else if (resTemp.answer.equals(Constants.Commands.NO)){
					if(resTemp.mass>=0.25)
						ns.trust.interaction(Constants.Interactions.NEGATIVE_NOT_HONEST_IV,wieghtPropose, (float) resTemp.mass);
				}else if (resTemp.answer.equals(Constants.Commands.BAD_REPUTATION)){
					ns.trust.interaction(Constants.Interactions.NEGATIVE_NOT_HONEST_IV, wieghtPropose, (float) resTemp.mass);
				}
			}
		}
		// Sort the map consisting the nodes participated in the Auction
		Map<Node, NodeStatistics> participatedNodes = new HashMap<Node, NodeStatistics>();
		for (Node n : auction.keySet()) {
			if (constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.SORT) {
				this.askAll(n, task.getTriggerTime());
			}
			participatedNodes.put(n, knownNodes.get(n));
		}

		//System.out.println("size: " + auction.size() + ":" +knownNodes.size());
		// Sort the
		List<Map.Entry<Node, NodeStatistics>> sorted = new LinkedList<Map.Entry<Node, NodeStatistics>> (participatedNodes.entrySet());
		// Defined Custom Comparator here
		Collections.sort(sorted, new Comparator<Map.Entry<Node, NodeStatistics>>() {

			public int compare(Map.Entry<Node, NodeStatistics> n1, Map.Entry<Node, NodeStatistics> n2) {

				float node1Metric = 0;
				float node2Metric = 0;
				//System.out.println("Metric" + n1.getKey().getId() + ":" + n2.getKey().getId());
				node1Metric = ((Map.Entry<Node, NodeStatistics>) (n1)).getValue().trust.getMetric();
				node2Metric = ((Map.Entry<Node, NodeStatistics>) (n2)).getValue().trust.getMetric();
				if (node1Metric == node2Metric)
					return 0;
				else if (node1Metric > node2Metric)
					return -1;
				else
					return 1;
			}
		});

		// for (int i = 0; i < sorted.size(); i++) {
		// Map.Entry<Node, NodeStatistics> tmp = (Entry<Node, NodeStatistics>)
		// sorted.get(i);
		// System.out.print(tmp.getKey().toString() +
		// tmp.getValue().trust.getMetric() + "\t");
		// }
		// System.out.println("Finished");

		Iterator<Map.Entry<Node, NodeStatistics>> iterator = sorted.iterator();
		float thetaSum = 1;
		// int neighborNum = (int) ((float)
		// constants.get(KEY.NR_CHOSE_NEIGHBORS).intValue() * sorted.size() /
		// 100);
		while (iterator.hasNext()  && (1 - thetaSum) < Constants.THETA_THRESHOLD
				// && nodes.size() < neighborNum
				&& nodes.size() < participatedNodes.size()) {
			Map.Entry<Node, NodeStatistics> temp = iterator.next();

			if (!nodes.contains(temp.getKey())) {
				nodes.add(temp.getKey());
				thetaSum *= (1 - temp.getValue().trust.getMetric());
				// System.out.println("Add:" + temp.getKey() +
				// temp.getValue().theta.getMetric() + ":"
				// + auction.get(temp.getKey()).getRequiredFlopCoins());
			}
		}

		if ((1 - thetaSum) < Constants.THETA_THRESHOLD ) {
			task.setTaskRemainingBudget(1);
			task.setUnsuccessProbability(thetaSum);
			// System.out.println("Id:" + task.getTaskId() + " Prob:" + (1 -
			// thetaSum));
		} else {
			task.setTaskRemainingBudget(0);
		}
		// System.out.println(sorted.size() + ":" + nodes.size());

		Simulation.neighborNum += nodes.size();
		Simulation.neighborNumTimes++;

		//System.out.println("**" + auction.size() + "::" + sorted.size() +
		//"::" + nodes.size());
		return nodes;
	}

	/**
	 * Here we assume each node considers its battery level to be bigger than a
	 * thresold and responds honestly
	 * 
	 * @param task
	 * @return
	 */
	private Response giveOffResponse(Task task, Node otherNode, double current) {
		if(task.getApplication() == Framework.MES_FORW){
			this.receiveRequestForwardig(otherNode, task);
		}
		//System.out.println("Request ");
		NodeStatistics ns = this.knownNodes.get(otherNode);
		double max = task.getCreationTime() + task.getMaxAllowedDelay();
		double realWeight = 0;

		if (this.appraiser(otherNode) && this.isOnForOffloading(current) && this.canExecute(task, current) && !this.isBusy(task.estimateTaskExecutionTime(this), max) 
				&& this.appraiser(otherNode)) {
			realWeight = 1;
			//System.out.println(id + ":" + "YES" + ":" + realWeight);

			return new Response(Commands.YES, realWeight);
		}else{
			if(!this.appraiser(otherNode)){
				realWeight = (otherNode.getMinBatteryLevel() - this.getMinBatteryLevel()) <= 0 ? 0 : ((otherNode.getMinBatteryLevel() - this.getMinBatteryLevel())/100);
				//realWeight = 0;
				//System.out.println(id + ":" + "APPRAISE" + ":" + realWeight);
				return new Response (Commands.BAD_REPUTATION, realWeight);
			}
			else if(this.isBusy(task.estimateTaskExecutionTime(this), max)){
				realWeight = 1;
				//System.out.println(id + ":" + "BUSY" + ":" + realWeight);
				return new Response (Commands.BUSY, realWeight);
			}else{

				if(!(this.isOnForOffloading(current) && this.canExecute(task, current))){
					//System.out.println(id + ":" + "NO" + ":" + realWeight);
					//realWeight = (otherNode.getMinBatteryLevel() - this.getMinBatteryLevel()) <= 0 ? 0 : ((otherNode.getMinBatteryLevel() - this.getMinBatteryLevel())/100);
					realWeight = this.batteryLevel.getBattery(current)/100;
					return new Response (Commands.NO, realWeight);
				}
				
				ns.selfRespect.interaction(Constants.Interactions.NEGATIVE_NOT_HONEST_IV,
						(task.estimateEnergyConsumption(this)/Task.MAX_APP_ENERGY), realWeight);
			}
		}
		return null;
	}

	private boolean appraiser(Node node){
		NodeStatistics ns = this.knownNodes.get(node);
		double reputation = ns.trust.getMetric();
		double baseMetric = ns.trust.getHisotry()==0 ? 0 : 0.001;
		if(Constants.RESPECT_ALG==Constants.RESPECT.LOWER_THAN_ZERO){
			return reputation >=baseMetric;
		}else if(Constants.RESPECT_ALG==Constants.RESPECT.LOWER_THAN_YOU){
			return reputation >= ns.selfRespect.getMetric();
		}
		return false;
	}

	//	/**
	//	 * Here we assume each node considers its battery level to be bigger than a
	//	 * thresold and responds honestly
	//	 * 
	//	 * @param task
	//	 * @return
	//	 */
	//	public Bid giveBid(Task task) {
	//		System.out.println("Request ");
	//		double max = task.getCreationTime() + task.getMaxAllowedDelay();
	//		if (this.isOn() && this.canExecute(task) && !this.isBusy(task.estimateTaskExecutionTime(this), max)) {
	//			System.out.println("Give Bid");
	//			return new Bid(this.getCpuPower(), (int) task.estimateEnergyConsumption(this));
	//		}
	//		return null;
	//	}

	/**
	 * 
	 * @return
	 */
	public boolean canExecute(Task task, double current) {
		float consumedPercent = (float) (task.estimateEnergyConsumption(this) / this.joulesPerPercent);
		return this.batteryLevel.getBattery(current) - consumedPercent > this.batteryLevelMin;
	}

	/**
	 * 
	 * @return
	 */
	public boolean canExecuteLocal(Task task, double current) {
		float consumedPercent = (float) (task.estimateEnergyConsumption(this) / this.joulesPerPercent);
		return this.batteryLevel.getBattery(current) - consumedPercent > 10;
	}

	/**
	 * Greedy choose 3 nodes from the ones this node is connected to now.
	 * 
	 * @return
	 */
	private List<Node> chooseNodesGreedy(Task task) {
		List<Node> nodes = new LinkedList<>();

		Random r = new Random();
		Vector<Node> inContact = new Vector<Node>();
		for (Node n : knownNodes.keySet()) {
			NodeStatistics ns = knownNodes.get(n);
			if (ns.isInContact()) {
				inContact.add(n);
			}
		}

		for ( Node tmp : inContact) {
			if (!nodes.contains(tmp))
				nodes.add(tmp);
		}

		task.setTaskRemainingBudget(1);
		Simulation.neighborNum += nodes.size();
		Simulation.neighborNumTimes++;

		return nodes;
	}

	/**
	 * Randomly choose 3 nodes from the ones this node has previously met. Sort
	 * the nodes based on their cpu power.
	 * 
	 * @return
	 */
	private List<Node> chooseNodesRandomly(Task task) {
		List<Node> nodes = new LinkedList<>();

		Vector<Node> inContact = new Vector<Node>();
		for (Node n : knownNodes.keySet()) {
			NodeStatistics ns = knownNodes.get(n);
			inContact.add(n);
		}

		// Node[] tempNodes = inContact.toArray(new Node[inContact.size()]);
		for ( Node tmp : inContact) {
			if (!nodes.contains(tmp))
				nodes.add(tmp);
		}
		// System.out.println("^^^^^^:" + nodes.size());
		task.setTaskRemainingBudget(0);
		Simulation.neighborNum += nodes.size();
		Simulation.neighborNumTimes++;
		return nodes;
	}

	/**
	 * Executes this task and comsumes the required resources
	 * 
	 * @param task
	 */
	private void execute(Task task, double current) {
		float consumedPercent = (float) (task.estimateEnergyConsumption(this) / this.joulesPerPercent);
		//this.usedJoules +=task.estimateEnergyConsumption(this);
		// System.out.println("::::::::::::" + this.batteryLevel + ":" +
		// consumedPercent);
		this.batteryLevel.consumeBattery(current, consumedPercent);
		assert(batteryLevel.getBattery(current) >= 0);
		// System.out.println("::::::::::::" + this.batteryLevel);
	}

	/**
	 * Some other node is giving a task for offload execution to this node. The
	 * original node or some other node may have executed this task in the
	 * meantime, but don't care, once you have the task you have to execute it.
	 * 
	 * @param fromNode
	 *            The node giving the task for offload.
	 * @param task
	 *            The task received for offload execution.
	 */
	private void receiveOffloadedTask(Node fromNode, Task task, Trace trace, double current) {

		//log.info("Received task " + task + " from node " + fromNode);
		NodeStatistics ns = knownNodes.get(fromNode); // Can't be null, there
		ns.receivedTasks.add(task);
		double max = task.getCreationTime() + task.getMaxAllowedDelay();

		if (this.isOn(current) && (!Constants.BUSY || 
				(!this.isBusy(task.estimateTaskExecutionTime(this), max)) && this.canExecute(task, current))) {

			int sum = 0;
			for (NodeStatistics ntemp : knownNodes.values())
				sum += ntemp.receivedTasks.size();

			System.out.println(task.getTaskId() + " finished Execution" + this);

			// Move the task to the finished set
			ns.receivedTasks.remove(task);
			this.execute(task, current);
			ns.receivedAndFinishedTasks.add(task);
			task.setNode1(fromNode);
			if (constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.FLOPCOIN_THETA || constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.SORT) {
				ns.selfRespect.interaction(Constants.Interactions.POSITIVE_GET_BACK_RESULT_III,
						task.estimateEnergyConsumption(this)/Task.MAX_APP_ENERGY, 1);
			}
			double taskStartExecution = Math.max(this.lastBusyTime, task.getTriggerTime());
			this.lastBusyTime = Math.max(this.lastBusyTime, task.getTriggerTime()) + task.estimateTaskExecutionTime(this);

			Task localCopyTask = task.copy(taskStartExecution + task.estimateTaskExecutionTime(this), fromNode);
			localCopyTask.setState(TaskState.TASK_FINISHED);
			localCopyTask.setNode1(this);
			trace.addEvent(localCopyTask);

		} else {
			if (Constants.BUSY) {
				/*
				 * ns.receivedTasks.remove(task); this.execute(task);
				 * ns.receivedAndFinishedTasks.add(task);
				 */
			}
		}
	}

	/**
	 * Receive the answer of an offloaded task from another node.
	 * 
	 * @param fromNode
	 * @param task
	 * @return If the result of the offloaded task is useful or not
	 */
	private void receiveOffloadedResult(Node fromNode, Task task) {
		// log.info("Received result of task " + task + " from node " +
		// fromNode);
		//System.out.println(task.getTaskId() + " :Receive Result " + this);


		NodeStatistics ns = knownNodes.get(fromNode);
		ns.offloadedTasks.remove(task);
		ns.finishedOffloadedTasks.add(task);

		// Add this task to the interruptedLocalTasks so that future TO_RUN_LOCAL events for this task
		// will not execute it. Do this only if the task was not already finished in local. In this case
		// we give the peer its required flopCoins!
		if (!this.finishedLocalTasks.contains(task)) {
			if (!futureLocalTasks.contains(task) || Constants.AFTER_LOCAL == 1) {
				if (constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.FLOPCOIN_THETA || constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.SORT) {

					ns.trust.interaction(Constants.Interactions.POSITIVE_GET_BACK_RESULT_III, (task.getEnergy()/Task.MAX_APP_ENERGY), 1);
					//System.out.println("POSITIVE");
					if (Constants.FLOPCOIN_RECOM == 1 && constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.FLOPCOIN_THETA) {
						informOthersAboutMyExperience();
					}
					//this.giveMoney(fromNode, task.estimateEnergyConsumption(fromNode)); // Give Money
				}
				// Remove this task from the futureOffload set of other nodes.
				removeTaskFromFutureOffload(task);
			}
			this.usedUseFulJoules += /*task.getRequestSize() * Task.JOULES_PER_BYTE_SEND + task.getAnswerSize() * Task.JOULES_PER_BYTE_RECEIVE +*/
					task.getEnergy(); 
			this.interruptedLocalTasks.add(task);
		}

		// Remove this task from the futureLocalTasks set if it's there
		if (futureLocalTasks.contains(task) && Constants.AFTER_LOCAL == 1) {
			this.futureLocalTasks.remove(task);
		}

		if (this.futureRemainBudgetTasks.contains(task))
			this.futureRemainBudgetTasks.remove(task);


	}



	/**
	 * Informs others about the interaction history of itself with other nodes
	 */
	private void informOthersAboutMyExperience() {

		Map<Node, BetaDist> interactHistory;
		for (Node tmp : knownNodes.keySet()) {
			if (knownNodes.get(tmp).isInContact()) {

				interactHistory = new HashMap<Node, BetaDist>();
				for (Node node : knownNodes.keySet()) {
					if (!node.equals(tmp)) {
						interactHistory.put(node, (BetaDist) knownNodes.get(node).trust);
					}
				}

				tmp.receieveOtherNodesExperience(this, interactHistory);
			}
		}
	}

	/**
	 * Receives the interaction history of the third party node that has just
	 * had an interaction with that specified node
	 * 
	 * @param thirdParty
	 * @param interactHistory
	 */
	public void receieveOtherNodesExperience(Node thirdParty, Map<Node, BetaDist> interactHistory) {

		// save the interaction history of this node
		knownNodes.get(thirdParty).recommendation = interactHistory;
		// Update the Theta parameter of every known node based on this
		// recommendation
		for (Node otherNode : interactHistory.keySet()) {
			if (knownNodes.containsKey(otherNode)) {
				updateTheta(otherNode);
			}
		}
	}

	/**
	 * Updates the theta parameter of this nodes based on the recommendation
	 * received from the other peering nodes
	 * 
	 * @param otherNode
	 */
	private void updateTheta(Node otherNode) {
		float alphaSigma = 0;
		float betaSigma = 0;
		float expectedsigma = 0;
		for (Node node : knownNodes.keySet()) {
			if (!node.equals(otherNode) && knownNodes.get(node).recommendation != null
					&& knownNodes.get(node).recommendation.containsKey(otherNode)) {
				// Computing the Sigma in Eq. 19
				alphaSigma += ((BetaDist) knownNodes.get(node).trust).getExpectedValue()
						* knownNodes.get(node).recommendation.get(otherNode).getDirectAlpha();
				betaSigma += ((BetaDist) knownNodes.get(node).trust).getExpectedValue()
						* knownNodes.get(node).recommendation.get(otherNode).getDirectBeta();
				expectedsigma += ((BetaDist) knownNodes.get(node).trust).getExpectedValue();
			}
		}

		// Set the updated Alpha and beta
		((BetaDist) knownNodes.get(otherNode).trust).setAlpha(
				(((BetaDist) knownNodes.get(otherNode).trust).getDirectAlpha() + alphaSigma) / (1 + expectedsigma));
		((BetaDist) knownNodes.get(otherNode).trust).setBeta(
				(((BetaDist) knownNodes.get(otherNode).trust).getDirectBeta() + betaSigma) / (1 + expectedsigma));
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the cpuPower
	 */
	public int getCpuPower() {
		return cpuPower;
	}

	/**
	 * @param cpuPower
	 *            the cpuPower to set
	 */
	public void setCpuPower(int cpuPower) {
		this.cpuPower = cpuPower;
	}

	@Override
	public int compareTo(Node otherNode) {
		if (this.id < otherNode.getId())
			return -1;
		else if (this.id > otherNode.getId())
			return 1;
		else
			return 0;
	}

	@Override
	public int hashCode() {
		// log.debug("Getting hashCode: " + this);
		return id;
	}

	@Override
	public boolean equals(Object o) {

		// log.debug("Checking for equality: " + this + " and " + (Node)o);

		if (this == o)
			return true;
		if (o == null)
			return false;

		final Node other = (Node) o;
		return this.id == other.id;
	}

	@Override
	public String toString() {
		return "[" + this.id + "]";
	}

	/**
	 * To sort Nodes in a list based on their theta parameter.
	 * 
	 * @author Mahdieh
	 */
	/*
	 * private class NodeComparator {
	 * 
	 * @Override public int compare(Object> n1, Object n2) {
	 * 
	 * float node1Metric = 0; float node2Metric = 0; switch
	 * (constants.get(KEY.SIMULATION_TYPE).intValue()){ case
	 * Constants.FLOPCOIN_THETA : node1Metric = (Map.Entry<Node,
	 * NodeStatistics>) n1.getValue().theta.getMetric(); node2Metric =
	 * (Map.Entry<Node, NodeStatistics>) n2.getValue().theta.getMetric(); break;
	 * case Constants.FLOPCOIN_CPU : node1Metric = (Map.Entry<Node,
	 * NodeStatistics>) n1.getKey().getCpuPower(); node2Metric =
	 * (Map.Entry<Node, NodeStatistics>) n2.getKey().getCpuPower();
	 * //System.out.println(node1Metric + ":" + node2Metric); break; case
	 * Constants.FLOPCOIN_THETA_CPU : //node2Metric =
	 * n1.getValue().theta.getMetric(); //node1Metric =
	 * n2.getValue().theta.getMetric(); break; } if (node1Metric == node2Metric)
	 * return 0; else if (node1Metric > node2Metric) return -1; else return 1; }
	 * }
	 */

	/**
	 * Keep statistics of meeting with other node: contact duration, contact
	 * number, intercontact, etc.
	 * 
	 * @author sokol
	 *
	 */
	private class NodeStatistics {

		private Reputation trust; // Theta parameter representing the behavior
		private Reputation selfRespect;
		// of
		// each node
		private Map<Node, BetaDist> recommendation; // The interaction hisotry
		// of this node with all
		// other nodes
		private List<Double> meetTimestamps; // List of timestamp meetings
		private List<Double> contactDuration; // List of contact durations

		// Set of tasks to be offloaded
		private Set<Task> futureOffloadTasks;
		// Set of tasks that we have started to offload but don't know
		// if the transmission is accomplished or not.
		private Set<Task> sendingOffloadTasks;
		// Set of tasks already offloaded
		private Set<Task> offloadedTasks;
		// Set of tasks offloaded and finished
		private Set<Task> finishedOffloadedTasks;

		// Received tasks for offloading
		private Set<Task> receivedTasks;
		// Received and finished tasks for offloading
		private Set<Task> receivedAndFinishedTasks;
		// Received, finished, and delivered tasks for offloading
		private Set<Task> receivedAndDeliveredTasks;
		// Received, finished, delivered and payed tasks for offloading
		private Set<Task> paidReceivedAndDeliveredTasks;

		public NodeStatistics() {

			if (constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.SORT) {
				trust = new SORT();
				selfRespect = new SORT();
			} else if (constants.get(KEY.SIMULATION_TYPE).intValue() == Constants.FLOPCOIN_THETA) {
				trust = new BetaDist();
				selfRespect = new BetaDist();
			}

			meetTimestamps = new LinkedList<>();
			contactDuration = new LinkedList<>();

			futureOffloadTasks = new HashSet<>();
			sendingOffloadTasks = new HashSet<>();
			offloadedTasks = new HashSet<>();
			finishedOffloadedTasks = new HashSet<>();

			receivedTasks = new HashSet<>();
			receivedAndFinishedTasks = new HashSet<>();
			receivedAndDeliveredTasks = new HashSet<>();
			paidReceivedAndDeliveredTasks = new HashSet<>();

		}

		/**
		 * If the list of meet timestamps is longer than that of contactDuration
		 * it means that there have been a meet without a leave yet.
		 * 
		 * @return true if the node keeping statistics is in contact with this
		 *         node.
		 */
		public boolean isInContact() {
			// return true;
			return meetTimestamps.size() > contactDuration.size();
		}

		public void addMeet(double time) {
			meetTimestamps.add(time);
		}

		public void addLeave(double time) {
			contactDuration.add(time - meetTimestamps.get(meetTimestamps.size() - 1));
			assert contactDuration.size() == meetTimestamps.size();
		}

		public double getLastContactDuration() {
			return contactDuration.get(contactDuration.size() - 1);
		}

		/**
		 * A new task is assigned for offloading to the node.
		 * 
		 * @param task
		 */
		public void addFutureTask(Task task) {
			futureOffloadTasks.add(task);
		}

		/**
		 * The task is sent to the other node for offloading. This happens when
		 * n1 and n2 are meeting. The task is removed from the futureOffloadTask
		 * set.
		 * 
		 * @param task
		 */
		public void addOffloadedTask(Task task) {
			// Add the task to the offloadedTasks set
			offloadedTasks.add(task);
		}

		public void addFinishedOffloadedTask(Task task) {
			finishedOffloadedTasks.add(task);
		}

		/**
		 * A new task was received for offloading.
		 * 
		 * @param task
		 */
		public void addReceivedTask(Task task) {
			receivedTasks.add(task);
		}

		public void addReceivedAndFinishedTask(Task task) {
			receivedAndFinishedTasks.add(task);
		}

		public void addReceivedAndDeliveredTask(Task task) {
			receivedAndDeliveredTasks.add(task);
		}
	}

	/**
	 * To sort Nodes in a list based on their CPU power.
	 * 
	 * @author sokol
	 */
	private class NodeCpuComparator implements Comparator<Node> {

		@Override
		public int compare(Node n1, Node n2) {
			if (n1.cpuPower == n2.cpuPower)
				return 0;
			else if (n1.cpuPower > n2.cpuPower)
				return 1;
			else
				return -1;
		}
	}

	/**
	 * @return List containing the number of contacts this node has had with all
	 *         the other nodes with id > this.id (so that we don't count twice).
	 */
	public List<Integer> getContactNumberAmongCouples() {
		List<Integer> contactNr = new LinkedList<>();
		for (Node n : knownNodes.keySet()) {
			if (this.id < n.id) {
				contactNr.add(knownNodes.get(n).meetTimestamps.size());
			}
		}
		return contactNr;
	}

	/**
	 * @return List with all meet timestamps this node has had with the other
	 *         nodes with id > this.id (so that we don't count twice).
	 */
	public List<Double> getMeetTimestamps() {
		List<Double> meetTimestamps = new LinkedList<>();
		for (Node n : knownNodes.keySet()) {
			if (this.id < n.id) {
				meetTimestamps.addAll(knownNodes.get(n).meetTimestamps);
			}
		}
		return meetTimestamps;
	}

	/**
	 * @return A list containing the contact durations this node has had with
	 *         all the other nodes with id > this.id (so that we don't count
	 *         twice).
	 */
	public List<Double> getContactDurationAmongCouples() {
		List<Double> contactDur = new LinkedList<>();
		for (Node n : knownNodes.keySet()) {
			if (this.id < n.id) {
				contactDur.addAll(knownNodes.get(n).contactDuration);
			}
		}
		return contactDur;
	}

	/**
	 * @return A list containing the contact durations this node has had with
	 *         all the other nodes with id > this.id (so that we don't count
	 *         twice).
	 */
	public List<Double> getIntercontactsAmongCouples() {
		List<Double> intercontacts = new LinkedList<>();
		for (Node n : knownNodes.keySet()) {
			if (this.id < n.id) {
				NodeStatistics ns = knownNodes.get(n);
				for (int i = 0; i < ns.meetTimestamps.size() - 1; i++)
					intercontacts.add((ns.meetTimestamps.get(i + 1) - ns.meetTimestamps.get(i)));
			}
		}
		return intercontacts;
	}

	/**
	 * @return Map containing the tasks' timestamp and number of nodes in
	 *         contact when the task had to be executed.
	 */
	public Map<Double, Integer> getNrNodesInContactOnTask() {
		return statNrNodesInRangeOnTask;
	}

	/**
	 * @return Number of tasks this node has executed in local.
	 */
	public int nrExecutedLocalTasks() {
		return finishedLocalTasks.size();
	}

	/**
	 * @return Number of tasks this node was planning to run locally but a
	 *         remote result came before executing the task locally.
	 */
	public int nrInterruptedLocalTasks() {
		return interruptedLocalTasks.size();
	}

	/**
	 * @return Number of tasks this node planned to run in local but the
	 *         simulation finished before they could have been executed locally
	 *         or remotely (remember that if a task is finished remotely then it
	 *         is removed from this set).
	 */
	public int nrFutureLocalTasks() {
		return futureLocalTasks.size();
	}

	/**
	 * @return Number of offload requests this node has successfully made but no
	 *         result has arrived from remote yet. The same task could be sent
	 *         to more than one node, this why the number of requests is
	 *         different than the number of tasks offloaded.
	 */
	public int nrOffloadRequests() {
		int count = 0;
		for (NodeStatistics ns : knownNodes.values()) {
			count += ns.offloadedTasks.size();
		}
		return count;
	}

	/**
	 * @return Number of offload requests this node has successfully received
	 *         and accepted
	 */
	public int nrOffloadRequestsReceived() {
		int count = 0;
		for (NodeStatistics ns : knownNodes.values()) {
			count += ns.receivedTasks.size();
			count += ns.receivedAndDeliveredTasks.size();
			count += ns.receivedAndFinishedTasks.size();
			count += ns.paidReceivedAndDeliveredTasks.size();
		}
		return count;
	}

	/**
	 * @return Number of offload results this node has received. The result of
	 *         the same task received from more than one node is counted more
	 *         than once.
	 */
	public int nrOffloadRresultsReceived() {
		int count = 0;
		for (NodeStatistics ns : knownNodes.values()) {
			count += ns.finishedOffloadedTasks.size();
		}
		return count;
	}

	/**
	 * 
	 * @return Set containing the tasks this node has offloaded and received
	 *         result.
	 */
	public Set<Task> getSetUniqueFinishedOffloadedTasks() {
		Set<Task> uniqueSet = new HashSet<>();
		for (NodeStatistics ns : knownNodes.values()) {
			uniqueSet.addAll(ns.finishedOffloadedTasks);
		}
		return uniqueSet;
	}

	/**
	 * @return Number of tasks this node is still planning to offload.
	 */
	public int nrFutureOffloadTasks() {
		int count = 0;
		for (NodeStatistics ns : knownNodes.values()) {
			count += ns.futureOffloadTasks.size();
		}
		return count;
	}

	/**
	 * @return Number of tasks that are in the middle of offload transmission.
	 */
	public int nrSendingOffloadTasks() {
		int count = 0;
		for (NodeStatistics ns : knownNodes.values()) {
			count += ns.sendingOffloadTasks.size();
		}
		return count;
	}

	public float getBatteryLevel(double current) {
		return (float) this.batteryLevel.getBattery(current);
	}

	public float getMinBatteryLevel() {
		return this.batteryLevelMin;
	}

	public boolean doYouKnow(Node aim) {
		return (knownNodes.containsKey(aim) && knownNodes.get(aim).trust.getHisotry() >0);
	}

	public Recommendation getOpinionAbout(Node aim) {
		assert doYouKnow(aim) == true;
		return knownNodes.get(aim).trust.recommendation();
	}

	public float getInteractionHistory(Constants.Interactions interactionType, Node aim) {
		//assert doYouKnow(aim) == true;
		return knownNodes.get(aim).trust.getInteractionNum(interactionType);
	}

	public void askAll(Node aim, double current) {
		double ecb = 0;
		double eib = 0;
		double er = 0;
		double betaecb = 0;
		double betaer = 0;
		double shMean = 0;
		Map<Node, Recommendation> recoms = new HashMap<Node, Recommendation>();
		//System.out.println("askAll" + this + aim);
		for (Node tmp : knownNodes.keySet()) {
			if (knownNodes.get(tmp).trust.getMetric()>0 && !aim.equals(tmp) && knownNodes.get(tmp).isInContact() && tmp.doYouKnow(aim)) {
				SORTRecom recom = (SORTRecom) tmp.getOpinionAbout(aim);
				double recomTrust = ((SORT) knownNodes.get(tmp).trust).getRecomTrust();
				// System.out.println(this.toString() + tmp.toString() +
				// knownNodes.get(tmp).trust.getMetric());
				if (recom.isValid()) {
					this.sendData((long) Task.REC_ASK, current);
					tmp.receiveData((long) Task.REC_ASK, current);
					tmp.sendData((long) Task.REC_ANS, current);
					this.receiveData((long) Task.REC_ASK, current);
					recoms.put(tmp, recom);
					ecb += recomTrust * recom.getRsh() * recom.getRcb();
					eib += recomTrust * recom.getRsh() * recom.getRib();
					// System.out.print(tmp + "Recom:" + recom.getRr() + ":" + recomTrust + ":" + recom.getRrh());
					er += recomTrust * recom.getRrh() * recom.getRr();
					betaecb += recomTrust * recom.getRsh();
					betaer += recomTrust * recom.getRrh();
					// System.out.println("\t" + betaer + ":" + er);
					shMean += recom.getRsh();
				}
			}
		}

		if (recoms.isEmpty()) {
			((SORT) knownNodes.get(aim).trust).setReputation(0);
			((SORT) knownNodes.get(aim).trust).setReputationEta(0);
			return;
		} else {

			// calculate
			ecb = (betaecb == 0) ? 0 : ecb / betaecb;
			eib = (betaecb == 0) ? 0 : eib / betaecb;
			//System.out.println(aim + "er:" + er + ":ecb" +ecb + ":" +  betaer);
			er = (betaer == 0) ? 0 : er / betaer;
			shMean = shMean / recoms.size();
			// System.out.println("Params:" + ecb + ":" + eib + ":" + er);
			((SORT) knownNodes.get(aim).trust).setReputation((shMean / Constants.maxSizeForSORT) * (ecb - eib / 2)
					+ (1 - (shMean / Constants.maxSizeForSORT)) * er);
			((SORT) knownNodes.get(aim).trust).setReputationEta(recoms.size());

			double y = (shMean / Constants.maxSizeForSORT) * (ecb - eib / 2)
					+ (1 - (shMean / Constants.maxSizeForSORT)) * er;
			//System.out.println("Params:" + ecb + ":" + eib + ":" + er + ":" + shMean + ":" + y);
			//assert y >= 0;
			// calculate difference
			double recSatisfact = 0;
			double recWeigh = 0;
			double diffc, diffb, diffr;
			// computing the differences
			for (Node recommender : recoms.keySet()) {
				SORTRecom recom = (SORTRecom) recoms.get(recommender);
				diffc = (ecb == 0) ? (1 - recom.getRcb()) : 1 - Math.abs(recom.getRcb() - ecb) / ecb;
				diffb = (eib == 0) ? (1 - recom.getRib()) : 1 - Math.abs(recom.getRib() - eib) / eib;
				diffr = (er == 0) ? (1 - recom.getRr()) : 1 - Math.abs(recom.getRr() - er) / er;
				// System.out.println(recommender + "diffc" + er + "diffb" +
				// recom.getRr() + "diffr" + diffr);
				recSatisfact = (diffc + diffb + diffr) / 3;
				recWeigh = (shMean / Constants.maxSizeForSORT) * ((double) recom.getRsh() / Constants.maxSizeForSORT)
						+ (1 - (shMean / Constants.maxSizeForSORT))
						* ((double) recom.getRrh() / Constants.maxSizeForSORT);
				// assert recSatisfact >= 0;
				// assert recWeigh >= 0;
				((SORT) knownNodes.get(recommender).trust).recommInteraction(recSatisfact, recWeigh);
			}
		}

	}

	public void charge() {
		this.batteryLevel.Charge();

	}

	public Set<Task> getLocalTasks() {
		return this.futureLocalTasks;
	}

	public boolean receivedMessage(Task tsk) {
		if(this.futureLocalTasks.contains(tsk))
			return true;
		for(NodeStatistics ns: this.knownNodes.values()){
			if( ns.receivedAndDeliveredTasks.contains(tsk) || ns.receivedAndFinishedTasks.contains(tsk)
					|| ns.receivedTasks.contains(tsk)){
				return true;
			}
		}
		return false;
	}

	public void setBattery(Battery battery) {
		this.batteryLevel = battery;
		
	}

	public Battery getBattery(){
		return this.batteryLevel;
	}
	public int getBatteryModel(){
		return this.batteryLevel.getType();
	}

	public float getUsedBattery() {
		return this.usedJoules;
	}

	public float getUsefulBattery() {
		return this.usedUseFulJoules;
	}
	

}
