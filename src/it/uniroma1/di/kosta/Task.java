package it.uniroma1.di.kosta;

import it.uniroma1.di.kosta.Constants.Dataset;
import it.uniroma1.di.kosta.Constants.Framework;
import it.uniroma1.di.kosta.Constants.KEY;

/**
 * Represents a task that can be offloaded to nearby nodes.
 * 
 * @author sokol
 *
 */
public class Task extends Event {

	public static int lastTaskId = 0;
	private int taskId = 0;
	private int taskUsedBudject = 0;
	private float unSuccessProbability = 0;
	private Framework taskType;

	public enum TaskState {
		TO_RUN, TO_OFFLOAD, OFFLOADED, TO_RUN_LOCAL, FINISHED_LOCAL, TASK_DELIVERED, RESPONSE_DELIVERED, TASK_FINISHED, TASK_PUNISH, EXCUSE_DELIVERED,
	}

	// public static final long MIN_DURATION = 1 * 60; // in s
	// public static final long MAX_DURATION = 2 * 60; // in s
	//
	// public static final int MIN_REQ_SIZE = 5; // in bytes
	// public static final int MAX_REQ_SIZE = 1 * 1024; // in bytes
	//
	// public static final int MIN_ANS_SIZE = 5; // in bytes
	// public static final int MAX_ANS_SIZE = 1 * 1024; // in bytes

	// public static float JOULES_PER_SEC = (float) 10; // how many joules is
	// consumed per second

	public static final long[] appDurations = new long[] { (long) 250, (long) 500 };
	public static final long[] appEnergy = new long[] {(long) 1000, (long) 500};
	public static final long[] appSendDataSize = new long[] { 100000, 1000, 100000, 1000000, 10000000, 1000, 2000000 };
	public static final long[] appReceiveDataSize = new long[] { 1000, 122880, 1000, 1000, 1000, 1000, 12000 };
	public static final double MAX_APP_DURATION = 500;
	public static final double MAX_APP_ENERGY = 1000;
	public static final double REQ_SIZE = 36.25;
	public static final double RESP_SIZE = 15;
	public static final double TSK_SIZE = 13;
	public static final double ANS_SIZE = 12;
	public static final double REC_ASK = 5;
	public static final double REC_ANS = 24;


	
	public static int DELAY_FACTOR = 30; // Multiply the task duration by this
											// factor to calculate the maximum
											// allowed delay

	// Joule/bit when uploading with 10Mb/s : 0.00000006840922832
	// Joule that each node consumes when transmitting one byte
	public static final float JOULES_PER_BYTE_SEND = (float) 0.00000048;
	// 0.00000005060218573
	// jOULES that each node consumes when receiving one bit
	public static final float JOULES_PER_BYTE_RECEIVE = (float) 0.0000004;

	private TaskState state;
	private final double duration; // duration of this task (in s) if run on the
									// node that creates it
	// (each node has different cpu)
	private final double maxAllowedDelay; // if the task is created at time t_0
											// then it should finish
	// before (t_0 + maxAllowedDelay)
	private final long requestSize; // size of the data needed to send for this
									// task in bytes
	private final long resultSize; // size of the data needed to receive for
									// this
									// task in bytes
	private final double energy;

	/**
	 * Used for a deep copy of this task, while keeping the same id.
	 * 
	 * @param taskId
	 * @param node
	 * @param startTime
	 * @param duration
	 * @param requestSize
	 * @param answerSize
	 * @param joulesPerSecond
	 */
//	private Task(int taskId, Node node, double createTime, double duration, long requestSize, long answerSize,
//			double ene, int budget, float prob) {
//		super(node, null, createTime, createTime, EventType.TASK_OFFLOAD);
//		this.taskId = taskId;
//		this.state = TaskState.TO_RUN;
//		this.duration = duration;
//		this.maxAllowedDelay = DELAY_FACTOR * this.duration;
//		this.requestSize = requestSize;
//		this.resultSize = answerSize;
//		this.energy = ene;
//		this.taskUsedBudject = budget;
//		this.unSuccessProbability = prob;
//	}
	
	/**
	 * Used for a deep copy of this task, while keeping the same id.
	 * 
	 * @param taskId
	 * @param node
	 * @param startTime
	 * @param duration
	 * @param requestSize
	 * @param answerSize
	 * @param joulesPerSecond
	 */
	private Task(int taskId, Node node1, Node node2, double createTime, double duration, long requestSize, long answerSize,
			double ene, int budget, float prob, Framework type) {
		super(node1, node2, createTime, createTime, EventType.TASK_OFFLOAD);
		this.taskId = taskId;
		this.state = TaskState.TO_RUN;
		this.duration = duration;
		//Set deadline for the task
		if(type==Framework.TASK_OFF){
		this.maxAllowedDelay = DELAY_FACTOR * this.duration;
		}else if(type==Framework.MES_FORW){
			//Mahdieh: change mdc here
			this.maxAllowedDelay = 24 * 3600;
		}else{
			this.maxAllowedDelay = -1;
		}
		this.requestSize = requestSize;
		this.resultSize = answerSize;
		this.energy = ene;
		this.taskUsedBudject = budget;
		this.unSuccessProbability = prob;
		this.taskType = type;
	}

	/**
	 * Used when a new task is generated, the id is updated to the last one.
	 * 
	 * @param node
	 * @param startTime
	 * @param duration
	 * @param requestSize
	 * @param answerSize
	 */
	public Task(Node node, double startTime, double duration, long requestSize, long answerSize, double energy,
			int budget, float successProb, Framework type) {
		this(-1, node,null, startTime, duration, requestSize, answerSize, energy, budget, successProb, type);
		lastTaskId++;
		this.taskId = lastTaskId;
	}

	/**
	 * This mehtod gives you how many budgets you have sepnt on this task
	 * 
	 * @return
	 */
	public int getTaskRemainingBudget() {
		return this.taskUsedBudject;
	}

	public void setTaskRemainingBudget(int newBudget) {
		this.taskUsedBudject = newBudget;
	}

	/**
	 * @return the state
	 */
	public TaskState getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(TaskState state) {
		this.state = state;
	}

	/**
	 * @return the duration in s
	 */
	public double getDuration() {
		return duration;
	}

	/**
	 * @return the maxAllowedDelay in s
	 */
	public double getMaxAllowedDelay() {
		return maxAllowedDelay;
	}

	/**
	 * @return the requestSize in bytes
	 */
	public long getRequestSize() {
		return requestSize;
	}

	/**
	 * @return the answerSize in bytes
	 */
	public long getAnswerSize() {
		return resultSize;
	}

	/**
	 * 
	 * @return the consumed joules per second of execution of this task
	 */
	public double getEnergy() {
		return this.energy;
	}

	/**
	 * Given the upload rate in Kb/s calculate how much time it takes to send
	 * this task.
	 * 
	 * @param ulRate
	 * @return
	 */
	public double calculateRequestTxTime(double ulRate) {
		// Request size is given in bytes.
		return (requestSize * 8) / ulRate;
	}

	/**
	 * Given the upload rate in Kb/s calculate how much time it takes to send
	 * the result of this ask.
	 * 
	 * @param ulRate
	 * @return
	 */
	public double calculateResultTxTime(double ulRate) {
		// Request size is given in bytes.
		return (resultSize * 8) / ulRate;
	}

	/**
	 * Estimate how much time it takes to execute this task on the given node.
	 * The estimation is calculated as the product between the task duration on
	 * the node that originated it and the fraction between givenNodeCpuPower
	 * and original's node CPU power.
	 * 
	 * @param task
	 * @return
	 */
	public long estimateTaskExecutionTime(Node otherNode) {
		return (long) (duration * ((double) this.getNode1().getCpuPower() / otherNode.getCpuPower()));
	}

	/**
	 * Estimates how much Energy the "OtherNode" will consume while executing
	 * this task
	 * 
	 * @param otherNode
	 * @return
	 */
	public double estimateEnergyConsumption(Node otherNode) {
		// System.out.println((float) estimateTaskExecutionTime(otherNode) + ":"
		// + this.joulesPerSecond);
		return  (energy * ((double) this.getNode1().getCpuPower() / otherNode.getCpuPower()));
	}

	/**
	 * @return the taskId
	 */
	public int getTaskId() {
		return taskId;
	}

	/**
	 * @param taskId
	 *            the taskId to set
	 */
	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public Task copy() {
		return copy(this.getTriggerTime());
	}

	public Task copy(double newTriggerTime) {
		Task tsk = new Task(this.getTaskId(), this.getNode1(), this.getNode2(), this.getCreationTime(), this.getDuration(),
				this.getRequestSize(), this.getAnswerSize(), this.getEnergy(), this.getTaskRemainingBudget(),
				this.getUnsuccessProbability(), this.taskType);
		tsk.setTriggerTime(newTriggerTime);
		return tsk;
	}
	
	public Task copy(double newTriggerTime, Node node2) {
		Task tsk = new Task(this.getTaskId(), this.getNode1(), node2, this.getCreationTime(), this.getDuration(),
				this.getRequestSize(), this.getAnswerSize(), this.getEnergy(), this.getTaskRemainingBudget(),
				this.getUnsuccessProbability(), this.taskType);
		tsk.setTriggerTime(newTriggerTime);
		return tsk;
	}

	/**
	 * Setting the success probability of nodes
	 * 
	 * @return
	 */
	public float getUnsuccessProbability() {
		return this.unSuccessProbability;
	}

	public void setUnsuccessProbability(float prob) {
		this.unSuccessProbability = prob;
	}

	

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;

		final Task other = (Task) o;
		if (this.taskId == other.taskId)
			return true;
		return false;
	}

	public Constants.Framework getApplication(){
		return this.taskType;
	}

	@Override
	public int hashCode() {
		return taskId;
	}

	@Override
	public String toString() {
		return "[id=" + taskId + ", time=" + getTriggerTime() + ", duration=" + getDuration() + ", node=" + getNode1()
				+ ", state=" + getState() + "]";
	}

	public boolean isValid(double timeStamp) {
		return this.getCreationTime()+this.maxAllowedDelay > timeStamp;
	}
}
