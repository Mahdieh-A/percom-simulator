package it.uniroma1.di.kosta;

import java.util.Collections;
import java.util.Iterator;
import java.util.Vector;

public class Statistic {

	private Vector<Float> values;

	Statistic() {
		values = new Vector<Float>();
	}

	public void addReplication(float value) {
		values.addElement(value);
	}

	public float getMean() {
		float sum = 0;
		for (float num : values) {
			sum += num;
		}
		//System.out.println("Size: " + values.size());
		return values.size()==0 ? 0 : sum / values.size();
	}

	public float getError() {
		assert!values.isEmpty();
		System.out.println();
		float mean = this.getMean();
		float temp = 0;
		for (float num : values)
			temp += (mean - num) * (mean - num);
		return (float) Math.sqrt(temp / values.size()) / 2;
	}

	public Vector<Float> getValues() {
		return this.values;
	}

	public void addReplication(Statistic st) {
		for (float value : st.getValues()) {
			values.addElement(value);
		}
	}

	public void calculateOutlier() {
		float Q1 = this.getPercentile(25);
		float Q3 = this.getPercentile(75);
		float diff = Q3 - Q1;
		float start = (float) (Q1 - 1.5 * diff);
		float end = (float) (Q3 + 1.5 * diff);
		Iterator<Float> valIterator = values.iterator();
		while (valIterator.hasNext()) {
			float val = valIterator.next();
			if (val < start || val > end) {
				valIterator.remove();
			}
		}

	}

	public float getPercentile(int percentile) {
		Collections.sort(values);
		if (values.size() == 0) {
			return 0;
		} else if (values.size() == 1) {
			return ((float) percentile * values.get(0)) / 100;
		}
		int index = values.size() * percentile + 50;
		if (index % 100 == 0) {
			// System.out.println("Percentile: " + percentile + "Size:" +
			// values.size() + " Index:" + index / 100);
			return values.get(index / 100 - 1);
		} else {
			int indexA = (index / 100);
			int indexB = (index / 100) + 1;
			float coef = ((float) (index % 100)) / 100;
//			System.out.println("Percentile: " + percentile + "Size:" +
//			values.size() + " Index1:" + indexA + " Index2:"
//			+ indexB + " coef" + coef);
			return (coef * values.get(indexB - 1) + (1 - coef) * values.get(indexA - 1));
		}
	}

	public float getMin() {
		if(values.size()==0){
			return 0;
		}else{
			Collections.sort(values);
			return values.firstElement().floatValue();
		}
	}

	public float getMax() {
		if(values.size()==0){
			return 0;
		}else{
		Collections.sort(values);
		return values.lastElement().floatValue();
		}
	}

	public float sum() {
		float sum = 0;
		for (float num : values) {
			sum += num;
		}
		return sum;
	}

}
