package it.uniroma1.di.kosta;

import java.util.Random;

public class Utils {

	public static double intervalRandomDouble(Random r, double min, double max) {
		return min + (r.nextDouble() * (max - min));
	}

	public static long intervalRandomLong(Random r, long min, long max) {
		return min + (long) (r.nextDouble() * (max - min));
	}

	public static int intervalRandomInt(Random r, int min, int max) {
		return min + r.nextInt(max - min + 1);
	}

	/**
	 * Return a double number from a poisson process with given arrival rate.
	 * 
	 * @param random
	 * @param rate
	 */
	public static double nextPoissonDouble(Random random, double rate) {
		return (-Math.log(1 - random.nextDouble()) / rate);
	}
}
