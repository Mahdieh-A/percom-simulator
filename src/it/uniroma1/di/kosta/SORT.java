package it.uniroma1.di.kosta;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import it.uniroma1.di.kosta.Constants.Interactions;

public class SORT implements Reputation {

	private Map<Constants.Interactions, Integer> interactionPerType;
	private Vector<Pair> SH; // interaction history about service
	private Vector<Pair> RH; // interaction history about recommendation
	private double integrityBelief; // integrity belief about service trust
	private double competenceBelief;// competence belief about service trust
	private double reputation; // reputation of a node
	private int reputationEta; // the number of recommendations in computing one
	// node reputation
	private double recommnedationTrust; // recommendation trust about another
	// node

	public SORT() {

		this.reputation = 0;
		this.reputationEta = 0;
		this.integrityBelief = 0;
		this.competenceBelief = 0;
		this.recommnedationTrust = 0;
		interactionPerType = new HashMap <Constants.Interactions, Integer> ();

		for (Interactions tmp : Interactions.values())
			interactionPerType.put(tmp, 0);

		this.SH = new Vector<Pair>();
		this.RH = new Vector<Pair>();
	}

	@Override
	public float getInteractionNum(Interactions interactionType) {
		return this.interactionPerType.get(interactionType).floatValue();
	}

	@Override
	public float getMetric() {
		double serviceTrust = ((double) SH.size() / Constants.maxSizeForSORT)*
				(this.competenceBelief - this.integrityBelief)+ (1 - ((double) SH.size() / Constants.maxSizeForSORT)) * this.reputation;
		//System.out.println("size:" + SH.size() + " ib:" + this.integrityBelief + " cb:" + this.competenceBelief + " r:" + this.reputation + " Final:" + serviceTrust);
		//assert serviceTrust >= 0;
		return (float) serviceTrust;
	}

	@Override
	public void interaction(Constants.Interactions interactionType, double weight, double msatis) {

		double satisfaction = 0;
		double weightTemp = 0;
		int count =  this.interactionPerType.get(interactionType);
		this.interactionPerType.put(interactionType, count+1);
		System.out.println("Interaction: " + weight + msatis);

		if (interactionType == Interactions.POSITIVE_GET_BACK_RESULT_III) {
			satisfaction = 1;
			weightTemp =  weight>=1 ? 1 : weight;

		} else {
			satisfaction = -1* msatis;
			weightTemp = weight>=1 ? 1 : weight;
		}

		if (SH.size() < Constants.maxSizeForSORT) {
			this.SH.addElement(new Pair(satisfaction, weightTemp /*/ Task.MAX_APP_ENERGY*/));
		} else {
			SH.removeElementAt(0);
			SH.addElement(new Pair( satisfaction, weightTemp /*/ Task.MAX_APP_ENERGY*/));
		}
		updateTrust();
	}

	private void updateTrust() {
		double tempcb = 0;
		double tempib = 0;
		double beta = 0;
		double f = 0;

		for (int i = 0; i < SH.size(); i++) {
			f = ((double) (i + 1) / SH.size());
			tempcb += SH.get(i).satisfaction * SH.get(i).weight * f;
			beta += f * SH.get(i).weight;
			//System.out.println(SH.get(i).satisfaction + ":" + SH.get(i).weight + ":" + f);

		}
		tempcb = (beta == 0) ? 0 : (tempcb / beta);
		for (int i = 0; i < SH.size(); i++) {
			f = ((double) (i + 1) / SH.size());
			tempib += (SH.get(i).weight * f) * (SH.get(i).satisfaction - tempcb) * (SH.get(i).satisfaction - tempcb);
		}

		tempib = (SH.size() == 0) ? 0 : (Math.sqrt(tempib / SH.size()));

		this.competenceBelief = tempcb;
		this.integrityBelief = tempib;
		//assert tempcb >= 0;
		//assert tempib >= 0;

		System.out.println("C:" + SH.size() + ":" + this.competenceBelief + " Integrity:" + this.integrityBelief + " Size:" + SH.size() + ":" + this.reputation);
		//		double temp = ((double) SH.size() / Constants.maxSizeForSORT) * (competenceBelief - this.integrityBelief / 2)
		//				+ (1 - ((double) SH.size() / Constants.maxSizeForSORT)) * this.reputation;
		//		// System.out.println("Recom:" + serviceTrust);
		//assert temp >= 0;
	}

	public void recommInteraction(double recSatisfact, double recWeigh) {
		this.RH.add(new Pair(recSatisfact, recWeigh));
		updateReputationTrust();
	}

	private void updateReputationTrust() {
		double tempcb = 0;
		double tempib = 0;
		double beta = 0;
		double f = 0;

		for (int i = 0; i < RH.size(); i++) {
			f = ((double) (i + 1) / RH.size());
			tempcb += RH.get(i).satisfaction * RH.get(i).weight * f;
			beta += f * RH.get(i).weight;
		}
		tempcb = (beta == 0) ? 0 : (tempcb / beta);

		for (int i = 0; i < RH.size(); i++) {
			f = ((double) (i + 1) / RH.size());
			tempib += ((RH.get(i).satisfaction * RH.get(i).weight * f) - tempcb)
					* ((RH.get(i).satisfaction * RH.get(i).weight * f) - tempcb);
		}

		tempib = (RH.size() == 0) ? 0 : (Math.sqrt(tempib / RH.size()));

		// BUG
		this.recommnedationTrust = 1;
		// this.recommnedationTrust = ((double) RH.size() /
		// Constants.maxSizeForSORT) * (tempcb - tempib / 2)
		// + (1 - ((double) RH.size() / Constants.maxSizeForSORT)) *
		// this.reputation;
	}

	@Override
	public Recommendation recommendation() {
		return new SORTRecom(SH.size(), this.reputationEta, this.competenceBelief, this.integrityBelief,
				this.reputation);
	}

	private class Pair {
		public double satisfaction;
		public double weight;

		public Pair(double s, double w) {
			this.satisfaction = s;
			this.weight = w;
		}
	}

	public double getRecomTrust() {
		if (RH.size() == 0) {
			return this.reputation;
		} else {
			return this.recommnedationTrust;
		}
	}

	public void setReputation(double reputation) {
		this.reputation = reputation;
	}

	public void setReputationEta(int reputationEta) {
		this.reputationEta = reputationEta;
	}

	@Override
	public int getHisotry() {

		return SH.size();
	}

}
