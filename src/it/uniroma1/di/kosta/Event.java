package it.uniroma1.di.kosta;

/**
 * Represents an event which can be a meet or a leave
 * 
 * @author sokol
 */
public class Event implements Comparable<Event> {

	private EventType type;
	private double triggerTime;
	private final double creationTime;
	private Node node1;
	private Node node2;

	public Event(Node node1, Node node2, double time, double creation, EventType type) {
		this.node1 = node1;
		this.node2 = node2;
		this.triggerTime = time;
		this.type = type;
		this.creationTime = creation;
	}


	public double getCreationTime() {
		// System.out.println("Creation Time:" + creationTime);
		return creationTime;
	}

	/**
	 * @return the type of this event
	 */
	public EventType getType() {
		return type;
	}

	/**
	 * @return the time
	 */
	public double getTriggerTime() {
		return this.triggerTime;
	}

	/**
	 * @param time
	 *            the time to set
	 */
	public void setTriggerTime(double time) {
		this.triggerTime = time;
	}

	/**
	 * @param type
	 *            the type to set to this event
	 */
	public void setType(EventType type) {
		this.type = type;
	}

	/**
	 * @return the id1
	 */
	public Node getNode1() {
		return node1;
	}

	/**
	 * @param node1
	 *            the id1 to set
	 */
	public void setNode1(Node node1) {
		this.node1 = node1;
	}

	/**
	 * @return the id2
	 */
	public Node getNode2() {
		return node2;
	}

	/**
	 * @param node2
	 *            the id2 to set
	 */
	public void setNode2(Node node2) {
		this.node2 = node2;
	}

	@Override
	public int compareTo(Event otherEvent) {
		if (triggerTime < otherEvent.triggerTime) {
			return -1;
		} else if (triggerTime > otherEvent.triggerTime) {
			return 1;
		} else {
			return type.compareTo(otherEvent.type);
			// return 0;
		}
	}

	@Override
	public String toString() {
		return "[startTime=" + triggerTime + ", type=" + type + ", " + node1 + " -> " + node2 + "]";
	}
}
