package it.uniroma1.di.kosta;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;
import it.uniroma1.di.kosta.Constants;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.uniroma1.di.kosta.Constants.Dataset;
import it.uniroma1.di.kosta.Constants.Framework;
import it.uniroma1.di.kosta.Constants.KEY;
/**
 * Will take care of the simulation. First read the trace and save it.
 * 
 * @author sokol
 *
 */
public class Simulation {

	private Trace trace;

	private File traceFile;
	private Constants.Dataset type;
	private int energyTime;
	private Map<KEY, Number> constants;
	private Vector<Float> levels;
	//private Map<Integer, Statistic> flopCoinsDist;
	private Map<Integer, Statistic> trustDist;
	private Map<Integer, Float> offloadDist;
	private Map<Integer, Float> batteryDist;
	private Map<Integer, Float> usefulBatteryDist;


	private Map<Constants.Interactions, Map<Integer, Statistic>> interactionDist;
	private Map<Task, Set<Integer>> messageDist;

	public static float neighborNum;
	public static int neighborNumTimes;
	public static final Logger log = null;
	public static Map<Integer, Vector<Float>> trafficPlot = new HashMap<Integer, Vector<Float>>();
	public static Map<Integer, Vector<Float>> energyPlot = new HashMap<Integer, Vector<Float>>();

	private static Map<Integer, Vector<Statistic>> info05BatteryPlot = new HashMap<Integer, Vector<Statistic>>();
	private static Map<Integer, Vector<Statistic>> info06BatteryPlot = new HashMap<Integer, Vector<Statistic>>();
	private static Map<Integer, Vector<Statistic>> humanetBatteryPlot = new HashMap<Integer, Vector<Statistic>>();
	private static Map<Dataset, Map<Integer,Vector<Statistic>>> reputationPlot = new HashMap<Dataset, Map<Integer,Vector<Statistic>>>();
	private static Map<Dataset, Map<Integer,Vector<Statistic>>> offloadPlot = new HashMap<Dataset, Map<Integer,Vector<Statistic>>>();

	/**
	 * The first argument is the trace type while the second one is the path to
	 * the trace.
	 * 
	 * @param argv
	 */
	public Simulation(String[] argv, Vector<Float> levels) {
		try {
			// Logger.getRootLogger().setLevel(Level.OFF);
			type = Dataset.values()[Integer.parseInt(argv[0])];
			traceFile = new File(argv[1]);
			this.levels =levels;
			energyTime = 0;
			constants = Constants.CONSTS.get(type);
			if (argv.length > 2)
				constants.put(Constants.KEY.SIMULATION_TYPE, Integer.parseInt(argv[2]));
			assert constants != null;

			if(constants.get(KEY.TRACE_TYPE).intValue()==Constants.READ_AT_ONCE){
				trace = new Trace(type, constants, traceFile);
			}else if(constants.get(KEY.TRACE_TYPE).intValue()==Constants.READ_NOT_AT_ONCE){
				trace = new MDCTrace(type, constants, traceFile);
			}
			System.out.println("Number of nodes: " + trace.getNodes().size());

			//flopCoinsDist = new HashMap<Integer, Statistic>();
			trustDist = new HashMap<Integer, Statistic>();
			batteryDist = new HashMap<Integer, Float>();
			usefulBatteryDist = new HashMap<Integer, Float>();
			interactionDist = new HashMap<Constants.Interactions, Map<Integer, Statistic>> ();
			offloadDist = new HashMap<Integer, Float>();
			if(Constants.APPLICATION == Constants.Framework.MES_FORW || Constants.APPLICATION == Constants.Framework.MIX){
				messageDist = new HashMap <Task, Set<Integer>> ();
			}
			generateTaskOffloadEvents();
			this.generateReportEvents();
			//setBatteryLevelForNodesRandomly();
			setPercentBatteryLevel(Integer.parseInt(argv[3]));
			startSimulation();
			printStatistics();
		} catch (FileNotFoundException e) {
			//log.error("Could not find the file: " + e);
		}
	}

	private void setPercentBatteryLevel(int percent) {
		double percentValue = ((double) percent) / 100;
		assert trace.getNodes().size() >= levels.size();
		if (levels.size() == 1) {
			// System.out.println("We have 1 Level of Battery: " + levels.get(0));
			for (Node node : trace.getNodes().values()) {
				node.setMinBatteryLevel(levels.get(0));
			}
		} else {
			System.out.println("We have more than one Level of Battery: ");
			Random battery = new Random();
			for (Node node : trace.getNodes().values()) {
				double rand = battery.nextDouble();
				node.setMinBatteryLevel(rand < percentValue ? levels.get(1) : levels.get(0));
			}

			for (float level : levels) {
				//offloadDist.put((int) level, new Statistic());
				trustDist.put((int) level, new Statistic());
				for (Constants.Interactions key :Constants.Interactions.values() ){
					if(this.interactionDist.get(key)==null){
						this.interactionDist.put(key, new HashMap<Integer, Statistic>());
					}
					this.interactionDist.get(key).put((int) level, new Statistic());
				}
			}
		}

	}

	private void setBatteryLevelForNodes(Vector<Float> levels) {
		assert trace.getNodes().size() >= levels.size();
		if (levels.size() == 1) {
			// System.out.println("We have 1 Level of Battery: " +
			// levels.get(0));
			for (Node node : trace.getNodes().values()) {
				node.setMinBatteryLevel(levels.get(0));
			}
		} else {
			int boundWidth = 0;
			int nodeNum = trace.getNodes().size();
			if (nodeNum % levels.size() == 0) {
				boundWidth = nodeNum / levels.size();
			} else {
				boundWidth = (int) Math.round((float) nodeNum / levels.size());
			}
			System.out.println("We have more than one Level of Battery: " +
					boundWidth);
			for (float level : levels) {
				//offloadDist.put((int) level, new Statistic());
				trustDist.put((int) level, new Statistic());
				for (Constants.Interactions key :Constants.Interactions.values() ){
					if(this.interactionDist.get(key)==null){
						this.interactionDist.put(key, new HashMap<Integer, Statistic>());
					}
					this.interactionDist.get(key).put((int) level, new Statistic());
				}

			}

			for (int id=1; id<trace.getNodes().size()+1; id++) {
				int index = (id / boundWidth) >= levels.size() ? levels.size() - 1 : id / boundWidth;
				// System.out.println("Node ID: " + id + "Assigned: " + index);
				trace.getNodes().get(trace.getNodes().keySet().toArray()[id-1]).setMinBatteryLevel(levels.get(index));
			}
		}
	}


	private void setBatteryLevelForNodesRandomly() {
		assert trace.getNodes().size() >= levels.size();
		if (levels.size() == 1) {
			// System.out.println("We have 1 Level of Battery: " + levels.get(0));
			for (Node node : trace.getNodes().values()) {
				node.setMinBatteryLevel(levels.get(0));
			}
		} else {
			System.out.println("We have more than one Level of Battery: ");
			Random battery = new Random();
			for (Node node : trace.getNodes().values()) {
				int rand = Utils.intervalRandomInt(battery, 0, levels.size()-1);
				//System.out.println("Rand" + node + ":" + rand);
				node.setMinBatteryLevel(levels.get(rand));
			}

			for (float level : levels) {
				//offloadDist.put((int) level, new Statistic());
				trustDist.put((int) level, new Statistic());
				for (Constants.Interactions key :Constants.Interactions.values() ){
					if(this.interactionDist.get(key)==null){
						this.interactionDist.put(key, new HashMap<Integer, Statistic>());
					}
					this.interactionDist.get(key).put((int) level, new Statistic());
				}
			}
		}
	}



	/**
	 * Start the simulation. Get the next event from the eventQueue and do
	 * something depending on the event type.
	 */
	private void startSimulation() {
		while (trace.hasMoreEvents()) {

			Event event = trace.pollNextEvent();
			double time = event.getTriggerTime();

			// If this event is not in the considered interval then discard it
			if (time > constants.get(KEY.TRACE_END_CONSIDERING).longValue()) {
				continue;
			}
			// System.out.println("Event:" + event.getType());
			Node node1 = event.getNode1();
			Node node2 = event.getNode2();

			// log.debug(event);

			switch (event.getType()) {
			case REPORT:
				System.out.println("REPORT:" + time);
				handleReportEvent(time);
				break;

			case MEET:
				//log.debug(event);
				//meet physically, because we needed to first see each other
				node1.meet(time, node2);
				node2.meet(time, node1);
				//meet operations
				node1.meetOperations(time, node2, trace);
				node2.meetOperations(time, node1, trace);
				break;

			case LEAVE:
				//log.debug(event);
				node1.leave(time, node2);
				node2.leave(time, node1);
				break;

			case TASK_OFFLOAD:
				// log.debug("Task: " + event);
				node1.newTask(trace, (Task) event);
				break;

			default:
				//log.error("Event type not recognized");
				break;
			}
		}

		//		log.info("traceStartTime: " + Trace.startTime);
		//		log.info("traceEndTime: " + Trace.endTime);
	}

	private void handleReportEvent(double currentTime) {

		/*float meanBattery = trace.getAveragePowerLevel();
		if (type == Constants.INFOCOM_05) {
			if (info05BatteryPlot.get(constants.get(KEY.SIMULATION_TYPE).intValue()).size() <= Constants.TIMESTAMP_NUM)
				info05BatteryPlot.get(constants.get(KEY.SIMULATION_TYPE).intValue()).add(this.energyTime,
						new Statistic());
			info05BatteryPlot.get(constants.get(KEY.SIMULATION_TYPE).intValue()).get(this.energyTime)
			.addReplication(meanBattery);
		} else if (type == Constants.INFOCOM_06) {
			if (info06BatteryPlot.get(constants.get(KEY.SIMULATION_TYPE).intValue()).size() <= Constants.TIMESTAMP_NUM)
				info06BatteryPlot.get(constants.get(KEY.SIMULATION_TYPE).intValue()).add(this.energyTime,
						new Statistic());
			info06BatteryPlot.get(constants.get(KEY.SIMULATION_TYPE).intValue()).get(this.energyTime)
			.addReplication(meanBattery);

		} else if (type == Constants.HUMANET) {
			if (humanetBatteryPlot.get(constants.get(KEY.SIMULATION_TYPE).intValue()).size() <= Constants.TIMESTAMP_NUM)
				humanetBatteryPlot.get(constants.get(KEY.SIMULATION_TYPE).intValue()).add(this.energyTime,
						new Statistic());
			humanetBatteryPlot.get(constants.get(KEY.SIMULATION_TYPE).intValue()).get(this.energyTime)
			.addReplication(meanBattery);

		}*/
		for(Node node : this.trace.nodes.values()){
			if(type==Dataset.MDC && node.getBatteryModel()==Constants.numberBattery && node.getBatteryLevel(currentTime) < 50){
				node.charge();
			}
		}
		if(Constants.REPORT) {
			if(reputationPlot.get(type) == null){
				reputationPlot.put(type, new HashMap<Integer, Vector<Statistic>>());
			}
			if (reputationPlot.get(type).size() <= Constants.TIMESTAMP_NUM) {
				reputationPlot.get(type).put(this.energyTime, new Vector<Statistic>());
			}

			for(int index=0; index<levels.size(); index++){
				if (reputationPlot.get(type).get(energyTime).size() <= index){
					reputationPlot.get(type).get(energyTime).add(new Statistic());
				}
				setTrustDist();
				reputationPlot.get(type).get(this.energyTime).get(index).addReplication(getTrustDist(levels.get(index).intValue()));
			}

			if(offloadPlot.get(type) == null){
				offloadPlot.put(type, new HashMap<Integer, Vector<Statistic>>());
			}
			if (offloadPlot.get(type).size() <= Constants.TIMESTAMP_NUM) {
				offloadPlot.get(type).put(this.energyTime, new Vector<Statistic>());
			}

			for(int index=0; index<levels.size(); index++){
				if (offloadPlot.get(type).get(energyTime).size() <= index){
					offloadPlot.get(type).get(energyTime).add(new Statistic());
				}
				setOffloadDist();
				offloadPlot.get(type).get(this.energyTime).get(index).addReplication(getoffloadDist(levels.get(index).intValue()));
			}}

		energyTime++;
	}

	private static void printReportResults(Vector<Float> levels, String filePath, Dataset traceType, int percent) {

		StringBuilder reputation = new StringBuilder();
		StringBuilder throughput = new StringBuilder();

		FileWriter repFile, offFile;
		try {
			repFile = new FileWriter(filePath + traceType +percent+ "Rep.dat");
			offFile = new FileWriter(filePath + traceType +percent+ "Off.dat");
			reputation.append("Levels\t");
			throughput.append("Levels\t");
			for (Float level : levels) {
				reputation.append(level.intValue() + "\t");
				throughput.append(level.intValue() + "\t");

			}
			reputation.append("\n");
			throughput.append("\n");


			for (int time = 0; time < Constants.TIMESTAMP_NUM+1; time++) {
				reputation.append(time+"\t");
				throughput.append(time+"\t");
				for (int i = 0; i<levels.size(); i++) {
					reputation.append(reputationPlot.get(traceType).get(time).get(i).getMean() + "\t");
					throughput.append(offloadPlot.get(traceType).get(time).get(i).getMean() + "\t");
				}
				reputation.append("\n");
				throughput.append("\n");
			}
			repFile.write(reputation.toString());
			repFile.flush();
			offFile.write(throughput.toString());
			offFile.flush();

		} catch (IOException e){
			System.out.println("Print report...");
			e.printStackTrace();
		}

	}

	private void generateReportEvents() {
		int num = Constants.TIMESTAMP_NUM;
		long duration = (constants.get(KEY.TASKS_END_GENERATING).intValue()
				- constants.get(KEY.TASKS_START_GENERATING).intValue());
		double step = (double) duration / num;
		for (int i = 0; i < num; i++) {
			double time = i * step + constants.get(KEY.TASKS_START_GENERATING).intValue();
			trace.addEvent(new Event(null, null, time, time,EventType.REPORT));
		}

		trace.addEvent(new Event(null, null, constants.get(KEY.TASKS_END_GENERATING).intValue(), constants.get(KEY.TASKS_END_GENERATING).intValue(), EventType.REPORT));
	}



	/**
	 * Generate some tasks (TASK_OFFLOAD events) and add them to the event
	 * queue. Only node1 will be specified for this event at generation time.
	 * Node1 will later choose to whom delegate the task depending on the
	 * algorithm.
	 */
	private void generateTaskOffloadEvents() {
		HashMap<Integer, Node> nodes = trace.getNodes();

		Random randomTask = new Random();
		Random randomCucncurrentTask = new Random();
		Random randomTime = new Random();
		Random taskRand = new Random();


		// Assign some task to each node.
		for (Node node : nodes.values()) {
			// double taskStartTime = Trace.startTime;
			double taskStartTime = constants.get(KEY.TASKS_START_GENERATING).doubleValue();

			for (int i = 0; i < constants.get(KEY.NR_TASKS_PER_NODE).intValue(); i++) {

				// Choose a waiting time for this event following poisson
				// distribution on the interval
				double waitTime = Utils.nextPoissonDouble(randomTime,
						constants.get(KEY.TASKS_ARRIVAL_RATE).doubleValue());
				taskStartTime += waitTime;
				if (taskStartTime > constants.get(KEY.TASKS_END_GENERATING).longValue()) {
					continue;
				}

				int cuncurrentTasks = 0;
				int taskType = 0;
				long duration = 0;
				long energy = 0;
				long reqSize = 0;
				long ansSize = 0;
				Framework taskApp = Constants.Framework.TASK_OFF;
				if(Constants.APPLICATION == Constants.Framework.TASK_OFF){
					// Calculate a duration time for this task following some distribution.
					cuncurrentTasks = Utils.intervalRandomInt(randomCucncurrentTask, 1, 1);
					taskType = Utils.intervalRandomInt(randomTask, 0, 1);
					duration = Task.appDurations[taskType];
					energy = Task.appEnergy[taskType];
					reqSize = Task.appSendDataSize[taskType];
					ansSize = Task.appReceiveDataSize[taskType];
					taskApp = Constants.Framework.TASK_OFF;
				}else if(Constants.APPLICATION == Constants.Framework.MES_FORW){
					cuncurrentTasks = Utils.intervalRandomInt(randomCucncurrentTask, 1, 1);
					duration = 5; //time to generate packet for different nodes, Mahdieh
					energy = 5;
					reqSize = 100;
					ansSize = 1;
					taskApp = Constants.Framework.MES_FORW;
				}else if(Constants.APPLICATION == Constants.Framework.MIX){
					cuncurrentTasks = Utils.intervalRandomInt(randomCucncurrentTask, 1, 1);
					double rand = taskRand.nextDouble();
					taskApp = (rand < 0.25 ? Constants.Framework.TASK_OFF :Constants.Framework.MES_FORW);
					if(taskApp == Constants.Framework.MES_FORW){
						duration = 5; //time to generate packet for different nodes, Mahdieh
						energy = 5;
						reqSize = 100;
						ansSize = 1;
					}else if(taskApp == Constants.Framework.TASK_OFF){
						taskType = Utils.intervalRandomInt(randomTask, 0, 1);
						duration = Task.appDurations[taskType];
						energy = Task.appEnergy[taskType];
						reqSize = Task.appSendDataSize[taskType];
						ansSize = Task.appReceiveDataSize[taskType];
					}

				}

				i += cuncurrentTasks - 1;
				for (int taskNum = 0; taskNum < cuncurrentTasks; taskNum++) {

					Task task = new Task(node, taskStartTime, duration, reqSize, ansSize, energy,
							10000 /*It can be removed*/, 1, taskApp);
					if(taskApp == Constants.Framework.MES_FORW){
						this.messageDist.put(task, new HashSet<Integer>());
					}

					trace.addEvent(task);
				}
			}
		}
	}



	/**
	 * Print statistics regarding: 1. Distribution of number of contacts among
	 * all node peers 2. Distribution of contact duration among all node peers
	 * 3. -//- of intercontacts among all node peers 4. Statistics about tasks:
	 * TODO
	 */
	private void printStatistics() {
		System.out.println("Start Statistics ...");
		File statsDir = new File(traceFile.getParent() + File.separator + "stats");
		statsDir.mkdir();
		String basePath = statsDir.getAbsolutePath() + File.separator;

		// Number of contacts
		String nrContactsFilePath = basePath + "nrContacts.dat";
		String nrContactsCdfFilePath = basePath + "nrContactsCdf.dat";
		printStatistics(trace.getContactNumberDistr(), nrContactsFilePath, nrContactsCdfFilePath);

		// Contact duration
		String contactDurFilePath = basePath + "contactDur.dat";
		String contactDurCdfFilePath = basePath + "contactDurCdf.dat";
		printStatistics(trace.getContactDurationDistr(), contactDurFilePath, contactDurCdfFilePath);

		// InterContacts
		String intercontactsFilePath = basePath + "intercontacts.dat";
		String intercontactsCdfFilePath = basePath + "intercontactsCdf.dat";
		printStatistics(trace.getIntercontactDistr(), intercontactsFilePath, intercontactsCdfFilePath);

		// Number of nodes in contact when there is a task for execution
		String nrNodesInContactOnTaskFilePath = basePath + "nrNodesInContactOnTask.dat";
		String nrNodesInContactOnTaskFilePathCount = basePath + "nrNodesInContactOnTaskCount.dat";
		printNrNodesInContactOnTask(nrNodesInContactOnTaskFilePath, nrNodesInContactOnTaskFilePathCount);

		// Given a time-interval, print the number of contacts with
		// time-interval granularity nodes had
		// during the simulation.
		String nrContactsIntervalFilePath = basePath + "nrContactsInterval.dat";
		printNrContactsInterval(trace.getMeetTimestamps(), Constants.INTERVAL_GRANULARITY, nrContactsIntervalFilePath);

		// Print some statistics about tasks
		//		log.info("Locally executed tasks: " + trace.getNrExecutedLocalTasks());
		//		log.info("Locally interrupted tasks: " + trace.getNrInterruptedLocalTasks());
		//		log.info("Locally interrupted tasks before local: " + Node.countNotStartedLocal);
		//		log.info("Offload results received: " + trace.getNrOffloadResultsReceived());
		//		log.info("Unique offloaded and finished: " + trace.getNrUniqueFinishedOffloadedTasks());
		//		log.info("Offload requests not finished yet: " + trace.getNrOffloadRequests());

		System.out.println("End Statistics ...");
	}

	/**
	 * Given a time-interval, print the number of contacts with that
	 * time-interval granularity.
	 * 
	 * @param interval
	 *            Interval for granularity (in seconds).
	 * @param meetTimestamps
	 *            Sorted list containing all meet timestamps of all nodes.
	 */
	private void printNrContactsInterval(List<Double> meetTimestamps, int interval, String nrContactsIntervalFilePath) {

		try (FileWriter fw = new FileWriter(nrContactsIntervalFilePath)) {
			StringBuilder out = new StringBuilder();
			double currTime = meetTimestamps.get(0);
			int count = 0;

			Iterator<Double> iterator = meetTimestamps.iterator();
			while (iterator.hasNext()) {
				double timestamp = iterator.next();
				if (timestamp < currTime + interval) {
					count++;
				} else {
					out.append(currTime + "\t" + count + "\n");
					currTime += interval;
					count = 1;
				}
			}

			fw.write(out.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Sets the flopCoin distribution of the nodes
	 */
	/*
	public void setFlopCoinDist() {

		for (Node n : trace.getNodes().values()) {
			int nodeLevel = (int) n.getMinBatteryLevel();
			flopCoinsDist.get(nodeLevel).addReplication(n.getFlopCoins());
		}

	}
	 */


	private void setBatteryDist(){
		for(Float  l: levels){
			int nodeLevel = (int) l.intValue();
			float sumTotal = 0;
			float sumUseful = 0;
			int count = 0;
			for (Node other : trace.getNodes().values()) {
				int otherNodeLevel = (int)other.getMinBatteryLevel();
				if (otherNodeLevel == nodeLevel) {			
					//	System.out.println(nodeLevel + other.toString() + local.getTaskId() +":"+ this.messageDist.get(local).size());
					sumTotal += other.getUsedBattery();
					sumUseful += other.getUsefulBattery();
					count ++;
				}}
			System.out.println("Here" + sumTotal + ":" + count);
			batteryDist.put(nodeLevel, count==0 ? 0 :sumTotal / count);
			usefulBatteryDist.put(nodeLevel, count==0 ? 0 :sumUseful / count);
		}
	}

	private void setOffloadDist() {
		offloadDist = new HashMap<Integer, Float>();
		if(Constants.APPLICATION == Constants.Framework.MES_FORW){
			for(Task tsk : this.messageDist.keySet()){
				for(Node n : trace.getNodes().values() ){
					if(n.receivedTask(tsk)){
						this.messageDist.get(tsk).add(n.getId());
					}
				}}

			for(Float  l: levels){
				int nodeLevel = (int) l.intValue();
				if (!offloadDist.containsKey(nodeLevel)) {
					float sum = 0;
					int count = 0;
					for (Node other : trace.getNodes().values()) {
						int otherNodeLevel = (int)other.getMinBatteryLevel();
						if (otherNodeLevel == nodeLevel) {
							for(Task local : other.getLocalTasks()){
								//System.out.println(nodeLevel + other.toString() + local.getTaskId() +":"+ this.messageDist.get(local).size());
								sum += ((float) this.messageDist.get(local).size())-1;
								//sum += (float) other.nrInterruptedLocalTasks();

							}
							count += other.requestsNum;
						}}
					System.out.println("Here" + sum + ":" + count);
					offloadDist.put(nodeLevel, count==0? 0 :sum / count);
				}
			}
		} else if(Constants.APPLICATION == Constants.Framework.TASK_OFF){		
			for(Float  l: levels){
				int nodeLevel = (int) l.intValue();
				//			System.out.println(n +":" + nodeLevel + "Offloading results received:" + n.nrOffloadRresultsReceived()
				//			+ " Interrupted:" + n.nrInterruptedLocalTasks()
				//			+ " Uniqueu:" + n.getSetUniqueFinishedOffloadedTasks().size()
				//			+ " Offloading requests Remained:" + n.nrOffloadRequests()
				//			+ " Total requests:" + n.requestsNum
				//			+ " Sending offloaded Tasks" + n.nrSendingOffloadTasks());
				//		
				if (!offloadDist.containsKey(nodeLevel)) {
					float sum = 0;
					int count = 0;
					for (Node other : trace.getNodes().values()) {
						int otherNodeLevel = (int)other.getMinBatteryLevel();
						if (otherNodeLevel == nodeLevel) {
							//sum += (other.requestsNum==0)? 0:((float) other.nrInterruptedLocalTasks()) / other.requestsNum;
							sum += (float) other.nrInterruptedLocalTasks();
							count+=  other.requestsNum;
						}
					}
					System.out.println("Here" + sum + count);
					offloadDist.put(nodeLevel, count==0? 0 :sum / count);
				}
			}}else if(Constants.APPLICATION == Constants.Framework.MIX){
				for(Task tsk : this.messageDist.keySet()){
					for(Node n : trace.getNodes().values() ){
						if(n.receivedTask(tsk)){
							this.messageDist.get(tsk).add(n.getId());
						}
					}}

				for(Float  l: levels){
					int nodeLevel = (int) l.intValue();
					if (!offloadDist.containsKey(nodeLevel)) {
						float sum = 0;
						int count = 0;
						for (Node other : trace.getNodes().values()) {
							int otherNodeLevel = (int)other.getMinBatteryLevel();
							if (otherNodeLevel == nodeLevel) {
								for(Task local : other.getLocalTasks()){
									if(local.getApplication() == Constants.Framework.MES_FORW){
										//System.out.println(nodeLevel + other.toString() + local.getTaskId() +":"+ this.messageDist.get(local).size());
										sum += ((float) this.messageDist.get(local).size())-1;
									}
									//sum += (float) other.nrInterruptedLocalTasks();
									}
								sum += ((float) other.nrInterruptedLocalTasks());
								count += other.requestsNum;
							}}
						System.out.println("Here" + sum + ":" + count);
						offloadDist.put(nodeLevel, count==0? 0 :sum / count);
					}
				}
			}
	}

	private Statistic getInteractionDist(Constants.Interactions interactionType, int level) {
		return this.interactionDist.get(interactionType).get(level);
	}

	private void setInteractionDist() {
		for (Node aim : trace.getNodes().values()) {
			Map<Constants.Interactions,Statistic> counts = new HashMap<Constants.Interactions, Statistic>();

			for(Constants.Interactions key: Constants.Interactions.values()){
				counts.put(key, new Statistic());
			}
			for (Node thirdParty : trace.getNodes().values()) {

				if (!aim.equals(thirdParty) && thirdParty.doYouKnow(aim)) {
					for(Constants.Interactions key: Constants.Interactions.values()){

						counts.get(key).addReplication(thirdParty.getInteractionHistory(key, aim));

					}
				}
			}
			for (Constants.Interactions key : Constants.Interactions.values()){
				int level = (int)aim.getMinBatteryLevel();
				this.interactionDist.get(key).get(level).addReplication(counts.get(key).sum());
			}

		}

	}

	public void setTrustDist() {
		for(Float level : levels){
			trustDist.put(level.intValue(), new Statistic());
		}
		for (Node aim : trace.getNodes().values()) {
			Statistic st = new Statistic();
			for (Node thirdParty : trace.getNodes().values()) {
				if (!aim.equals(thirdParty) && thirdParty.doYouKnow(aim)) {
					st.addReplication(thirdParty.getOpinionAbout(aim).getMetric());
					//System.out.println("Dist:" + thirdParty + aim +
					//	thirdParty.getOpinionAbout(aim).getMetric());
				}
			}

			trustDist.get((int) aim.getMinBatteryLevel()).addReplication(st.getMean());
			// trustDist.get((int) aim.getMinBatteryLevel()).add(st.getMin());
			// trustDist.get((int)
			// aim.getMinBatteryLevel()).add(st.getPercentile(25));
			// trustDist.get((int)
			// trustDist.get((int)
			// aim.getMinBatteryLevel()).add(st.getPercentile(75));
			// trustDist.get((int) aim.getMinBatteryLevel()).add(st.getMax());
		}
	}

	/**
	 * Print the number of nodes a node is contact with when the node has to
	 * execute a task.
	 * 
	 * @param nrNodesInContactOnTaskFilePath
	 * @param nrNodesInContactOnTaskFilePathCount
	 */
	private void printNrNodesInContactOnTask(String nrNodesInContactOnTaskFilePath,
			String nrNodesInContactOnTaskFilePathCount) {
		// Number of nodes in contact when there is a task for execution
		StringBuilder out = new StringBuilder();
		Map<Double, Integer> nrNodesContactOnTask = trace.getNrNodesInContactOnTask();
		int[] count = new int[trace.getNodes().size()];
		int total = 0;
		for (Double time : nrNodesContactOnTask.keySet()) {
			int value = nrNodesContactOnTask.get(time);
			out.append(time + "\t" + value + "\n");
			count[value]++;
			total++;
		}

		// Calculate and print the frequency for each count
		StringBuilder outCount = new StringBuilder();
		for (int i = 0; i < count.length; i++) {
			double f = count[i] / (double) total;
			outCount.append(i + "\t" + f + "\n");
		}

		try (FileWriter fw = new FileWriter(nrNodesInContactOnTaskFilePath);
				FileWriter fwCount = new FileWriter(nrNodesInContactOnTaskFilePathCount)) {

			fw.write(out.toString());
			fwCount.write(outCount.toString());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Used to print statistics about number of contacts, contact duration, and
	 * intercontacts.
	 * 
	 * @param sortedList
	 * @param statFilePath
	 * @param statCdfFilePath
	 */
	private <T extends Comparable<T>> void printStatistics(List<T> sortedList, String statFilePath,
			String statCdfFilePath) {

		StringBuilder out = new StringBuilder();

		try (FileWriter fw = new FileWriter(statFilePath); FileWriter fwCdf = new FileWriter(statCdfFilePath)) {
			for (T i : sortedList) {
				out.append(i + "\n");
			}
			fw.write(out.toString());
			fw.flush();

			// Print the cumulative distribution of the considered statistics
			out.setLength(0);
			for (T i : sortedList) {
				double prob = nrElementsHigherEqual(sortedList, i) / (double) sortedList.size();
				out.append(i + "\t" + prob + "\n");
			}
			fwCdf.write(out.toString());
			fwCdf.flush();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Count the number of elements higher or equal than the given value. The
	 * list is sorted in ascending order.
	 * 
	 * @param sortedList
	 * @param value
	 * @return
	 */
	private <T extends Comparable<T>> int nrElementsHigherEqual(List<T> sortedList, T value) {
		int count = 0;
		for (T elem : sortedList) {
			if (elem.compareTo(value) == -1)
				count++;
			else
				break;
		}

		return sortedList.size() - count;
	}

	private void usage() {
		log.info("Usage: java -jar Simulation.jar <TRACE_TYPE> <TRACE_PATH>");
		log.info("where: TRACE_TYPE is the type of the trace being processed");
		log.info("\t 0 for INFOCOM");
		log.info("\t 1 for SIGCOMM");
		log.info("where: TRACE_PATH is the path to the trace file.");
	}

	public static void printTaskStatistics(String[] columns, Map<Integer, Vector<Float>> stats, String statFilePath) {
		StringBuilder out = new StringBuilder();

		FileWriter fw;
		try {

			fw = new FileWriter(statFilePath);

			SortedSet<Integer> keys = new TreeSet<Integer>(stats.keySet());
			out.append("# ");
			for (String st : columns) {
				out.append(st + "\t");
			}
			out.append("\n");
			for (Integer i : keys) {
				out.append(i);
				for (Float t : stats.get(i))
					out.append("\t" + String.format("%f", t));
				out.append("\n");
			}

			fw.write(out.toString());
			fw.flush();
		} catch (IOException e) {

			System.out.println("Print Statistics...");
			e.printStackTrace();
		}
	}

	private static void printAllResults() {

		String[] columns = { "Algorithm", "Lazy", "Ours", "Greedy" };
		String[] rows = { "Info05", "Info06", "Humanet" };
		StringBuilder traf = new StringBuilder();
		StringBuilder ene = new StringBuilder();
		StringBuilder off = new StringBuilder();
		FileWriter traffic, energy, offload;
		try {

			traffic = new FileWriter("Traffic.dat");
			traf.append("# ");
			for (String st : columns) {
				traf.append(st + "\t");
			}
			traf.append("\n");

			energy = new FileWriter("Energy.dat");
			ene.append("# ");
			for (String st : columns) {
				ene.append(st + "\t");
			}
			ene.append("\n");

			offload = new FileWriter("Offload.dat");
			off.append("# ");
			for (String st : columns) {
				off.append(st + "\t");
			}
			off.append("\n");

			for (int type = 0; type <= 2; type++) {
				traf.append(rows[type] + "\t" + Simulation.trafficPlot.get(type).get(0) + "\t"
						+ Simulation.trafficPlot.get(type).get(1) + "\t" + Simulation.trafficPlot.get(type).get(2)
						+ "\t" + Simulation.trafficPlot.get(type).get(3) + "\t"
						+ Simulation.trafficPlot.get(type).get(4) + "\t" + Simulation.trafficPlot.get(type).get(5));

				traf.append("\n");
			}

			for (int type = 0; type <= 2; type++) {
				off.append(rows[type] + "\t" + Simulation.offloadPlot.get(type).get(0) + "\t"
						+ Simulation.offloadPlot.get(type).get(1) + "\t" + Simulation.offloadPlot.get(type).get(2)
						+ "\t" + Simulation.offloadPlot.get(type).get(3) + "\t"
						+ Simulation.offloadPlot.get(type).get(4) + "\t" + Simulation.offloadPlot.get(type).get(5));

				off.append("\n");
			}

			for (int type = 0; type <= 2; type++) {
				ene.append(rows[type] + "\t" + Simulation.energyPlot.get(type).get(0) + "\t"
						+ Simulation.energyPlot.get(type).get(1) + "\t" + Simulation.energyPlot.get(type).get(2) + "\t"
						+ Simulation.energyPlot.get(type).get(3) + "\t" + Simulation.energyPlot.get(type).get(4) + "\t"
						+ Simulation.energyPlot.get(type).get(5));

				ene.append("\n");
			}

			traffic.write(traf.toString());
			traffic.flush();
			energy.write(ene.toString());
			energy.flush();
			offload.write(off.toString());
			offload.flush();
		} catch (IOException e) {

			System.out.println("Print Statistics...");
			e.printStackTrace();
		}

	}

	private static void printBatteryResults() {

		String[] columns = { "Algorithm", "Lazy", "Ours", "Greedy" };
		StringBuilder info05 = new StringBuilder();
		StringBuilder info06 = new StringBuilder();
		StringBuilder hamnet = new StringBuilder();
		FileWriter info05File, info06File, humanetFile;
		try {

			info05File = new FileWriter("Info05Energy.dat");
			for (String st : columns) {
				info05.append(st + "\t");
			}
			info05.append("\n");

			info06File = new FileWriter("Info06Energy.dat");
			for (String st : columns) {
				info06.append(st + "\t");
			}
			info06.append("\n");

			humanetFile = new FileWriter("humanetEnergy.dat");
			for (String st : columns) {
				hamnet.append(st + "\t");
			}
			hamnet.append("\n");

			for (int time = 0; time < Constants.TIMESTAMP_NUM; time++) {
				for (int type : Simulation.info05BatteryPlot.keySet()) {
					info05.append(Simulation.info05BatteryPlot.get(type).get(time).getMean() + "\t");
				}

				info05.append("\n");
			}

			for (int time = 0; time < Constants.TIMESTAMP_NUM; time++) {
				for (int type : Simulation.info06BatteryPlot.keySet()) {
					info06.append(Simulation.info06BatteryPlot.get(type).get(time).getMean() + "\t");
				}

				info06.append("\n");
			}

			for (int time = 0; time < Constants.TIMESTAMP_NUM; time++) {
				for (int type : Simulation.humanetBatteryPlot.keySet()) {
					hamnet.append(Simulation.humanetBatteryPlot.get(type).get(time).getMean() + "\t");
				}

				hamnet.append("\n");
			}

			info05File.write(info05.toString());
			info05File.flush();
			info06File.write(info06.toString());
			info06File.flush();
			humanetFile.write(hamnet.toString());
			humanetFile.flush();
		} catch (

				IOException e)

		{

			System.out.println("PrStatistics...");
			e.printStackTrace();
		}

	}

	private static void printTraceStatistics(String[] columns, Vector<Integer> budgets, Vector<Integer> deadlines,
			Map<Integer, Vector<Float>> neighborStats, String statFilePath) {

		System.out.println("size" + budgets.size());

		StringBuilder s05 = new StringBuilder();
		StringBuilder s06 = new StringBuilder();
		StringBuilder hu = new StringBuilder();
		FileWriter Info05, Info06, humanet;
		try {

			Info05 = new FileWriter("Inf05.dat");
			s05.append("# ");
			for (String st : columns) {
				s05.append(st + "\t");
			}
			s05.append("\n");

			Info06 = new FileWriter("Inf06.dat");
			s06.append("# ");
			for (String st : columns) {
				s06.append(st + "\t");
			}
			s06.append("\n");

			humanet = new FileWriter("Humanet.dat");
			hu.append("# ");
			for (String st : columns) {
				hu.append(st + "\t");
			}
			hu.append("\n");

			int index = 0;
			for (int b = 0; b < budgets.size(); b++) {
				for (int d = 0; d < deadlines.size(); d++) {
					System.out.println("size" + deadlines.size() + ":" + b * deadlines.size() + d);
					s05.append(budgets.get(b) + "\t" + deadlines.get(d) + "\t"
							+ neighborStats.get(b * deadlines.size() + d).get(index) + "\t"
							+ neighborStats.get(b * deadlines.size() + d).get(index + 1));
					s05.append("\n");
				}
				s05.append("\n");
			}

			index = 2;
			for (int b = 0; b < budgets.size(); b++) {
				for (int d = 0; d < deadlines.size(); d++) {
					s06.append(budgets.get(b) + "\t" + deadlines.get(d) + "\t"
							+ neighborStats.get(b * deadlines.size() + d).get(index) + "\t"
							+ neighborStats.get(b * deadlines.size() + d).get(index + 1));
					s06.append("\n");
				}
				s06.append("\n");
			}

			index = 4;
			for (int b = 0; b < budgets.size(); b++) {
				for (int d = 0; d < deadlines.size(); d++) {
					hu.append(budgets.get(b) + "\t" + deadlines.get(d) + "\t"
							+ neighborStats.get(b * deadlines.size() + d).get(index) + "\t"
							+ neighborStats.get(b * deadlines.size() + d).get(index + 1));
					hu.append("\n");
				}
				hu.append("\n");
			}

			Info05.write(s05.toString());
			Info05.flush();
			Info06.write(s06.toString());
			Info06.flush();
			humanet.write(hu.toString());
			humanet.flush();
		} catch (IOException e) {

			System.out.println("Print Statistics...");
			e.printStackTrace();
		}

	}

	//	public Statistic getFlopCoinDist(float level) {
	//
	//		return this.flopCoinsDist.get((int) level);
	//	}

	public float getoffloadDist(float level) {

		return this.offloadDist.get((int) level);
	}

	public float getBatteryDist(float level) {

		return this.batteryDist.get((int) level);
	}

	public float getUsefulBatteryDist(float level) {

		return this.usefulBatteryDist.get((int) level);
	}
	public Statistic getTrustDist(float level) {

		return this.trustDist.get((int) level);
	}

	public static void main(String[] argv) {

		/**
		 * @author Mahdieh
		 */
		// Path to different trace files
		String parentPath = "traces/";
		String infocom05Path = parentPath.concat("infocom05/contacts.Exp3.dat");
		String infocom06Path = parentPath.concat("infocom06/contacts.mobiles.Exp6.dat");
		String humanetPath = parentPath.concat("humanet/t_encounters.csv");
		String mdcPath = parentPath.concat("mdc/output.csv");

		Vector<Integer> deadline = new Vector<Integer>(Arrays.asList(new Integer[] { 5, 10, 20, 30, 40, 50 }));

		Vector<Float> multiLevels = new Vector<Float>(
				Arrays.asList(new Float[] { (float) 20, (float) 100 }));

		int simulationAlgorithm = Constants.SORT;
		Constants.APPLICATION = Constants.Framework.MIX;
		simulationAlgorithm = argv.length>0 ? (argv[0].equals("SORT")? Constants.SORT : (argv[0].equals("LAZY")? Constants.RANDOM: Constants.GREEDY)) : simulationAlgorithm;
		Constants.APPLICATION = argv.length>1 ? (argv[1].equals("OFF")? Constants.Framework.TASK_OFF : (argv[1].equals("FORW")? Constants.Framework.MES_FORW : Constants.Framework.TASK_OFF)): Constants.APPLICATION;

		int replicationNum = argv.length>2 ?  Integer.valueOf(argv[2]) : 2;		
		Constants.REPORT = false;
		if(simulationAlgorithm == Constants.SORT){
			Constants.REPORT = true;
		}
		System.out.println("HERE: "+ simulationAlgorithm + ":" + Constants.APPLICATION + ":" +replicationNum);
		Constants.BUSY = true;
		Task.DELAY_FACTOR = 30;
		Constants.consumeEnergyForLocal = true;
		Constants.FLOPCOIN_RECOM = 1;
		Constants.AFTER_LOCAL = 1;
		Constants.THETA_THRESHOLD = (float) 0.9;
		Constants.RESPECT_ALG = Constants.RESPECT.LOWER_THAN_ZERO;
		// BUDGET



		produceResults(multiLevels, simulationAlgorithm, replicationNum, parentPath, infocom05Path,infocom06Path, humanetPath, mdcPath,
				("result" + simulationAlgorithm));

		// produceResultsBudgets(budgets, multiLevels, simulationAlgorithm,
		// xAxis, replicationNum, parentPath,
		// infocom05Path, infocom06Path, humanetPath,
		// ("result" + simulationAlgorithm + Constants.AFTER_LOCAL +
		// Constants.FLOPCOIN_RECOM));

		simulationAlgorithm = Constants.GREEDY;
		System.out.println("------------------------------------------------------------");

		// produceResultsRandom(deadline, multiLevels, simulationAlgorithm,
		// xAxis2, replicationNum, parentPath,
		// infocom05Path, infocom06Path, humanetPath,
		// "result" + simulationAlgorithm + Constants.AFTER_LOCAL +
		// Constants.FLOPCOIN_RECOM);

		System.out.println("------------------------------------------------------------");

		simulationAlgorithm = Constants.RANDOM;

		// produceResultsRandom(deadline, multiLevels, simulationAlgorithm,
		// xAxis2, replicationNum, parentPath,
		// infocom05Path, infocom06Path, humanetPath,
		// "result" + simulationAlgorithm + Constants.AFTER_LOCAL +
		// Constants.FLOPCOIN_RECOM);

		// for (int type = 0; type <= 2; type++) {
		// Simulation.energyPlot.put(type, new Vector<Float>());
		// Simulation.offloadPlot.put(type, new Vector<Float>());
		// Simulation.trafficPlot.put(type, new Vector<Float>());
		// }
		//
		// simulationAlgorithm = Constants.GREEDY;
		// produceAllResults(multiLevels, simulationAlgorithm, replicationNum,
		// parentPath, infocom05Path, infocom06Path,
		// humanetPath, ("result" + simulationAlgorithm + Constants.AFTER_LOCAL
		// + Constants.FLOPCOIN_RECOM));
		//
		// System.out.println("------------------------------------------------------------");
		// simulationAlgorithm = Constants.FLOPCOIN_THETA;
		// produceAllResults(multiLevels, simulationAlgorithm, replicationNum,
		// parentPath, infocom05Path, infocom06Path,
		// humanetPath, ("result" + simulationAlgorithm + Constants.AFTER_LOCAL
		// + Constants.FLOPCOIN_RECOM));
		//
		// System.out.println("------------------------------------------------------------");
		// simulationAlgorithm = Constants.RANDOM;
		// produceAllResults(multiLevels, simulationAlgorithm, replicationNum,
		// parentPath, infocom05Path, infocom06Path,
		// humanetPath, ("result" + simulationAlgorithm + Constants.AFTER_LOCAL
		// + Constants.FLOPCOIN_RECOM));
		//
		// printAllResults();
		//
		// Constants.BATTERY = true;
		// Constants.TIMESTAMP_NUM = 6;
		//
		// for (int type = 0; type <= 3; type++) {
		// if (type != 2) {
		// Simulation.info05BatteryPlot.put(type, new Vector<Statistic>());
		// Simulation.info06BatteryPlot.put(type, new Vector<Statistic>());
		// Simulation.humanetBatteryPlot.put(type, new Vector<Statistic>());
		// }
		// }
		// simulationAlgorithm = Constants.GREEDY;
		// produceBatteryResults(multiLevels, simulationAlgorithm,
		// replicationNum, parentPath, infocom05Path,
		// infocom06Path, humanetPath,
		// ("result" + simulationAlgorithm + Constants.AFTER_LOCAL +
		// Constants.FLOPCOIN_RECOM));
		//
		// System.out.println("------------------------------------------------------------");
		// simulationAlgorithm = Constants.FLOPCOIN_THETA;
		// produceBatteryResults(multiLevels, simulationAlgorithm,
		// replicationNum, parentPath, infocom05Path,
		// infocom06Path, humanetPath,
		// ("result" + simulationAlgorithm + Constants.AFTER_LOCAL +
		// Constants.FLOPCOIN_RECOM));
		//
		// System.out.println("------------------------------------------------------------");
		// simulationAlgorithm = Constants.RANDOM;
		// produceBatteryResults(multiLevels, simulationAlgorithm,
		// replicationNum, parentPath, infocom05Path,
		// infocom06Path, humanetPath,
		// ("result" + simulationAlgorithm + Constants.AFTER_LOCAL +
		// Constants.FLOPCOIN_RECOM));
		//
		// printBatteryResults();

	}

	private static void produceBatteryResults(Vector<Float> levels, int algorithm, int replications, String parentPath,
			String infocom05Path, String infocom06Path, String humanetPath, String resultPath) {

		Map<Integer, Vector<Float>> neighborStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> trafficStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> neighborStdStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> trafficStdStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> batteryStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> neighborNumStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> totalStats = new HashMap<Integer, Vector<Float>>();
		int neighborPercent = 1;
		for (int type = 0; type <= 2; type++) {

			Vector<Float> minLevel = levels;
			// Alocating for that percent
			if (neighborStats.get(neighborPercent) == null)
				neighborStats.put(neighborPercent, new Vector<Float>());

			if (totalStats.get(neighborPercent) == null)
				totalStats.put(neighborPercent, new Vector<Float>());

			if (neighborNumStats.get(neighborPercent) == null)
				neighborNumStats.put(neighborPercent, new Vector<Float>());

			if (trafficStats.get(neighborPercent) == null)
				trafficStats.put(neighborPercent, new Vector<Float>());

			if (neighborStdStats.get(neighborPercent) == null)
				neighborStdStats.put(neighborPercent, new Vector<Float>());

			if (trafficStdStats.get(neighborPercent) == null)
				trafficStdStats.put(neighborPercent, new Vector<Float>());

			if (batteryStats.get(neighborPercent) == null)
				batteryStats.put(neighborPercent, new Vector<Float>());

			// used for chnaging hte level of battery threshold
			// minLevel = new Vector<Float>(
			// Arrays.asList(new Float[] {(float) neighborPercent*10}));

			Statistic AfterStartSum = new Statistic();
			Statistic totalSum = new Statistic();
			Statistic BeforeStartSum = new Statistic();
			Statistic uniqueResponsesReceived = new Statistic();
			Map<Integer, Statistic> flopCoin = new HashMap<Integer, Statistic>();
			Map<Integer, Statistic> trust = new HashMap<Integer, Statistic>();

			float meanBatteryLevel = 0;
			float varBatteryLevel = 0;
			Statistic neighborNum = new Statistic();
			Simulation replication = null;

			// Number of times to reach to a certain accuracy
			for (int i = 0; i < replications; i++) {
				Node.countNotStartedLocal = 0;
				Simulation.neighborNum = 0;
				Simulation.neighborNumTimes = 0;
				Constants.Dataset typeEqu = Constants.Dataset.values()[type];
				switch (typeEqu) {
				case INFOCOM_05:
					replication = new Simulation(
							new String[] { Integer.toString(type), infocom05Path, Integer.toString(algorithm) },
							minLevel);
					break;
				case INFOCOM_06:
					replication = new Simulation(
							new String[] { Integer.toString(type), infocom06Path, Integer.toString(algorithm) },
							minLevel);
					break;
				case HUMANET:
					replication = new Simulation(
							new String[] { Integer.toString(type), humanetPath, Integer.toString(algorithm) },
							minLevel);
					break;
				default:
					System.out.println("Inappripriate choice: " + type);
				}
				// Computing per each iteration
				AfterStartSum.addReplication((float) replication.trace.getNrInterruptedLocalTasks()
						/ (replication.trace.getNrExecutedLocalTasks()
								+ replication.trace.getNrInterruptedLocalTasks()));
				// AfterStartSum.addReplication((float)
				// replication.trace.getNrInterruptedLocalTasks());

				totalSum.addReplication((float) (replication.trace.getNrExecutedLocalTasks()
						+ replication.trace.getNrInterruptedLocalTasks()));

				System.out.println(replication.trace.getNrExecutedLocalTasks() + ":"
						+ replication.trace.getNrInterruptedLocalTasks() + ":"
						+ replication.trace.getNrOffloadRequests() + ":"
						+ replication.trace.getNrFutureOffloadRequests());

				BeforeStartSum
				.addReplication((float) Node.countNotStartedLocal / replication.trace.getNrExecutedLocalTasks()
						+ Node.countNotStartedLocal);

				uniqueResponsesReceived.addReplication((float) replication.trace.getNrOffloadRequestsReceived());

				meanBatteryLevel += replication.trace.getAveragePowerLevel(Constants.CONSTS.get(type).get(KEY.TRACE_END_CONSIDERING).doubleValue());
				varBatteryLevel += replication.trace.getStdDevOfPowerLevel(Constants.CONSTS.get(type).get(KEY.TRACE_END_CONSIDERING).doubleValue());
				neighborNum.addReplication(
						((float) replication.trace.getNrOffloadRequestsReceived()) );
			}

			// Gathering the results
			meanBatteryLevel /= replications;
			varBatteryLevel /= replications;

			if (Constants.AFTER_LOCAL == 1) {
				neighborStats.get(neighborPercent).add(AfterStartSum.getMean());
				neighborStats.get(neighborPercent).add(AfterStartSum.getError());

			} else {
				neighborStats.get(neighborPercent).add(BeforeStartSum.getMean());
				neighborStats.get(neighborPercent).add(BeforeStartSum.getError());
			}
			neighborNumStats.get(neighborPercent).add(neighborNum.getMean());

		}

		Simulation.printTaskStatistics(
				new String[] { "range", "Inf05-mean", "Inf05-std", "Inf06-mean", "Inf06-std", "Hmnt-mean", "Hmnt-std" },
				neighborNumStats, parentPath + resultPath + "neigbour.dat");

	}

	private static void produceAllResults(Vector<Float> levels, int algorithm, int replications, String parentPath,
			String infocom05Path, String infocom06Path, String humanetPath, String resultPath) {

		Map<Integer, Vector<Float>> neighborStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> trafficStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> neighborStdStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> trafficStdStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> batteryStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> neighborNumStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> totalStats = new HashMap<Integer, Vector<Float>>();
		int neighborPercent = 1;
		for (int type = 0; type <= 2; type++) {

			Vector<Float> minLevel = levels;
			// Alocating for that percent
			if (neighborStats.get(neighborPercent) == null)
				neighborStats.put(neighborPercent, new Vector<Float>());

			if (totalStats.get(neighborPercent) == null)
				totalStats.put(neighborPercent, new Vector<Float>());

			if (neighborNumStats.get(neighborPercent) == null)
				neighborNumStats.put(neighborPercent, new Vector<Float>());

			if (trafficStats.get(neighborPercent) == null)
				trafficStats.put(neighborPercent, new Vector<Float>());

			if (neighborStdStats.get(neighborPercent) == null)
				neighborStdStats.put(neighborPercent, new Vector<Float>());

			if (trafficStdStats.get(neighborPercent) == null)
				trafficStdStats.put(neighborPercent, new Vector<Float>());

			if (batteryStats.get(neighborPercent) == null)
				batteryStats.put(neighborPercent, new Vector<Float>());

			// used for chnaging hte level of battery threshold
			// minLevel = new Vector<Float>(
			// Arrays.asList(new Float[] {(float) neighborPercent*10}));

			Statistic AfterStartSum = new Statistic();
			Statistic totalSum = new Statistic();
			Statistic BeforeStartSum = new Statistic();
			Statistic uniqueResponsesReceived = new Statistic();
			Map<Integer, Statistic> flopCoin = new HashMap<Integer, Statistic>();
			Map<Integer, Statistic> trust = new HashMap<Integer, Statistic>();

			float meanBatteryLevel = 0;
			float varBatteryLevel = 0;
			Statistic neighborNum = new Statistic();
			Simulation replication = null;

			// Number of times to reach to a certain accuracy
			for (int i = 0; i < replications; i++) {
				Node.countNotStartedLocal = 0;
				Simulation.neighborNum = 0;
				Simulation.neighborNumTimes = 0;
				Constants.Dataset typeEqu = Constants.Dataset.values()[type];

				switch (typeEqu) {
				case INFOCOM_05:
					replication = new Simulation(
							new String[] { Integer.toString(type), infocom05Path, Integer.toString(algorithm) },
							minLevel);
					break;
				case INFOCOM_06:
					replication = new Simulation(
							new String[] { Integer.toString(type), infocom06Path, Integer.toString(algorithm) },
							minLevel);
					break;
				case HUMANET:
					replication = new Simulation(
							new String[] { Integer.toString(type), humanetPath, Integer.toString(algorithm) },
							minLevel);
					break;
				default:
					System.out.println("Inappripriate choice: " + type);
				}
				// Computing per each iteration
				AfterStartSum.addReplication((float) replication.trace.getNrInterruptedLocalTasks()
						/ (replication.trace.getNrExecutedLocalTasks()
								+ replication.trace.getNrInterruptedLocalTasks()));
				// AfterStartSum.addReplication((float)
				// replication.trace.getNrInterruptedLocalTasks());

				totalSum.addReplication((float) (replication.trace.getNrExecutedLocalTasks()
						+ replication.trace.getNrInterruptedLocalTasks()));

				System.out.println(replication.trace.getNrExecutedLocalTasks() + ":"
						+ replication.trace.getNrInterruptedLocalTasks() + ":"
						+ replication.trace.getNrOffloadRequests() + ":"
						+ replication.trace.getNrFutureOffloadRequests());

				BeforeStartSum
				.addReplication((float) Node.countNotStartedLocal / replication.trace.getNrExecutedLocalTasks()
						+ Node.countNotStartedLocal);

				// uniqueResponsesReceived.addReplication(((float)
				// replication.trace.getNrInterruptedLocalTasks())/
				// replication.trace.getNrOffloadRequestsReceived());

				uniqueResponsesReceived.addReplication(((float) replication.trace
						.getNrOffloadResultsReceived()/*
						 * replication.trace.
						 * getNrUniqueFinishedOffloadedTasks
						 * ()
						 */
						/ replication.trace.getNrInterruptedLocalTasks()));

				//	meanBatteryLevel += replication.trace.getAveragePowerLevel();
				//varBatteryLevel += replication.trace.getStdDevOfPowerLevel();
				//neighborNum.addReplication(replication.trace.getAveragePowerLevel());
			}

			// Gathering the results
			meanBatteryLevel /= replications;
			varBatteryLevel /= replications;

			if (Constants.AFTER_LOCAL == 1) {

				neighborStats.get(neighborPercent).add(AfterStartSum.getMean());
				neighborStats.get(neighborPercent).add(AfterStartSum.getError());

			} else {
				neighborStats.get(neighborPercent).add(BeforeStartSum.getMean());
				neighborStats.get(neighborPercent).add(BeforeStartSum.getError());
			}
			// neighborNumStats.get(neighborPercent).add(neighborNum.getMean());

			Simulation.energyPlot.get(type).add(neighborNum.getMean());
			Simulation.energyPlot.get(type).add(neighborNum.getError());

			Simulation.trafficPlot.get(type).add(uniqueResponsesReceived.getMean());
			Simulation.trafficPlot.get(type).add(uniqueResponsesReceived.getError());
		}

		Simulation.printTaskStatistics(
				new String[] { "range", "Inf05-mean", "Inf05-std", "Inf06-mean", "Inf06-std", "Hmnt-mean", "Hmnt-std" },
				neighborNumStats, parentPath + resultPath + "neigbour.dat");

	}

	/*

	private static void produceResultsBudgets(Vector<Integer> budgets, Vector<Float> levels, int algorithm,
			String xAxis, int replications, String parentPath, String infocom05Path, String infocom06Path,
			String humanetPath, String resultPath) {

		Vector<Integer> deadlines = new Vector<Integer>(Arrays.asList(new Integer[] { 5, 10, 20, 30, 40, 50 }));
		Vector<Integer> budgetPass = budgets;
		Map<Integer, Vector<Float>> neighborStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> trafficStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> neighborStdStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> trafficStdStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> batteryStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> neighborNumStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> totalStats = new HashMap<Integer, Vector<Float>>();
		int neighborPercent = 0;
		int size = deadlines.size() * budgets.size();
		System.out.println("Size:" + budgets.toString() + budgetPass.toString());
		// int index = 0;
		// for (int neighborPercent : budgets) {

		for (int index = 0; index < size; index++) {
			neighborPercent = index;
			if (xAxis.equals("BUDGET")) {
				Constants.TASK_FLOP_COIN_THRESHOLD = (int) neighborPercent;
			} else if (xAxis.equals("DEADLINE")) {
				Task.DELAY_FACTOR = (int) neighborPercent;
			} else if (xAxis.equals("BOTH")) {
				neighborPercent = index;
				System.out.println("Here" + index % deadlines.size() + ":" + index / deadlines.size());
				Task.DELAY_FACTOR = (int) deadlines.get(index % deadlines.size());
				Constants.TASK_FLOP_COIN_THRESHOLD = (int) budgets.get(index / deadlines.size());
			}

			for (int type = 0; type <= 2; type++) {

				Vector<Float> minLevel = levels;
				// Alocating for that percent
				if (neighborStats.get(neighborPercent) == null)
					neighborStats.put(neighborPercent, new Vector<Float>());

				if (totalStats.get(neighborPercent) == null)
					totalStats.put(neighborPercent, new Vector<Float>());

				if (neighborNumStats.get(neighborPercent) == null)
					neighborNumStats.put(neighborPercent, new Vector<Float>());

				if (trafficStats.get(neighborPercent) == null)
					trafficStats.put(neighborPercent, new Vector<Float>());

				if (neighborStdStats.get(neighborPercent) == null)
					neighborStdStats.put(neighborPercent, new Vector<Float>());

				if (trafficStdStats.get(neighborPercent) == null)
					trafficStdStats.put(neighborPercent, new Vector<Float>());

				if (batteryStats.get(neighborPercent) == null)
					batteryStats.put(neighborPercent, new Vector<Float>());

				// used for chnaging hte level of battery threshold
				// minLevel = new Vector<Float>(
				// Arrays.asList(new Float[] {(float) neighborPercent*10}));

				Statistic AfterStartSum = new Statistic();
				Statistic totalSum = new Statistic();
				Statistic BeforeStartSum = new Statistic();
				Statistic uniqueResponsesReceived = new Statistic();
				Map<Integer, Statistic> flopCoin = new HashMap<Integer, Statistic>();
				Map<Integer, Statistic> trust = new HashMap<Integer, Statistic>();

				for (float l : levels) {
					flopCoin.put((int) l, new Statistic());
					trust.put((int) l, new Statistic());
					// for (int i = 0; i < 5; i++) {
					// trust.get((int) l).add(i, new Statistic());

				}
				float meanBatteryLevel = 0;
				float varBatteryLevel = 0;
				Statistic neighborNum = new Statistic();
				Simulation replication = null;

				// Number of times to reach to a certain accuracy
				for (int i = 0; i < replications; i++) {
					Node.countNotStartedLocal = 0;
					Simulation.neighborNum = 0;
					Simulation.neighborNumTimes = 0;
					switch (type) {
					case Constants.INFOCOM_05:
						replication = new Simulation(
								new String[] { Integer.toString(type), infocom05Path, Integer.toString(algorithm) },
								minLevel);
						break;
					case Constants.INFOCOM_06:
						replication = new Simulation(
								new String[] { Integer.toString(type), infocom06Path, Integer.toString(algorithm) },
								minLevel);
						break;
					case Constants.HUMANET:
						replication = new Simulation(
								new String[] { Integer.toString(type), humanetPath, Integer.toString(algorithm) },
								minLevel);
						break;
					default:
						System.out.println("Inappripriate choice: " + type);
					}
					// Computing per each iteration
					AfterStartSum.addReplication((float) replication.trace.getNrInterruptedLocalTasks()
							/ (replication.trace.getNrExecutedLocalTasks()
									+ replication.trace.getNrInterruptedLocalTasks()));

					totalSum.addReplication((float) (replication.trace.getNrExecutedLocalTasks()
							+ replication.trace.getNrInterruptedLocalTasks()));

					System.out.println(replication.trace.getNrExecutedLocalTasks() + ":"
							+ replication.trace.getNrInterruptedLocalTasks() + ":"
							+ replication.trace.getNrOffloadRequests() + ":"
							+ replication.trace.getNrFutureOffloadRequests());

					BeforeStartSum.addReplication(
							(float) Node.countNotStartedLocal / replication.trace.getNrExecutedLocalTasks()
									+ Node.countNotStartedLocal);

					uniqueResponsesReceived.addReplication((float) replication.trace.getNrUniqueFinishedOffloadedTasks()
							/ replication.trace.getNrOffloadResultsReceived());

					meanBatteryLevel += replication.trace.getAveragePowerLevel();
					varBatteryLevel += replication.trace.getStdDevOfPowerLevel();
					neighborNum.addReplication(replication.neighborNum / replication.neighborNumTimes);
					System.out.println("Algorithm: " + algorithm + "Trace: " + type + "Offloading Requests Sent: "
							+ replication.neighborNum);
				}

				// Gathering the results
				meanBatteryLevel /= replications;
				varBatteryLevel /= replications;

				if (Constants.AFTER_LOCAL == 1) {
					neighborStats.get(neighborPercent).add(AfterStartSum.getMean());
					// neighborStats.get(neighborPercent).add(totalSum.getMean());
					neighborStats.get(neighborPercent).add(AfterStartSum.getError());

				} else {
					neighborStats.get(neighborPercent).add(BeforeStartSum.getMean());
					neighborStats.get(neighborPercent).add(BeforeStartSum.getError());
				}
				trafficStats.get(neighborPercent).add(uniqueResponsesReceived.getMean());
				trafficStats.get(neighborPercent).add(uniqueResponsesReceived.getError());
				trafficStdStats.get(neighborPercent).add(uniqueResponsesReceived.getError());
				batteryStats.get(neighborPercent).add(meanBatteryLevel);
				batteryStats.get(neighborPercent).add(varBatteryLevel);
				neighborNumStats.get(neighborPercent).add(neighborNum.getMean());

			}
		}
		System.out.println(budgetPass.size());
		Simulation.printTraceStatistics(new String[] { "Budget", "Deadline", "Prob", "Error" }, budgetPass, deadlines,
				neighborStats, parentPath + resultPath + ".dat");

		// Simulation.printTaskStatistics(
		// new String[] { "range", "Inf05-mean", "Inf05-std", "Inf06-mean",
		// "Inf06-std", "Hmnt-mean", "Hmnt-std" },
		// neighborStats, parentPath + resultPath + ".dat");

		Simulation.printTaskStatistics(
				new String[] { "range", "Inf05-mean", "Inf05-std", "Inf06-mean", "Inf06-std", "Hmnt-mean", "Hmnt-std" },
				trafficStats, parentPath + resultPath + "Traffic.dat");
		// Simulation.printTaskStatistics(new String[] { "range", "Inf05-std",
		// "Inf06-std", "Hmnt-std" }, trafficStdStats,
		// parentPath + resultPath + "TrafficError.dat");
		Simulation.printTaskStatistics(
				new String[] { "range", "Inf05-mean", "Inf05-std", "Inf06-mean", "Inf06-std", "Hmnt-mean", "Hmnt-std" },
				batteryStats, parentPath + resultPath + "Battery.dat");
		Simulation.printTaskStatistics(new String[] { "range", "Inf05-std", "Inf06-std", "Hmnt-std" }, neighborNumStats,
				parentPath + resultPath + "Neigbor.dat");

	}

	private static void produceResultsRandom(Vector<Integer> budgets, Vector<Float> levels, int algorithm, String xAxis,
			int replications, String parentPath, String infocom05Path, String infocom06Path, String humanetPath,
			String resultPath) {
		System.out.println(budgets);

		Map<Integer, Vector<Float>> neighborStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> trafficStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> neighborStdStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> trafficStdStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> batteryStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> neighborNumStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> totalStats = new HashMap<Integer, Vector<Float>>();

		for (int neighborPercent : budgets) {
			if (xAxis.equals("BUDGET")) {
				Constants.TASK_FLOP_COIN_THRESHOLD = (int) neighborPercent;
			} else if (xAxis.equals("DEADLINE")) {
				System.out.println("Here:" + neighborPercent);
				Task.DELAY_FACTOR = (int) neighborPercent;
			}

			for (int type = 0; type <= 2; type++) {

				Vector<Float> minLevel = levels;
				// Alocating for that percent
				if (neighborStats.get(neighborPercent) == null)
					neighborStats.put(neighborPercent, new Vector<Float>());

				if (totalStats.get(neighborPercent) == null)
					totalStats.put(neighborPercent, new Vector<Float>());

				if (neighborNumStats.get(neighborPercent) == null)
					neighborNumStats.put(neighborPercent, new Vector<Float>());

				if (trafficStats.get(neighborPercent) == null)
					trafficStats.put(neighborPercent, new Vector<Float>());

				if (neighborStdStats.get(neighborPercent) == null)
					neighborStdStats.put(neighborPercent, new Vector<Float>());

				if (trafficStdStats.get(neighborPercent) == null)
					trafficStdStats.put(neighborPercent, new Vector<Float>());

				if (batteryStats.get(neighborPercent) == null)
					batteryStats.put(neighborPercent, new Vector<Float>());

				// used for chnaging hte level of battery threshold
				// minLevel = new Vector<Float>(
				// Arrays.asList(new Float[] {(float) neighborPercent*10}));

				Statistic AfterStartSum = new Statistic();
				Statistic totalSum = new Statistic();
				Statistic BeforeStartSum = new Statistic();
				Statistic uniqueResponsesReceived = new Statistic();
				Map<Integer, Statistic> flopCoin = new HashMap<Integer, Statistic>();
				Map<Integer, Statistic> trust = new HashMap<Integer, Statistic>();

				for (float l : levels) {
					flopCoin.put((int) l, new Statistic());
					trust.put((int) l, new Statistic());
					// for (int i = 0; i < 5; i++) {
					// trust.get((int) l).add(i, new Statistic());

				}
				float meanBatteryLevel = 0;
				float varBatteryLevel = 0;
				Statistic neighborNum = new Statistic();
				Simulation replication = null;

				// Number of times to reach to a certain accuracy
				for (int i = 0; i < replications; i++) {
					Node.countNotStartedLocal = 0;
					Simulation.neighborNum = 0;
					Simulation.neighborNumTimes = 0;
					switch (type) {
					case Constants.INFOCOM_05:
						replication = new Simulation(
								new String[] { Integer.toString(type), infocom05Path, Integer.toString(algorithm) },
								minLevel);
						break;
					case Constants.INFOCOM_06:
						replication = new Simulation(
								new String[] { Integer.toString(type), infocom06Path, Integer.toString(algorithm) },
								minLevel);
						break;
					case Constants.HUMANET:
						replication = new Simulation(
								new String[] { Integer.toString(type), humanetPath, Integer.toString(algorithm) },
								minLevel);
						break;
					default:
						System.out.println("Inappripriate choice: " + type);
					}
					// Computing per each iteration
					AfterStartSum.addReplication((float) replication.trace.getNrInterruptedLocalTasks()
							/ (replication.trace.getNrExecutedLocalTasks()
									+ replication.trace.getNrInterruptedLocalTasks()));

					totalSum.addReplication((float) (replication.trace.getNrExecutedLocalTasks()
							+ replication.trace.getNrInterruptedLocalTasks()));

					System.out.println(replication.trace.getNrExecutedLocalTasks() + ":"
							+ replication.trace.getNrInterruptedLocalTasks() + ":"
							+ replication.trace.getNrOffloadRequests() + ":"
							+ replication.trace.getNrFutureOffloadRequests());

					BeforeStartSum.addReplication(
							(float) Node.countNotStartedLocal / replication.trace.getNrExecutedLocalTasks()
									+ Node.countNotStartedLocal);

					uniqueResponsesReceived.addReplication((float) replication.trace.getNrUniqueFinishedOffloadedTasks()
							/ replication.trace.getNrOffloadResultsReceived());

					meanBatteryLevel += replication.trace.getAveragePowerLevel();
					varBatteryLevel += replication.trace.getStdDevOfPowerLevel();
					neighborNum.addReplication(replication.neighborNum / replication.neighborNumTimes);
					System.out.println("Algorithm: " + algorithm + "Trace: " + type + "Offloading Requests Sent: "
							+ replication.neighborNum);
				}

				// Gathering the results
				meanBatteryLevel /= replications;
				varBatteryLevel /= replications;

				if (Constants.AFTER_LOCAL == 1) {
					neighborStats.get(neighborPercent).add(AfterStartSum.getMean());
					// neighborStats.get(neighborPercent).add(totalSum.getMean());
					neighborStats.get(neighborPercent).add(AfterStartSum.getError());

				} else {
					neighborStats.get(neighborPercent).add(BeforeStartSum.getMean());
					neighborStats.get(neighborPercent).add(BeforeStartSum.getError());
				}
				trafficStats.get(neighborPercent).add(uniqueResponsesReceived.getMean());
				trafficStats.get(neighborPercent).add(uniqueResponsesReceived.getError());
				trafficStdStats.get(neighborPercent).add(uniqueResponsesReceived.getError());
				batteryStats.get(neighborPercent).add(meanBatteryLevel);
				batteryStats.get(neighborPercent).add(varBatteryLevel);
				neighborNumStats.get(neighborPercent).add(neighborNum.getMean());

			}
		}
		// Simulation.printTraceStatistics(new String[] { "Budget", "Deadline",
		// "Prob", "Error" }, budgetPass, deadlines,
		// neighborStats, parentPath + resultPath + ".dat");

		Simulation.printTaskStatistics(
				new String[] { "range", "Inf05-mean", "Inf05-std", "Inf06-mean", "Inf06-std", "Hmnt-mean", "Hmnt-std" },
				neighborStats, parentPath + resultPath + ".dat");

		Simulation.printTaskStatistics(
				new String[] { "range", "Inf05-mean", "Inf05-std", "Inf06-mean", "Inf06-std", "Hmnt-mean", "Hmnt-std" },
				trafficStats, parentPath + resultPath + "Traffic.dat");
		// Simulation.printTaskStatistics(new String[] { "range", "Inf05-std",
		// "Inf06-std", "Hmnt-std" }, trafficStdStats,
		// parentPath + resultPath + "TrafficError.dat");
		Simulation.printTaskStatistics(
				new String[] { "range", "Inf05-mean", "Inf05-std", "Inf06-mean", "Inf06-std", "Hmnt-mean", "Hmnt-std" },
				batteryStats, parentPath + resultPath + "Battery.dat");
		Simulation.printTaskStatistics(new String[] { "range", "Inf05-std", "Inf06-std", "Hmnt-std" }, neighborNumStats,
				parentPath + resultPath + "Neigbor.dat");

	}

	 */

	private static void produceResults(Vector<Float> levels, int algorithm, int replications,
			String parentPath, String infocom05Path, String infocom06Path, String humanetPath,String mdcPath,  String resultPath) {

		Map<Integer, Vector<Float>> batteryStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> trustStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> offloadStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> positiveSimulationResultStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> negativeSimulationResultStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> negativeSimulationCoinStats = new HashMap<Integer, Vector<Float>>();
		Map<Integer, Vector<Float>> negativeSimulationNoStats = new HashMap<Integer, Vector<Float>>();


		//for (float level : levels) {

		//}


		for(int percent=5; percent<50; percent+=5){

			trustStats.put((int) percent, new Vector<Float>());
			offloadStats.put((int) percent, new Vector<Float>());
			positiveSimulationResultStats.put((int) percent, new Vector<Float>());
			negativeSimulationResultStats.put((int) percent, new Vector<Float>());
			negativeSimulationCoinStats.put((int) percent, new Vector<Float>());
			negativeSimulationNoStats.put((int) percent, new Vector<Float>());
			batteryStats.put((int) percent, new Vector<Float>());

			for (int type = 0; type <= 3; type++) {			
				Vector<Float> minLevel = levels;

				// used for chnaging hte level of battery threshold
				// minLevel = new Vector<Float>(
				// Arrays.asList(new Float[] {(float) neighborPercent*10}));

				Map<Integer, Statistic> trust = new HashMap<Integer, Statistic>();
				Map<Integer, Statistic> offload = new HashMap<Integer, Statistic>();
				Map<Integer, Statistic> battery = new HashMap<Integer, Statistic>();
				Map<Integer, Statistic> usefulBattery = new HashMap<Integer, Statistic>();
				Map<Integer, Statistic> positiveSimulationResult = new HashMap<Integer, Statistic>();
				Map<Integer, Statistic> negativeSimulationResult = new HashMap<Integer, Statistic>();
				Map<Integer, Statistic> negativeSimulationCoin = new HashMap<Integer, Statistic>();
				Map<Integer, Statistic> negativeSimulationNo = new HashMap<Integer, Statistic>();
				//float meanBatteryLevel = 0;
				//float varBatteryLevel = 0;


				for (float l : levels) {
					trust.put((int) l, new Statistic());
					battery.put((int) l, new Statistic());
					usefulBattery.put((int) l, new Statistic());
					offload.put((int) l, new Statistic());
					positiveSimulationResult.put((int) l, new Statistic());
					negativeSimulationResult.put((int) l, new Statistic());
					negativeSimulationCoin.put((int) l, new Statistic());
					negativeSimulationNo.put((int) l, new Statistic());
				}

				Simulation replication = null;
				Constants.Dataset typeEqu = Constants.Dataset.values()[type];

				// Number of times to reach to a certain accuracy
				for (int i = 0; i < replications; i++) {
					Node.countNotStartedLocal = 0;
					Simulation.neighborNum = 0;
					Simulation.neighborNumTimes = 0;
					switch (typeEqu) {
					case INFOCOM_05:
						replication = new Simulation(
								new String[] { Integer.toString(type), infocom05Path, Integer.toString(algorithm), Integer.toString(percent)},
								minLevel);
						break;
					case INFOCOM_06:
						replication = new Simulation(
								new String[] { Integer.toString(type), infocom06Path, Integer.toString(algorithm), Integer.toString(percent) },
								minLevel);
						break;
					case HUMANET:
						replication = new Simulation(
								new String[] { Integer.toString(type), humanetPath, Integer.toString(algorithm), Integer.toString(percent) },
								minLevel);
						break;
					case NO_TRACE:
						replication = new Simulation(
								new String[] { Integer.toString(type), humanetPath, Integer.toString(algorithm), Integer.toString(percent) },
								minLevel);
						break;
					case MDC:		
						replication = new Simulation(
								new String[] { Integer.toString(type), mdcPath, Integer.toString(algorithm), Integer.toString(percent) },
								minLevel);
						break;
					default:
						System.out.println("Inappripriate choice: " + type);
					}

					//				System.out.println(replication.trace.getNrExecutedLocalTasks() + ":"
					//						+ replication.trace.getNrInterruptedLocalTasks() + ":"
					//						+ replication.trace.getNrOffloadRequests() + ":"
					//						+ replication.trace.getNrFutureOffloadRequests());


					if(algorithm == Constants.SORT || algorithm == Constants.FLOPCOIN_THETA){
						replication.setTrustDist();
						replication.setInteractionDist();

					}
					replication.setOffloadDist();
					replication.setBatteryDist();
					System.out.println("Offload ....");

					for (float level : levels) {
						if(algorithm == Constants.SORT || algorithm == Constants.SORT){
							trust.get((int) level).addReplication(replication.getTrustDist(((int) level)));
							positiveSimulationResult.get((int) level).addReplication(
									replication.getInteractionDist(Constants.Interactions.POSITIVE_GET_BACK_RESULT_III, ((int) level)));
							negativeSimulationResult.get((int) level).addReplication(
									replication.getInteractionDist(Constants.Interactions.NEGATIVE_GET_BACK_RESULT_I, ((int) level)));
							negativeSimulationCoin.get((int) level).addReplication(
									replication.getInteractionDist(Constants.Interactions.NEGATIVE_GIVE_BACK_COIN_II, ((int) level)));
							negativeSimulationNo.get((int) level).addReplication(
									replication.getInteractionDist(Constants.Interactions.NEGATIVE_NOT_HONEST_IV, ((int) level)));
						}
						offload.get((int) level).addReplication(replication.getoffloadDist((int) level));
						battery.get((int) level).addReplication(replication.getBatteryDist((int) level));
						usefulBattery.get((int) level).addReplication(replication.getUsefulBatteryDist((int) level));


					}

					//meanBatteryLevel += replication.trace.getAveragePowerLevel(Constants.CONSTS.get(typeEqu).get(KEY.TRACE_END_CONSIDERING).intValue());
					//varBatteryLevel += replication.trace.getStdDevOfPowerLevel(Constants.CONSTS.get(typeEqu).get(KEY.TRACE_END_CONSIDERING).intValue());
					System.out.println("Algorithm: " + algorithm + "Trace: " + type + "Offloading Requests Sent: "
							+ replication.neighborNum);
				}

				// Gathering the results
				//meanBatteryLevel /= replications;
				//varBatteryLevel /= replications;
				//batteryStats.get(index).add(meanBatteryLevel);
				//batteryStats.get(index).add(varBatteryLevel);

				for (float level : levels) {
					if(algorithm == Constants.SORT || algorithm == Constants.FLOPCOIN_THETA){
						trustStats.get((int) percent).add(trust.get((int) level).getMin());
						trustStats.get((int) percent).add(trust.get((int) level).getPercentile(25));
						trustStats.get((int) percent).add(trust.get((int) level).getMean());
						trustStats.get((int) percent).add(trust.get((int) level).getPercentile(75));
						trustStats.get((int) percent).add(trust.get((int) level).getMax());

						positiveSimulationResultStats.get((int) percent).add(positiveSimulationResult.get((int) level).getMin());
						positiveSimulationResultStats.get((int) percent).add(positiveSimulationResult.get((int) level).getMean());
						positiveSimulationResultStats.get((int) percent).add(positiveSimulationResult.get((int) level).getMax());

						negativeSimulationResultStats.get((int) percent).add(negativeSimulationResult.get((int) level).getMin());
						negativeSimulationResultStats.get((int) percent).add(negativeSimulationResult.get((int) level).getMean());
						negativeSimulationResultStats.get((int) percent).add(negativeSimulationResult.get((int) level).getMax());

						negativeSimulationCoinStats.get((int) percent).add(negativeSimulationCoin.get((int) level).getMin());
						negativeSimulationCoinStats.get((int) percent).add(negativeSimulationCoin.get((int) level).getMean());
						negativeSimulationCoinStats.get((int) percent).add(negativeSimulationCoin.get((int) level).getMax());

						negativeSimulationNoStats.get((int) percent).add(negativeSimulationNo.get((int) level).getMin());
						negativeSimulationNoStats.get((int) percent).add(negativeSimulationNo.get((int) level).getMean());
						negativeSimulationNoStats.get((int) percent).add(negativeSimulationNo.get((int) level).getMax());
					}
					offloadStats.get((int) percent).add(offload.get((int) level).getMean());
					//offloadStats.get((int) percent).add(offload.get((int) level));
					batteryStats.get((int) percent).add(battery.get((int) level).getMean());
					batteryStats.get((int) percent).add(battery.get((int) level).getError());
					batteryStats.get((int) percent).add(usefulBattery.get((int) level).getMean());
					batteryStats.get((int) percent).add(usefulBattery.get((int) level).getError());

				}
				if(Constants.REPORT){
					Simulation.printReportResults(levels,parentPath, typeEqu, percent);
				}
			}}



		if(algorithm == Constants.SORT || algorithm == Constants.SORT){
			Simulation.printTaskStatistics(
					new String[] { "Battery-Th", "Inf05-min", "Inf05-25th", "Inf05-50th", "Inf05-75th", "Inf05-max",
							"Inf06-min", "Inf06-25th", "Inf06-50th", "Inf06-75th", "Inf06-max", "Hmnt-min", "Hmnt-25th",
							"Hmnt-50th", "Hmnt-75th", "Hmnt-max" },
					positiveSimulationResultStats, parentPath + resultPath + "positive.dat");
			Simulation.printTaskStatistics(
					new String[] { "Battery-Th", "Inf05-min", "Inf05-25th", "Inf05-50th", "Inf05-75th", "Inf05-max",
							"Inf06-min", "Inf06-25th", "Inf06-50th", "Inf06-75th", "Inf06-max", "Hmnt-min", "Hmnt-25th",
							"Hmnt-50th", "Hmnt-75th", "Hmnt-max" },
					negativeSimulationResultStats, parentPath + resultPath + "negativeResult.dat");

			Simulation.printTaskStatistics(
					new String[] { "Battery-Th", "Inf05-min", "Inf05-25th", "Inf05-50th", "Inf05-75th", "Inf05-max",
							"Inf06-min", "Inf06-25th", "Inf06-50th", "Inf06-75th", "Inf06-max", "Hmnt-min", "Hmnt-25th",
							"Hmnt-50th", "Hmnt-75th", "Hmnt-max" },
					negativeSimulationCoinStats, parentPath + resultPath + "negativeCoin.dat");

			Simulation.printTaskStatistics(
					new String[] { "Battery-Th", "Inf05-min", "Inf05-25th", "Inf05-50th", "Inf05-75th", "Inf05-max",
							"Inf06-min", "Inf06-25th", "Inf06-50th", "Inf06-75th", "Inf06-max", "Hmnt-min", "Hmnt-25th",
							"Hmnt-50th", "Hmnt-75th", "Hmnt-max" },
					negativeSimulationNoStats, parentPath + resultPath + "negativeNo.dat");


			Simulation
			.printTaskStatistics(
					new String[] { "Battery-Th", "Inf05", "Inf05", 
							"Inf06", "Inf06",
							"Hmnt", "Hmnt",  "mdc", "mdc"},
					trustStats, parentPath + resultPath + "Trust.dat");
		}
		Simulation
		.printTaskStatistics(
				new String[] { "Battery-Th", "Inf05", "Inf05", 
						"Inf06", "Inf06",
						"Hmnt", "Hmnt",  "mdc", "mdc"},
				offloadStats, parentPath + resultPath + "Offload.dat");

		Simulation.printTaskStatistics(
				new String[] { "range", "Battery-Th", "Inf05-mean", "Inf05-error", 
						"Inf06-mean", "Inf06-error",
						"Hmnt-mean", "Hmnt-error",  "mdc-mean", "mdc-error" },
				batteryStats, parentPath + resultPath + "Battery.dat");

	}

}
