package it.uniroma1.di.kosta;

public enum NodeStatus {
  HAS_TASK_FOR_OFFLOAD, RUNNING, WAITING
}
