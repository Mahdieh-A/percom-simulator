package it.uniroma1.di.kosta;

public interface Recommendation {

	public float getMetric();
}
