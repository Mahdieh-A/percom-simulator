package it.uniroma1.di.kosta;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class Constants {

	
	enum Dataset {INFOCOM_05,INFOCOM_06, HUMANET,  MDC, NO_TRACE};
	// Types of Interaction
	enum Interactions {
	POSITIVE_GET_BACK_RESULT_III, NEGATIVE_GET_BACK_RESULT_I, NEGATIVE_GIVE_BACK_COIN_II, NEGATIVE_NOT_HONEST_IV,
	}
	//types of devices
	enum Devices {WAERABLE_DEVICES, ENTRY_LEVEL_DEVICES, MID_RANGE_DEVICES, HIGH_END_DEVICES,}
	enum RESPECT {LOWER_THAN_ZERO, LOWER_THAN_YOU};
	enum Framework {TASK_OFF, MES_FORW, MIX};
	// Kind of simulation
	public static final int RANDOM = 0;
	public static final int GREEDY = 1;
	public static final int FLOPCOIN_THETA = 2;
	public static final int SORT = 3;
	public static final int maxSizeForSORT = 20; // used in the SORT algorithm
	public static RESPECT RESPECT_ALG = RESPECT.LOWER_THAN_ZERO;
	public static Framework APPLICATION = Framework.TASK_OFF;
	public static final int READ_AT_ONCE = 0; //read the trace file once
	public static final int READ_NOT_AT_ONCE = 1; //read the file in more iterations
	public static final int JOULES_PER_PERCENT = 4000; // JOULES that every
	// percent of battery
	// level gives us

	public static int AFTER_LOCAL = 1;
	public static int FLOPCOIN_RECOM = 1;
	public static boolean BUSY = true;
	public static float THETA_THRESHOLD = (float) 0.9;
	//public static int INITIAL_FLOP_COIN = 8000;
	//public static int TASK_FLOP_COIN_THRESHOLD = 2000;
	public static boolean consumeEnergyForLocal = true;

	// for reporting battery as time passes
	public static boolean REPORT = true;
	public static int TIMESTAMP_NUM = 30;
	
	enum Commands {
		YES,
		BUSY,
		BAD_REPUTATION,
		NO;
	}

	public static final int fileBattery = 1;
	public static final int numberBattery = 0;

	/**
	 * 
	 * @author sokol
	 *
	 */
	enum KEY {
		FIRST_NODE_ID, LAST_NODE_ID, TRACE_START, // Real trace start time
		TRACE_END, // Real trace end time
		TRACE_START_CONSIDERING, // Consider only events with timestamp > this
		// time
		TRACE_END_CONSIDERING, // Consider only events with timestamp < this
		// time
		NR_TASKS_PER_NODE, TASKS_START_GENERATING, TASKS_END_GENERATING, TASKS_ARRIVAL_RATE, NR_CHOSE_NEIGHBORS, // The
		// number
		// of
		// neighbors
		// one
		// node
		// will
		// choose
		// to
		// send
		// the
		// task
		// for offload
		CONTACT_DUR_THRESHOLD_MIN, CONTACT_DUR_THRESHOLD_MAX,
		SIMULATION_TYPE, TRACE_TYPE, BATTERY, 
	}

	public static final Map<Dataset, EnumMap<KEY, Number>> CONSTS = new HashMap<Dataset, EnumMap<KEY, Number>>();

	static {
		//////////////////////////////////////////////////////////////////////////////////////////
		// Infocom05: 41 nodes, start=20821, end=274883
		// NR_TASKS_PER_NODE=100 and TASKS_ARRIVAL_RATE=1.0/1900 assures tasks
		// during all the trace duration
		//////////////////////////////////////////////////////////////////////////////////////////
		EnumMap<KEY, Number> infocom05 = new EnumMap<>(KEY.class);
		infocom05.put(KEY.FIRST_NODE_ID, 1);
		infocom05.put(KEY.LAST_NODE_ID, 41);
		infocom05.put(KEY.TRACE_START, 20821);
		infocom05.put(KEY.TRACE_END, 274883);
		infocom05.put(KEY.TRACE_START_CONSIDERING, 70000);
		infocom05.put(KEY.TRACE_END_CONSIDERING, 120000);
		infocom05.put(KEY.NR_TASKS_PER_NODE, 100);
		infocom05.put(KEY.TASKS_ARRIVAL_RATE, 1.0 / 180);
		infocom05.put(KEY.TASKS_START_GENERATING, 73600);
		infocom05.put(KEY.TASKS_END_GENERATING, 110000);
		infocom05.put(KEY.NR_CHOSE_NEIGHBORS, 100);
		infocom05.put(KEY.CONTACT_DUR_THRESHOLD_MIN, 0);
		infocom05.put(KEY.CONTACT_DUR_THRESHOLD_MAX, 24*3600);
		infocom05.put(KEY.SIMULATION_TYPE, RANDOM);
		infocom05.put(KEY.TRACE_TYPE, READ_AT_ONCE);
		infocom05.put(KEY.BATTERY, Constants.numberBattery);
		CONSTS.put(Dataset.INFOCOM_05, infocom05);
		//////////////////////////////////////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////////////////////////////////////
		// Infocom06: 78 nodes, start=3782, end=419671
		// NR_TASKS_PER_NODE=100 and TASKS_ARRIVAL_RATE= assures tasks
		// during all the trace duration
		//////////////////////////////////////////////////////////////////////////////////////////
		EnumMap<KEY, Number> infocom06 = new EnumMap<>(KEY.class);
		infocom06.put(KEY.FIRST_NODE_ID, 1);
		infocom06.put(KEY.LAST_NODE_ID, 78);
		infocom06.put(KEY.TRACE_START, 3782);
		infocom06.put(KEY.TRACE_END, 419671);
		infocom06.put(KEY.TRACE_START_CONSIDERING, 50000);
		infocom06.put(KEY.TRACE_END_CONSIDERING, 100000);
		infocom06.put(KEY.NR_TASKS_PER_NODE, 100);
		infocom06.put(KEY.TASKS_ARRIVAL_RATE, 1.0 / 180);
		infocom06.put(KEY.TASKS_START_GENERATING, 53600);
		infocom06.put(KEY.TASKS_END_GENERATING, 90000);
		infocom06.put(KEY.CONTACT_DUR_THRESHOLD_MIN, 0);
		infocom06.put(KEY.CONTACT_DUR_THRESHOLD_MAX, 24*3600);
		infocom06.put(KEY.NR_CHOSE_NEIGHBORS, 100);
		infocom06.put(KEY.SIMULATION_TYPE, RANDOM);
		infocom06.put(KEY.TRACE_TYPE, READ_AT_ONCE);
		infocom06.put(KEY.BATTERY, Constants.numberBattery);
		CONSTS.put(Dataset.INFOCOM_06, infocom06);
		//////////////////////////////////////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////////////////////////////////////
		// Humanet: 56 nodes, start=, end=
		// NR_TASKS_PER_NODE=100 and TASKS_ARRIVAL_RATE= assures tasks
		// during all the trace duration
		//////////////////////////////////////////////////////////////////////////////////////////
		EnumMap<KEY, Number> humanet = new EnumMap<>(KEY.class);
		humanet.put(KEY.FIRST_NODE_ID, 1);
		humanet.put(KEY.LAST_NODE_ID, 56);
		humanet.put(KEY.TRACE_START, 1290556416);
		humanet.put(KEY.TRACE_END, 1290601611);
		humanet.put(KEY.TRACE_START_CONSIDERING, 1290556416);
		humanet.put(KEY.TRACE_END_CONSIDERING, 1290601611);
		humanet.put(KEY.NR_TASKS_PER_NODE, 100);
		humanet.put(KEY.TASKS_ARRIVAL_RATE, 1.0 / 180);
		humanet.put(KEY.TASKS_START_GENERATING, 1290560000);
		humanet.put(KEY.TASKS_END_GENERATING, 1290600000);
		humanet.put(KEY.CONTACT_DUR_THRESHOLD_MIN, 0);
		humanet.put(KEY.CONTACT_DUR_THRESHOLD_MAX, 24*3600);
		humanet.put(KEY.NR_CHOSE_NEIGHBORS, 100);
		humanet.put(KEY.SIMULATION_TYPE, RANDOM);
		humanet.put(KEY.TRACE_TYPE, READ_AT_ONCE);
		humanet.put(KEY.BATTERY, Constants.numberBattery);
		CONSTS.put(Dataset.HUMANET, humanet);

		EnumMap<KEY, Number> noTrace = new EnumMap<>(KEY.class);
		noTrace.put(KEY.FIRST_NODE_ID, 1);
		noTrace.put(KEY.LAST_NODE_ID, 50);
		noTrace.put(KEY.TRACE_START, 0);
		noTrace.put(KEY.TRACE_END, 100000);
		noTrace.put(KEY.TRACE_START_CONSIDERING, 0);
		noTrace.put(KEY.TRACE_END_CONSIDERING, 100000);
		noTrace.put(KEY.NR_TASKS_PER_NODE, 200);
		noTrace.put(KEY.TASKS_ARRIVAL_RATE, 1.0 / 50);
		noTrace.put(KEY.TASKS_START_GENERATING, 1000);
		noTrace.put(KEY.TASKS_END_GENERATING, 99000);
		noTrace.put(KEY.CONTACT_DUR_THRESHOLD_MIN, 0);
		noTrace.put(KEY.CONTACT_DUR_THRESHOLD_MAX, 24*3600);
		noTrace.put(KEY.NR_CHOSE_NEIGHBORS, 100);
		noTrace.put(KEY.SIMULATION_TYPE, RANDOM);
		noTrace.put(KEY.TRACE_TYPE, READ_AT_ONCE);
		noTrace.put(KEY.BATTERY, Constants.numberBattery);
		CONSTS.put(Dataset.NO_TRACE, noTrace);


		EnumMap<KEY, Number> mdc = new EnumMap<>(KEY.class);
		mdc.put(KEY.FIRST_NODE_ID, 5449);
		mdc.put(KEY.LAST_NODE_ID, 5995);
		mdc.put(KEY.TRACE_START, 1251759602);
		mdc.put(KEY.TRACE_END, 1301612400);
		mdc.put(KEY.TRACE_START_CONSIDERING, 1251759602);
		mdc.put(KEY.TRACE_END_CONSIDERING, 1254351602);
		mdc.put(KEY.NR_TASKS_PER_NODE, 1000);
		mdc.put(KEY.TASKS_ARRIVAL_RATE, 1.0 / 1000);
		mdc.put(KEY.TASKS_START_GENERATING, 1251846002);//after 1 day
		mdc.put(KEY.TASKS_END_GENERATING, 1254265202);
		mdc.put(KEY.CONTACT_DUR_THRESHOLD_MIN, 0);
		mdc.put(KEY.CONTACT_DUR_THRESHOLD_MAX, 30*24*3600);
		mdc.put(KEY.NR_CHOSE_NEIGHBORS, 100);
		mdc.put(KEY.SIMULATION_TYPE, RANDOM);
		mdc.put(KEY.TRACE_TYPE, READ_NOT_AT_ONCE);
		mdc.put(KEY.BATTERY, Constants.numberBattery);
		CONSTS.put(Dataset.MDC, mdc);
	}



	public static final double UL_RATE = 10 * 1024 * 1024; // upload rate in
	// Mb/s
	public static final double DL_RATE = 10 * 1024 * 1024; // download rate in
	// Mb/s

	// Interval for printing number of contacts with interval granularity.
	public static final int INTERVAL_GRANULARITY = 600; // in seconds

	public static final int CPU_POWER_MIN = 700;
	public static final int CPU_POWER_MAX = 1500;
	

}
