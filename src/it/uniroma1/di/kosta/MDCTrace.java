package it.uniroma1.di.kosta;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Vector;

import it.uniroma1.di.kosta.Constants.Dataset;
import it.uniroma1.di.kosta.Constants.KEY;

public class MDCTrace extends Trace {
	private Scanner traceScanner;
	private double endTime;


	public MDCTrace(Dataset type, Map<KEY, Number> constants, File traceFile) throws FileNotFoundException {
		super();
		this.type = type;
		this.traceFile = traceFile;
		nodes = new HashMap<>();
		eventQueue = new PriorityQueue<>();
		this.constants = constants;
		this.traceScanner = new Scanner(traceFile);
		this.endTime = constants.get(KEY.TRACE_START_CONSIDERING).intValue();
		readNodes();
		setBatteryLevel();
		readTrace();
	}

	
	private void readNodes() throws FileNotFoundException {
		File nodeFile = new File(traceFile.getParent() + File.separator + "nodes.csv");
		Scanner nodesScanner = new Scanner(nodeFile);
		while(nodesScanner.hasNextLine()){
			int nodeId = Integer.parseInt(nodesScanner.nextLine());
			Node node = nodes.get(nodeId);
			if (node == null) {
				node = new Node(nodeId, constants);
				nodes.put(nodeId, node);
			}
		}
		nodesScanner.close();
	}

	/**
	 * Reads a trace depending on the format.
	 * 
	 * @param type
	 * @throws FileNotFoundException
	 */
	private void readTrace() throws FileNotFoundException {
		switch (this.type) {
		case INFOCOM_05:
			super.readInfocomTrace();
			break;

		case INFOCOM_06:
			// Infocom_06 has the same format as the Infocom_05 but it has more
			// nodes (78)
			super.readInfocomTrace();
			break;

		case HUMANET:
			super.readHumanetTrace();
			break;
		case NO_TRACE:
			super.generateTrace();
			break;
		case MDC :
			System.out.println("MDC");
			readMdcTrace();
			break;
		default:
			System.err.println("Trace type not recognized: " + type);
			break;
		}
	}

	/**
	 * MDC trace file has this format
	 * +----------+------------+------------+---------+----------+
	 * | userid   | userid     | meetTime   |leaveTime| Duration |
	 * +----------+------------+------------+---------+----------+
	 *    
	 * 
	 * 
	 * @throws FileNotFoundException
	 */
	private void readMdcTrace() throws FileNotFoundException {
		long meetTime = 0;
		long leaveTime = 0;
		this.endTime +=  24 * 3600;
		while (this.traceScanner.hasNextLine() && meetTime < this.endTime && leaveTime <= this.endTime) {
			String[] lineSplit = this.traceScanner.nextLine().trim().split("\\s+");

			int nodeId1 = Integer.parseInt(lineSplit[0]);
			int nodeId2 = Integer.parseInt(lineSplit[1]);
			meetTime = Long.parseLong(lineSplit[2]);
			leaveTime = Long.parseLong(lineSplit[3]);

			super.handleTraceLine(nodeId1, nodeId2, meetTime, leaveTime);
		}
	}


	/**
	 * @return true if there are still some events in the queue.
	 */
	@Override
	public boolean hasMoreEvents() {
		return eventQueue != null && !eventQueue.isEmpty() && (endTime < constants.get(KEY.TRACE_END_CONSIDERING).intValue()) ;
	}

	/**
	 * Retrieves but not removes the event with the smallest time.
	 * 
	 * @return The next event
	 * @throws FileNotFoundException 
	 */
	@Override
	public Event peekNextEvent() {
		Event peek = eventQueue.peek();
		if(peek.getTriggerTime() > this.endTime){
			try {
				readMdcTrace();
				peek = eventQueue.peek();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		return peek;
	}

	/**
	 * Retrieves and removes the event with the smallest time.
	 * 
	 * @return The next event
	 */
	@Override
	public Event pollNextEvent() {
		Event peek = eventQueue.peek();
		if(peek.getTriggerTime() > this.endTime){
			try {
				readMdcTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		return eventQueue.poll();
	}

}
