package it.uniroma1.di.kosta;

/**
 * The order of these event types is important: LEAVE should be declared before
 * MEET<br>
 * so that LEAVE < MEET
 * 
 * @author sokol
 *
 */
public enum EventType {
	LEAVE, MEET, TASK_OFFLOAD,  REPORT,
}
