package it.uniroma1.di.kosta;

import java.util.HashMap;
import java.util.Map;

public class BetaDist implements Reputation {

	/**
	 * 
	 * @author Mahdieh
	 *
	 */
	private float alpha;
	private float beta;
	private float directAlpha;
	private float directBeta;
	private Map<Constants.Interactions, Integer> interactionPerType;
	
	public BetaDist() {
		super();
		this.alpha = 1;
		this.beta = 1;
		this.directAlpha = 1;
		this.directBeta = 1;
		interactionPerType = new HashMap <Constants.Interactions, Integer> ();
			for (Constants.Interactions tmp : Constants.Interactions.values())
			interactionPerType.put(tmp, 0);
	
	}

	/**
	 * Increases the alpha parameter and so increases the value toward 1
	 * 
	 * @param taskDuration
	 */
	private void increaseAlpha(Constants.Interactions interactionType) {
		this.directAlpha += 1;
		this.alpha += 1;
		this.interactionPerType.put(interactionType, this.interactionPerType.get(interactionType)+1);
	}

	/**
	 * Increases the beta parameter and so deccreases the value toward 0
	 * 
	 * @param taskDuration
	 */
	private void increaseBeta(Constants.Interactions interactionType) {
		this.directBeta += 0.5;
		this.beta += 0.5;
		this.interactionPerType.put(interactionType, this.interactionPerType.get(interactionType)+1);

	}

	/**
	 * Return the expected value of the distribution
	 * 
	 * @return
	 */
	public float getExpectedValue() {
		return (alpha / (alpha + beta));
	}

	public float getDirectAlpha() {
		return this.directAlpha;
	}

	public void setAlpha(float alpha) {
		this.alpha = alpha;
	}

	public float getDirectBeta() {
		return this.directBeta;
	}

	public void setBeta(float beta) {
		this.beta = beta;
	}

	/**
	 * Return the mean value of the distribution
	 * 
	 * @return
	 */
	private float getMean() {
		// System.out.println("Alpha: " + alpha + "Beta: " + beta);
		float mean = (float) ((alpha - 0.33) / (alpha + beta - 0.66));
		return mean;
	}

	/**
	 * return the variance of the beta distribution
	 * 
	 * @return
	 */
	private float getVariance() {
		float var = (alpha * beta) / ((alpha + beta) * (alpha + beta) * (alpha + beta + 1));
		return var;
	}

	/**
	 * Return the Mean - Var/2 of the Beta Distribution as the main metric of
	 * the whole dist.
	 * 
	 * @return
	 */
	@Override
	public float getMetric() {
		// System.out.println(this.getMean() + ":" + Math.sqrt(getVariance()) /
		// 2);
		return (float) (this.getMean());
	}

	@Override
	public float getInteractionNum(Constants.Interactions interactionType) {
		return this.interactionPerType.get(interactionType).floatValue();

	}

	/**
	 * The interaction history with that node
	 */
	@Override
	public void interaction( Constants.Interactions interactionType, double value, double satis) {
		if (interactionType == Constants.Interactions.POSITIVE_GET_BACK_RESULT_III) {
			this.increaseAlpha(interactionType);
		} else if (interactionType == Constants.Interactions.NEGATIVE_GIVE_BACK_COIN_II
				|| interactionType == Constants.Interactions.NEGATIVE_GET_BACK_RESULT_I
				|| interactionType == Constants.Interactions.NEGATIVE_NOT_HONEST_IV) {
			this.increaseBeta(interactionType);
		} else {
			assert(0 == 1);
		}
	}

	@Override
	public Recommendation recommendation() {
		return new ThetaRecom(this.getMetric());
	}

	@Override
	public int getHisotry() {
		// TODO Auto-generated method stub
		return 0;
	}

}
