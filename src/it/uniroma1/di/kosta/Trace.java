package it.uniroma1.di.kosta;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.uniroma1.di.kosta.Constants.Dataset;
import it.uniroma1.di.kosta.Constants.KEY;

/**
 * Data structure to keep the trace.
 * 
 * @author sokol
 *
 */
public class Trace {

	//public static final Logger log = LogManager.getLogger("Trace");

	protected Dataset type;
	protected File traceFile;
	protected PriorityQueue<Event> eventQueue;
	protected HashMap<Integer, Node> nodes;
	protected Map<KEY, Number> constants;

	public static long startTime = Long.MAX_VALUE;
	public static long endTime = 0;

	public Trace(Dataset type, Map<KEY, Number> constants, File traceFile) throws FileNotFoundException {
		this.type = type;
		this.traceFile = traceFile;
		nodes = new HashMap<>();
		eventQueue = new PriorityQueue<>();
		this.constants = constants;
		readTrace();
		setBatteryLevel();
	}
	
	

	public Trace() {
	}
		


	public void setBatteryLevel() throws FileNotFoundException {
		
		if(constants.get(KEY.BATTERY).intValue()==Constants.fileBattery){
			File nodeFile = new File(traceFile.getParent() + File.separator + "battery.csv");
			Scanner nodesScanner = new Scanner(nodeFile);
			int thisNode = -1;
			Vector<Double>times = null;
			Vector<Double> value = null;
			while(nodesScanner.hasNextLine()){
				String[] lineSplit = nodesScanner.nextLine().trim().split("\\s+");
				int time = Integer.valueOf(lineSplit[1]);
				int nodeId = Integer.valueOf(lineSplit[2]);
				if(nodeId == thisNode){
					times.add((double) time);
					value.add((double) Integer.valueOf(lineSplit[4]));
				}else{
					System.out.println(thisNode);
					if(nodes.containsKey(thisNode)){
						nodes.get(thisNode).setBattery(new Battery(times, value));
					}
					times = new Vector<Double>();
					value = new Vector<Double>();
					thisNode = nodeId;
					times.add((double) time);
					value.add((double) Integer.valueOf(lineSplit[4]));
				}
			}
			System.out.println(thisNode + ":" + times.size());
			if(nodes.containsKey(thisNode)){
				nodes.get(thisNode).setBattery(new Battery(times, value));
			}
			for(Node n : this.nodes.values()){
				if(n.getBattery()==null)
					System.out.println("kh");
			}
		}else{
			for(Node n : this.nodes.values()){
				n.setBattery(new Battery());
			}
		}
		
	}


	/**
	 * Reads a trace depending on the format.
	 * 
	 * @param type
	 * @throws FileNotFoundException
	 */
	private void readTrace() throws FileNotFoundException {
		switch (this.type) {
		case INFOCOM_05:
			readInfocomTrace();
			break;

		case INFOCOM_06:
			// Infocom_06 has the same format as the Infocom_05 but it has more
			// nodes (78)
			readInfocomTrace();
			break;

		case HUMANET:
			readHumanetTrace();
			break;
		case NO_TRACE:
			generateTrace();
			break;
		
		default:
			System.err.println("Trace type not recognized: " + type);
			break;
		}
	}

	


	protected void generateTrace() {

		// Generate nodeId1, nodeId2, meetTime and leaveTime randomly
		int nodeId1 = 0;
		int nodeId2 = 0;
		double meetTime = 0;
		double leaveTime = 0;
		Random randomTime1 = new Random();
		Random randomTime2 = new Random();

		double contactDuration = (double) 1.0 / 200;
		double interContactDur = (double) 1.0 / 500;
		double time = constants.get(KEY.TRACE_START_CONSIDERING).doubleValue();

		for (int node1 = constants.get(KEY.FIRST_NODE_ID).intValue(); node1 <= constants.get(KEY.LAST_NODE_ID)
				.intValue(); node1++) {

			for (int node2 = node1 + 1; node2 <= constants.get(KEY.LAST_NODE_ID).intValue(); node2++) {
				time = constants.get(KEY.TRACE_START_CONSIDERING).doubleValue();

				nodeId1 = node1;
				nodeId2 = node2;
				while (true) {
					double interDur = Utils.nextPoissonDouble(randomTime1, interContactDur);
					double dur = Utils.nextPoissonDouble(randomTime2, contactDuration);

					// System.out.println("Trace" + nodeId1 + ":" + nodeId2 +
					// ":" + interDur + ":" + dur);

					if (time + interDur + dur > constants.get(KEY.TRACE_END_CONSIDERING).longValue()) {
						break;
					}

					meetTime = time + interDur;
					leaveTime = time + interDur + dur;

					// System.out.println(
					// "node1:" + node1 + "node2:" + node2 + "time:" + time +
					// "inter:" + interDur + "dur:" + dur);

					// Update the startTime and endTime for this trace
					if (meetTime < startTime) {
						startTime = (long) meetTime;
					}
					if (leaveTime > endTime) {
						endTime = (long) leaveTime;
					}

					// Consider only the events as seen by the node with smaller
					// id to avoid
					// bad events due to the fact that nodes are not in sync.
					if (nodeId1 > nodeId2) {
						assert false;
					}
					if (nodeId1 > constants.get(KEY.LAST_NODE_ID).intValue()
							|| nodeId2 > constants.get(KEY.LAST_NODE_ID).intValue()) {
						assert false;
					}

					Node nodeFirst = nodes.get(nodeId1);
					Node nodeSecond = nodes.get(nodeId2);

					if (nodeFirst == null) {
						nodeFirst = new Node(nodeId1, constants);
						nodes.put(nodeId1, nodeFirst);
					}
					if (nodeSecond == null) {
						nodeSecond = new Node(nodeId2, constants);
						nodes.put(nodeId2, nodeSecond);
					}

					// Add the two events in the priority queue.
					// The events will be sorted based on the time.
					eventQueue.add(new Event(nodeFirst, nodeSecond, meetTime, meetTime, EventType.MEET));
					eventQueue.add(new Event(nodeFirst, nodeSecond, leaveTime, leaveTime, EventType.LEAVE));

					time += dur + interDur;

				}
			}
		}

		System.out.println("Generating Finished!!!");
	}

	/**
	 * The INFOCOM_05 trace has the following format
	 * (http://crawdad.org/cambridge/haggle/):<br>
	 * =====<br>
	 * "contacts.Exp3.dat" is a file which describes the contact that were
	 * recorded by all devices we distributed during this experiment.<br>
	 * ========================<br>
	 * Examples taken from table.Exp3.dat (two first columns and first rows)<br>
	 * ========================<br>
	 * 1 8 121 121 1 0<br>
	 * 1 3 236 347 1 0<br>
	 * 1 4 236 347 1 0<br>
	 * 1 5 121 464 1 0<br>
	 * 1 8 585 585 2 464<br>
	 * ========================<br>
	 * 
	 * - The first column gives the ID of the device who recorded the sightings.
	 * <br>
	 * - The second column gives the ID of the device who was seen (it may be an
	 * iMote, or another device recorded during the experiment).<br>
	 * - The third and fourth column describe, respectively, the first and last
	 * time when the address of ID2 were recorded by ID1 for this contact.<br>
	 * - The fifth and sixth column are here for reading convenience. The fifth
	 * enumerate contacts with same ID1 and ID2, as 1,2,... . The last column
	 * describes the time difference between the beginning of this contact and
	 * the end of the previous contact with same ID1 and ID2. It is by
	 * convention set to 0 if this is the first contact for this ID1 and ID2.
	 * <br>
	 * 
	 * - Note, again, that these contacts may not be mutual between a pair of
	 * iMotes, because scanning period of different iMotes are not synchronized,
	 * and because the sightings might not be symmetric.<br>
	 * 
	 * @throws FileNotFoundException
	 */
	protected void readInfocomTrace() throws FileNotFoundException {
		System.out.println("Infocom Trace");

		Scanner scanner = new Scanner(traceFile);

		while (scanner.hasNextLine()) {
			String[] lineSplit = scanner.nextLine().trim().split("\\s+");
			int nodeId1 = Integer.parseInt(lineSplit[0]);
			int nodeId2 = Integer.parseInt(lineSplit[1]);
			long meetTime = Long.parseLong(lineSplit[2]);
			long leaveTime = Long.parseLong(lineSplit[3]);

			handleTraceLine(nodeId1, nodeId2, meetTime, leaveTime);
		}

		scanner.close();
	}

	/**
	 * http://crawdad.org/tecnalia/humanet/
	 * 
	 * The humanet database contains the following three tables: "t_encounters",
	 * "t_nodeList" and "t_states".<br>
	 * 
	 * Table "t_encounters" contains all the events detected by nodes throughout
	 * the experiment. It contains 7 fields, which are explained in the sequel:
	 * <br>
	 * - dev1 -> Bluetooth LAP address of the device that generated this entry
	 * <br>
	 * - dev2 -> Code identifying which type of entry this is:<br>
	 * a) "eeeee0" -> Transmission power reduction (-5dB)<br>
	 * b) "eeeee1" -> Transmission power increase ( 5dB)<br>
	 * c) "beac11" -> Device starts functioning in beacon mode (slave mode, when
	 * the device is left aside by its owner)<br>
	 * d) "beac00" -> Device starts functioning in normal mode (alternating
	 * master (detecting) and slave (being detected) modes)<br>
	 * e) "ffffff" -> Device rebooted<br>
	 * f) In all other cases, dev2 equals to the Bluetooth LAP address of the
	 * device detected.<br>
	 * - init -> Time of day when the event described by this entry started<br>
	 * - date_init -> Date when the event described by this entry started<br>
	 * - end -> Time of day when the event described by this entry finished<br>
	 * - date_end -> Date when the event described by this entry finished<br>
	 * - state -> Additional info regarding this entry (table "t_states"
	 * describes the meaning of used numerical codes):<br>
	 * a) If dev2="eeeee0" -> New transmission power in dBm<br>
	 * b) If dev2="eeeee1" -> New transmission power in dBm<br>
	 * c) If dev2="beac11" -> New transmission power in dBm<br>
	 * d) If dev2="beac00" -> New transmission power in dBm<br>
	 * e) If dev2="ffffff" -> Error code 255 as reboot indicator<br>
	 * f) In all other cases, "state" reflects device's position:<br>
	 * - "state"=0 -> Device is horizontally<br>
	 * - "state"=1 -> Device is vertically and static<br>
	 * - "state"=2 -> Device is vertically and moving<br>
	 * 
	 * Besides, and to identify more easily each device, table "t_nodeList"
	 * provides the list of the devices (and their Bluetooth address) that took
	 * part in the experiment:<br>
	 * - id -> unique numerical identificator of a device (in range [1,56] for
	 * people and [57,86] for beacons)<br>
	 * - nap -> Bluetooth NAP address of the device<br>
	 * - uap -> Bluetooth UAP address of the device<br>
	 * - lap -> Bluetooth LAP address of the device<br>
	 * 
	 * @throws FileNotFoundException
	 */
	protected void readHumanetTrace() throws FileNotFoundException {
		// On the same folder where the trace is located there should also be
		// the file called
		// "t_nodeList.csv" that I use to map LAP_address to nodeId: id nap uap
		// lap.
		Map<String, Integer> lapToIdMap = new HashMap<>();

		Scanner tNodeListScanner = new Scanner(new File(traceFile.getParent() + File.separator + "t_nodeList.csv"));
		while (tNodeListScanner.hasNextLine()) {
			String[] lineSplit = tNodeListScanner.nextLine().trim().split("\\s+");
			int id = Integer.parseInt(lineSplit[0]);
			if (id >= constants.get(KEY.FIRST_NODE_ID).intValue() && id <= constants.get(KEY.LAST_NODE_ID).intValue()) {
				// log.info("id=" + id);
				lapToIdMap.put(lineSplit[3], id);
			}
		}
		tNodeListScanner.close();

		// Now read the trace file: dev1 dev2 init date_init end date_end state
		// Keep only the events between nodes contained in the lapToIdMap.
		// Not only that, discard also events with dev2 \in {"eeeee0", "eeeee1",
		// "beac11", "beac00",
		// "ffffff"}.
		Set<String> discardEvents = new HashSet<>();
		discardEvents.add("eeeee0");
		discardEvents.add("eeeee1");
		discardEvents.add("beac11");
		discardEvents.add("beac00");
		discardEvents.add("ffffff");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Scanner traceScanner = new Scanner(traceFile);
		while (traceScanner.hasNextLine()) {
			String[] splitLine = traceScanner.nextLine().trim().split("\\s+");
			String dev1 = splitLine[0];
			String dev2 = splitLine[1];
			if (discardEvents.contains(dev2)) {
				continue;
			}
			if (!lapToIdMap.containsKey(dev1) || !lapToIdMap.containsKey(dev2)) {
				continue;
			}

			int nodeId1 = lapToIdMap.get(dev1);
			int nodeId2 = lapToIdMap.get(dev2);
			String init = splitLine[2];
			String dateInit = splitLine[3];
			String end = splitLine[4];
			String dateEnd = splitLine[5];

			try {
				long meetTimestamp = sdf.parse(dateInit + " " + init).getTime() / 1000; // convert
				// in
				// s
				long leaveTimestamp = sdf.parse(dateEnd + " " + end).getTime() / 1000;
				handleTraceLine(nodeId1, nodeId2, meetTimestamp, leaveTimestamp);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		traceScanner.close();
	}

	protected void handleTraceLine(int nodeId1, int nodeId2, long meetTime, long leaveTime) {

		// Discard events with short or long duration
		if ((leaveTime - meetTime) <= constants.get(KEY.CONTACT_DUR_THRESHOLD_MIN).longValue()
				|| (leaveTime - meetTime) >= constants.get(KEY.CONTACT_DUR_THRESHOLD_MAX).longValue()) {
			System.out.println("Exceeding Contatc Time Limits");
			return;
		}

		// Discard events out of our considered interval
		if (meetTime < constants.get(KEY.TRACE_START_CONSIDERING).longValue()
				|| meetTime > constants.get(KEY.TRACE_END_CONSIDERING).longValue()) {
			// log.info("Discarding event with timestamps: " + meetTime + " " +
			// leaveTime);
			System.out.println("Exceeding Limits");

			return;
		}
		// Discard events out of our considered interval
		if (leaveTime < constants.get(KEY.TRACE_START_CONSIDERING).longValue()
				|| leaveTime > constants.get(KEY.TRACE_END_CONSIDERING).longValue()) {
			System.out.println("Exceeding Limits");
			return;
		}

		// Update the startTime and endTime for this trace
		if (meetTime < startTime) {
			startTime = meetTime;
		}
		if (leaveTime > endTime) {
			endTime = leaveTime;
		}

		// Consider only the events as seen by the node with smaller id to avoid
		// bad events due to the fact that nodes are not in sync.
		if (nodeId1 >= nodeId2) {
			System.out.println("Exceeding Node Id Priority");
			return;
		}
		if (nodeId1 > constants.get(KEY.LAST_NODE_ID).intValue()
				|| nodeId2 > constants.get(KEY.LAST_NODE_ID).intValue()) {
			System.out.println("Exceeding Node Id Limit");
			return;
		}

		Node node1 = nodes.get(nodeId1);
		Node node2 = nodes.get(nodeId2);

		if (node1 == null) {
			node1 = new Node(nodeId1, constants);
			nodes.put(nodeId1, node1);
		}
		if (node2 == null) {
			node2 = new Node(nodeId2, constants);
			nodes.put(nodeId2, node2);
		}

		// Add the two events in the priority queue.
		// The events will be sorted based on the time.
		eventQueue.add(new Event(node1, node2, meetTime, meetTime, EventType.MEET));
		eventQueue.add(new Event(node1, node2, leaveTime, leaveTime, EventType.LEAVE));
	}

	/**
	 * @return the eventQueue
	 */
	public PriorityQueue<Event> getEventQueue() {
		return eventQueue;
	}

	/**
	 * @return true if there are still some events in the queue.
	 */
	public boolean hasMoreEvents() {
		return eventQueue != null && !eventQueue.isEmpty() ;
	}

	/**
	 * Retrieves but not removes the event with the smallest time.
	 * 
	 * @return The next event
	 */
	public Event peekNextEvent() {
		return eventQueue.peek();
	}

	/**
	 * Retrieves and removes the event with the smallest time.
	 * 
	 * @return The next event
	 */
	public Event pollNextEvent() {
		return eventQueue.poll();
	}

	public void addEvent(Event event) {
		eventQueue.add(event);
	}

	/**
	 * @return the nodes
	 */
	public HashMap<Integer, Node> getNodes() {
		return nodes;
	}

	/**
	 * @return Sorted list with the number of contacts among all couple of
	 *         nodes. The couples (id1, id2) considered are those with id1 < id2
	 *         in order to avoid double counting.
	 */
	public List<Integer> getContactNumberDistr() {
		List<Integer> nrContactsDistr = new LinkedList<>();
		for (Node n : nodes.values()) {
			nrContactsDistr.addAll(n.getContactNumberAmongCouples());
		}
		Collections.sort(nrContactsDistr);
		return nrContactsDistr;
	}

	/**
	 * @return Sorted list with all meet timestamps between all nodes.
	 */
	public List<Double> getMeetTimestamps() {
		List<Double> meetTimestamps = new LinkedList<>();
		for (Node n : nodes.values()) {
			meetTimestamps.addAll(n.getMeetTimestamps());
		}
		Collections.sort(meetTimestamps);
		return meetTimestamps;
	}

	/**
	 * @return Sorted list with the contacts duration among all couple of nodes.
	 *         The couples (id1, id2) considered are those with id1 < id2 in
	 *         order to avoid double counting.
	 */
	public List<Double> getContactDurationDistr() {
		List<Double> contactDurDistr = new LinkedList<>();
		for (Node n : nodes.values()) {
			contactDurDistr.addAll(n.getContactDurationAmongCouples());
		}

		Collections.sort(contactDurDistr);
		return contactDurDistr;
	}

	/**
	 * @return Sorted list with the contacts duration among all couple of nodes.
	 *         The couples (id1, id2) considered are those with id1 < id2 in
	 *         order to avoid double counting.
	 */
	public List<Double> getIntercontactDistr() {
		List<Double> intercontactDistr = new LinkedList<>();
		for (Node n : nodes.values()) {
			intercontactDistr.addAll(n.getIntercontactsAmongCouples());
		}

		Collections.sort(intercontactDistr);
		return intercontactDistr;
	}

	/**
	 * Get, from each node, the number of nodes in contact with when a task had
	 * to be executed.
	 * 
	 * @return A map of (timestamp, nrNodes), where timestamp is the task's
	 *         startTime and nrNodes is the number of nodes in contact with the
	 *         node at time timestamp.
	 */
	public Map<Double, Integer> getNrNodesInContactOnTask() {
		Map<Double, Integer> nrNodesInContactOnTask = new HashMap<>();

		for (Node n : nodes.values()) {
			nrNodesInContactOnTask.putAll(n.getNrNodesInContactOnTask());
		}
		return nrNodesInContactOnTask;
	}

	/**
	 * @return Total number of tasks executed in local by all nodes.
	 */
	public int getNrExecutedLocalTasks() {
		int count = 0;
		for (Node n : nodes.values()) {
			count += n.nrExecutedLocalTasks();
		}
		return count;
	}

	/**
	 * @return Total number of tasks (for all nodes) that were started in local
	 *         but interrupted because an offloaded result came before the local
	 *         execution could finish.
	 */
	public int getNrInterruptedLocalTasks() {
		int count = 0;
		for (Node n : nodes.values()) {
			count += n.nrInterruptedLocalTasks();
		}
		return count;
	}

	/**
	 * @return Total number of tasks (for all nodes) that were planned to run in
	 *         local but the simulation finished before these tasks could finish
	 *         in local or remote.
	 */
	public int getNrFutureLocalTasks() {
		int count = 0;
		for (Node n : nodes.values()) {
			count += n.nrFutureLocalTasks();
		}
		return count;
	}

	/**
	 * @return Total number of offload requests (for all nodes) successfully
	 *         made but no result has arrived from remote yet. The same task
	 *         could be sent to more than one node, this why the number of
	 *         requests is different than the number of tasks offloaded.
	 */
	public int getNrOffloadRequests() {
		int count = 0;
		for (Node n : nodes.values()) {
			count += n.nrOffloadRequests();
		}
		return count;
	}

	public int getNrOffloadRequestsReceived() {
		int count = 0;
		for (Node n : nodes.values()) {
			count += n.nrOffloadRequestsReceived();
		}
		return count;
	}

	public int getNrFutureOffloadRequests() {
		int count = 0;
		for (Node n : nodes.values()) {
			count += n.nrFutureOffloadTasks();
		}
		return count;
	}

	/**
	 * @return Total number of offload results (for all nodes) that were
	 *         received. The result of the same task received from more than one
	 *         node is counted more than once.
	 */
	public int getNrOffloadResultsReceived() {
		int count = 0;
		for (Node n : nodes.values()) {
			count += n.nrOffloadRresultsReceived();
		}
		return count;
	}

	/**
	 * 
	 * @return Number of unique tasks that all nodes have offloaded and received
	 *         result.
	 */
	public int getNrUniqueFinishedOffloadedTasks() {
		Set<Task> uniqueSet = new HashSet<>();

		for (Node n : nodes.values()) {
			uniqueSet.addAll(n.getSetUniqueFinishedOffloadedTasks());
		}
		return uniqueSet.size();
	}

	/**
	 * Return the average power level of all the nodes in the network
	 * 
	 * @return
	 */
	public float getAveragePowerLevel(double current) {
		System.out.println("Finished...");
		float powerLevelSum = 0;
		for (Node n : this.getNodes().values()) {
			powerLevelSum += n.getBatteryLevel(current);
		}
		return powerLevelSum / getNodes().size();

	}

	public float getStdDevOfPowerLevel(double current) {

		float mean = this.getAveragePowerLevel(current);
		float temp = 0;
		for (Node n : this.getNodes().values())
			temp += (mean - n.getBatteryLevel(current)) * (mean - n.getBatteryLevel(current));
		return (float) Math.sqrt(temp / this.getNodes().size()) / 2;
	}
}
