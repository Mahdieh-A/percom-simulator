package it.uniroma1.di.kosta;

import it.uniroma1.di.kosta.Constants.Interactions;

public interface Reputation {

	float getMetric();

	float getInteractionNum(Constants.Interactions interactionType);

	Recommendation recommendation();

	void interaction(Interactions interactionType, double weight, double msatis);

	int getHisotry();

}
