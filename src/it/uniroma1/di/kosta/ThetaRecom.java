package it.uniroma1.di.kosta;

public class ThetaRecom implements Recommendation {

	private float trust;

	public ThetaRecom(float rtrust) {
		this.trust = rtrust;
	}

	@Override
	public float getMetric() {
		return trust;
	}

}
