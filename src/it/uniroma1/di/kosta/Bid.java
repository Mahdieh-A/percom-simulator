package it.uniroma1.di.kosta;

public class Bid {

	// These are the parameters set on the interface
	private int offeredCpu;
	private int requiredFlopCoins;

	public Bid(int offeredCpu, int requiredFlopCoins) {
		this.offeredCpu = offeredCpu;
		this.requiredFlopCoins = requiredFlopCoins;
	}

	public int getOfferedCpu() {
		return offeredCpu;
	}

	public int getRequiredFlopCoins() {
		return this.requiredFlopCoins;
	}

}
